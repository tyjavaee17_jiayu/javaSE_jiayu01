package com.zuoye.zuoye01.zuoye0102;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class DemoFileUpLoadServer {
    public static void main(String[] args) throws IOException {
        File file = new File("e:\\jyUpload");
        if (!file.exists()){
            file.mkdir();
        }
        ServerSocket serverSocket = new ServerSocket(7777);
        Socket socket = serverSocket.accept();
        InputStream is = socket.getInputStream();
        FileOutputStream fos = new FileOutputStream(file + "\\jyLol.jpg");
        byte[] bytes = new byte[1024];
        int len = 0;
        while ((len = is.read(bytes))!= -1){
            fos.write(bytes);
        }
        OutputStream os = socket.getOutputStream();
        os.write("图片已收到！".getBytes());
        fos.close();
        socket.close();
        serverSocket.close();
    }
}
