package com.zuoye.zuoye01.zuoye0103;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class BSTCPServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8081);
        Socket socket = serverSocket.accept();
        InputStream is = socket.getInputStream();
       /* byte[] bytes = new byte[1024];
        int len = 0;
        while ((len = is.read(bytes))!= -1){
            System.out.println(new String(bytes,0,len));
        }*/
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String fileName = br.readLine();
        String[] arr = fileName.split(" ");
        String path = arr[1].substring(1);
        FileInputStream fis = new FileInputStream(path);
        OutputStream os = socket.getOutputStream();
        os.write("HTTP/1.1 200 OK\r\n".getBytes());
        os.write("Content-Type:text/html\r\n".getBytes());
        os.write("\r\n".getBytes());
        byte[] bytes = new byte[1024];
        int len = 0;
        while ((len = fis.read(bytes))!= -1){
            os.write(bytes,0,len);
        }
        fis.close();
        socket.close();
        serverSocket.close();
    }
}
