package com.zuoye.zuoye01.zuoye0101;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class DemoClient {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1", 4567);
        OutputStream os = socket.getOutputStream();
        os.write("你好，我是客户端小白".getBytes());
        InputStream is = socket.getInputStream();
        byte[] bytes = new byte[1024];
        int len = is.read(bytes);
        socket.close();
    }
}
