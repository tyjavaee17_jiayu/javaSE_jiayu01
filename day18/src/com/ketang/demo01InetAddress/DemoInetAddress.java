package com.ketang.demo01InetAddress;

import java.net.InetAddress;
import java.net.UnknownHostException;

/*
        java.net.InetAddress类:描述计算机的ip地址
        此类表示互联网协议 (IP) 地址。
        可以使用InetAddress类中的方法获取到计算机的ip地址

        静态方法:
        static InetAddress getLocalHost() 返回本地主机(你自己的电脑)。
        static InetAddress getByName(String host) 在给定主机名的情况下确定主机的 IP 地址。

        非静态的方法:
        String getHostAddress() 返回 IP 地址字符串（以文本表现形式）。
        String getHostName() 获取此 IP 地址的主机名。
        */
public class DemoInetAddress {
    public static void main(String[] args) throws UnknownHostException {
        //show01();
        show02();
    }

    private static void show02() throws UnknownHostException {
        //InetAddress inetAddress1 = InetAddress.getByName("DESKTOP-1ME9V18");
        //InetAddress inetAddress2 = InetAddress.getByName("www.tencent.com");
        InetAddress inetAddress3 = InetAddress.getByName("www.google.com");
        //System.out.println(inetAddress1);
        //System.out.println(inetAddress2);
        System.out.println(inetAddress3);

    }

    private static void show01() throws UnknownHostException {
        InetAddress localHost = InetAddress.getLocalHost();
        System.out.println(localHost);

        String[] arr = localHost.toString().split("/");
        String name = arr[0];
        String ip = arr[1];
        System.out.println(name);
        System.out.println(ip);

        System.out.println("----------------");
        String hostName = localHost.getHostName();
        System.out.println(hostName);

        System.out.println("----------------");
        String hostAddress = localHost.getHostAddress();
        System.out.println(hostAddress);
    }
}
