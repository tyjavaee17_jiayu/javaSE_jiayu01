package com.ketang.demo03FileUpLoad;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class DemoTCPClient {
    public static void main(String[] args) throws IOException {

        FileInputStream fis = new FileInputStream("d:\\LOL.jpg");

        Socket socket = new Socket("127.0.0.1", 8888);

        OutputStream os = socket.getOutputStream();

        byte[] bytes = new byte[1024];
        int len = 0;
        while ((len = fis.read(bytes))!= -1){
            os.write(bytes,0,len);
        }
        /*
            解决:上传完图片之后,给服务器写一个结束标记,告之服务器文件已经上传完毕,无需等待
            Socket对象中的方法:
                void shutdownOutput() 禁用此套接字的输出流。
            对于 TCP 套接字，任何以前写入的数据都将被发送，并且后跟 TCP 的正常连接终止序列。
          */
        socket.shutdownOutput();

        InputStream is = socket.getInputStream();

        while ((len = is.read(bytes))!= -1){
            System.out.println(new String(bytes,0,len));
        }

        fis.close();
        socket.close();
    }
}
