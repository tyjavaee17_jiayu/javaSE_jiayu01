package com.ketang.demo03FileUpLoad;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class DemoServer {
    public static void main(String[] args) throws IOException {
        File file = new File("e:\\upload");
        if (!file.exists()){
            file.mkdir();
        }

        ServerSocket serverSocket = new ServerSocket(8888);

        Socket socket = serverSocket.accept();

        InputStream is = socket.getInputStream();

        FileOutputStream fos = new FileOutputStream(file + "\\Lol.jpg");

        byte[] bytes = new byte[1024];

        int len = 0;
        while ((len = is.read(bytes))!= -1){
            fos.write(bytes,0,len);
        }

        OutputStream os = socket.getOutputStream();

        os.write("上传成功！".getBytes());

        fos.close();
        socket.close();
        serverSocket.close();
    }
}
