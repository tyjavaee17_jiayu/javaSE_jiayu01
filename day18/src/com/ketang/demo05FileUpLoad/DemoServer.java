package com.ketang.demo05FileUpLoad;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class DemoServer {
    public static void main(String[] args) throws IOException {
        File file = new File("e:\\upload");
        if (!file.exists()){
            file.mkdir();
        }
        ServerSocket serverSocket = new ServerSocket(12345);
        while (true){
            Socket socket = serverSocket.accept();
            new Thread(()->{
                try {
                    InputStream is = socket.getInputStream();
                    String fileName = "jiayu"+ System.currentTimeMillis()+".jpg";
                    FileOutputStream fos = new FileOutputStream(file+File.separator+fileName);
                    byte[] bytes = new byte[1024];
                    int len = 0;
                    while ((len = is.read(bytes))!= -1){
                        fos.write(bytes,0,len);
                    }
                    OutputStream os = socket.getOutputStream();
                    os.write("图片上传成功！".getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
