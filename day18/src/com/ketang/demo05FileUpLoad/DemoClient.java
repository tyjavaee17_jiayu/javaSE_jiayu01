package com.ketang.demo05FileUpLoad;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class DemoClient {
    public static void main(String[] args) throws IOException {
       //1.创建本地字节输入流FileInputStream对象,构造方法中绑定要读取的数据源
        FileInputStream fis = new FileInputStream("d:\\LOL.jpg");
        //2.创建客户端Socket对象,构造方法绑定服务器的ip地址和端口号
        Socket socket = new Socket("127.0.0.1", 12345);
        //3.使用客户端Socket对象中的方法getOutputStream,获取网络字节输出流OutputStream对象
        OutputStream os = socket.getOutputStream();
        //4.使用本地字节输入流FileInputStream对象中的read方法,读取要上传的文件
        byte[] bytes = new byte[1024];
        int len = 0;
        while ((len = fis.read(bytes))!= -1){
            //5.使用网络字节输出流OutputStream对象中的方法write,把读取到的文件上传到服务器中
            os.write(bytes,0,len);
        }
        socket.shutdownOutput();

       //6.使用客户端Socket对象中的方法getInputStream,获取网络字节输入流InputStream对象
        InputStream is = socket.getInputStream();
        //7.使用网络字节输入流InputStream对象中的方法read,读取服务器回写的"上传成功!"
        while ((len = is.read(bytes))!= -1){
            System.out.println(new String(bytes,0,len));
        }
       //8.释放资源(FileInputStream对象,Socket对象)
        fis.close();
        socket.close();
    }
}
