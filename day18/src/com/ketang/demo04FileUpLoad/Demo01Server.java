package com.ketang.demo04FileUpLoad;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Demo01Server {
    public static void main(String[] args) throws IOException {

        File file = new File("e:\\upload");
        if (!file.exists()){
            file.mkdir();
        }

        ServerSocket serverSocket = new ServerSocket(5555);

        Socket socket = serverSocket.accept();
        //使用Socket对象中的方法getInputStream,获取网络字节输入流InputStream对象
        InputStream is = socket.getInputStream();

        String fileName = "yyyy" + System.currentTimeMillis() + ".jpg";

        FileOutputStream fos = new FileOutputStream(file+File.separator+fileName);

        byte[] bytes = new byte[1024];
        int len = 0;
        while((len = is.read(bytes))!= -1){
            fos.write(bytes,0,len);
        }
        OutputStream os = socket.getOutputStream();
        os.write("图片上传成功！".getBytes());
        fos.close();
        socket.close();
        serverSocket.close();
    }
}
