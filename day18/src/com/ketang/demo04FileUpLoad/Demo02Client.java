package com.ketang.demo04FileUpLoad;

import java.io.*;
import java.net.Socket;

public class Demo02Client {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("d:\\LOL.jpg");

        Socket socket = new Socket("127.0.0.1", 5555);

        OutputStream os = socket.getOutputStream();

        byte[] bytes = new byte[1024];
        int len = 0;
        while ((len = fis.read(bytes))!= -1){
            os.write(bytes,0,len);
        }

        socket.shutdownOutput();

        InputStream is = socket.getInputStream();
        while ((len = is.read(bytes))!= -1){
            System.out.println(new String(bytes,0,len));

        }
        fis.close();
        socket.close();
    }
}
