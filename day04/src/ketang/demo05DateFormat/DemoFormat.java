package ketang.demo05DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

public class DemoFormat {
    public static void main(String[] args) {
        show01();
        //Wed Mar 18 17:53:43 CST 2020
        //2020/53/18
        show02();
        //Fri Jan 14 00:06:00 CST 1994

    }
    /*
        Date parse(String source)  把符合模式的字符串解析为Date日期
        实现步骤:
            1.创建SimpleDateFormat对象,构造方法中传递指定的模式
            2.调用SimpleDateFormat对象中的方法parse,把符合模式的字符串解析为Date日期
     */
    private static void show02() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd");
        Date date = null;
        try {
            date = sdf.parse("1994/06/14");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);
    }
    /*
        String format(Date date)  根据指定的日期,把日期格式化为符合模式的字符串
        使用步骤:
            1.创建SimpleDateFormat对象,构造方法中传递指定的模式

            2.调用SimpleDateFormat对象中的方法format根据构造方法中传递的模式,把日期格式化为符合模式的字符串
     */
    private static void show01() {
        SimpleDateFormat sdf =  new SimpleDateFormat("yyyy/mm/dd");
        Date date = new Date();
        System.out.println(date);
        String s = sdf.format(date);
        System.out.println(s);

    }
}
