package ketang.demo01toString;

public class DemoStudent {
    public static void main(String[] args) {
        Person p = new Person("迪丽热巴",18);
        System.out.println(p);//@1540e19d  （没有重写ToString方法）
        System.out.println("---------------");
        System.out.println(p);//Person{name='迪丽热巴', age=18}
        System.out.println("---------------");
        String s = p.toString();
        System.out.println(s);//Person{name='迪丽热巴', age=18}
    }
}
