package ketang.demo06calendar;

import java.util.Calendar;
import java.util.Date;

/*
    Calendar类中的常用成员方法
        int get(int field) 获取给定日历字段的值。
        void set(int field, int value) 将给定的日历字段设置为给定值。
        void add(int field, int amount)  把日历字段增加|减少指定的值
        Date getTime()  把日历转换为日期对象
    注意:
        以上方法的参数(int field),让我们传递指定的日历字段,这些日历字段在Calendar类中被定义为了常量
            public final static int YEAR = 1;
            public final static int MONTH = 2;
            public final static int DATE = 5;
            public final static int DAY_OF_MONTH = 5;
            public final static int HOUR = 10;
            public final static int MINUTE = 12;
            public final static int SECOND = 13;
            public final static int MILLISECOND = 14;
 */
public class Demo02Calendar {
    public static void main(String[] args) {
        show01();
        //2020年3月18日
        //6时24分52秒
        show02();
        //6670年8月20日
        show03();
        //2021年0月22日
        show04();
        //Wed Mar 18 18:57:41 CST 2020
    }
    /*
       Date getTime()  把日历转换为日期对象
    */
    private static void show04() {
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        System.out.println(date);
    }
    /*
        void add(int field, int amount)  把日历字段增加|减少指定的值
        参数:
            int field:指定的日历字段
            int amount:给字段增加|减少的值
                传递正数:增加
                传递负数:减少
     */
    private static void show03() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR,1);
        c.add(Calendar.MONTH,-2);
        c.add(Calendar.DAY_OF_MONTH,4);

        int year = c.get(Calendar.YEAR);
        System.out.print(year + "年");
        int month = c.get(Calendar.MONTH);
        System.out.print(month + "月");
        int day = c.get(Calendar.DAY_OF_MONTH);
        System.out.println(day + "日");
    }
    /*
        void set(int field, int value) 将给定的日历字段设置为给定值。
        参数:
            int field:指定的字段
            int value:指定的值
       同时设置年月日,建议使用以下的方法
             void set(int year, int month, int date) 设置日历字段 YEAR、MONTH 和 DAY_OF_MONTH 的值。
     */
    private static void show02() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR,1994);
        c.set(Calendar.MONTH,6);
        c.set(Calendar.DATE,14);
        c.set(6666,55,20);
        int year = c.get(Calendar.YEAR);
        System.out.print(year+ "年");
        int month = c.get(Calendar.MONTH);
        System.out.print((month+1)+ "月");
        int day = c.get(Calendar.DAY_OF_MONTH);
        System.out.println(day+ "日");
    }
    /*
        int get(int field) 获取给定日历字段的值。
        参数:
            int field:传递指定的日历字段
     */
    private static void show01() {
        Calendar C = Calendar.getInstance();
        int year = C.get(Calendar.YEAR);
        System.out.print(year+"年");
        int month = C.get(Calendar.MONTH);
        System.out.print((month+1)+ "月");
        int day = C.get(Calendar.DAY_OF_MONTH);
        System.out.println(day + "日");
        int hour = C.get(Calendar.HOUR);
        System.out.print(hour+"时");
        int minute = C.get(Calendar.MINUTE);
        System.out.print(minute+ "分");
        int second = C.get(Calendar.SECOND);
        System.out.println(second+"秒");

    }
}
