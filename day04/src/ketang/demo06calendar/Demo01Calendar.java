package ketang.demo06calendar;

import java.util.Calendar;

/*
    java.util.Calendar类:日历类
        我们可以使用Calendar类中的方法获取日历上的字段(年,月,日,时,分,秒...)
        我们可以使用Calendar类中的方法修改日历上的字段的值  2020-->2030  11月-->3月
        我们可以使用Calendar类中的方法把日历上的字段的值增加|减少   2020-5=2015  3月+4 = 7月
        我们可以使用Calendar类中的方法把日历对象转换为日期Date对象
    Calendar类是一个抽象类,无法直接创建对象使用,在Calendar类中有一个静态方法叫getInstance
        返回了Calendar类的子类对象
        static Calendar getInstance() 使用默认时区和语言环境获得一个日历。
*/
public class Demo01Calendar {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        System.out.println(c);
        //java.util.GregorianCalendar
        // [time=1584526414776,
        // areFieldsSet=true,
        // areAllFieldsSet=true,
        // lenient=true,
        // zone=sun.util.calendar.ZoneInfo[id="Asia/Shanghai",
        // offset=28800000,
        // dstSavings=0,
        // useDaylight=false,
        // transitions=19,
        // lastRule=null],f
        // irstDayOfWeek=1,
        // minimalDaysInFirstWeek=1,
        // ERA=1,YEAR=2020,MONTH=2,
        // WEEK_OF_YEAR=12,
        // WEEK_OF_MONTH=3,
        // DAY_OF_MONTH=18,
        // DAY_OF_YEAR=78,
        // DAY_OF_WEEK=4,
        // DAY_OF_WEEK_IN_MONTH=3,
        // AM_PM=1,
        // HOUR=6,
        // HOUR_OF_DAY=18,
        // MINUTE=13,S
        // ECOND=34,
        // MILLISECOND=776,
        // ZONE_OFFSET=28800000,
        // DST_OFFSET=0]
    }
}
