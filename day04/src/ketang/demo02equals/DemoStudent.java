package ketang.demo02equals;

import java.util.Random;

import static javafx.scene.input.KeyCode.R;

public class DemoStudent {
    public static void main(String[] args) {
        Student s1 = new Student("陈乔恩",30);
        Student s2 = new Student("朱亚文",30);
        System.out.println(s1.toString());
        System.out.println(s2.toString());
        System.out.println("-----------------");
        boolean b = s1.equals(s2);
        System.out.println(b);
        System.out.println("-----------------");
        s1 = s2 ;
        System.out.println(s1);
        System.out.println(s2);
        boolean b1 = s1.equals(s2);
        System.out.println(b1);
        System.out.println("-----------------");
        Random r = new Random();
        boolean b2 = s1.equals(r);
        System.out.println(b2);
        boolean b3 = s1.equals(s1);
        System.out.println(b3);
    }
}
