package ketang.demo04Date;

import java.util.Date;

//Date() 获取当前系统的日期和时间
//Date(long date) 传递毫秒值,把毫秒值转换为Date日期
public class DemoDate {
    public static void main(String[] args) {

        show01();
        //Wed Mar 18 17:16:46 CST 2020
        show02();
        //空参构造
        //Thu Jan 01 08:00:00 CST 1970
        //带参构造
        //Thu Jan 22 16:40:12 CST 1970
        show03();
        //Wed Mar 18 17:25:00 CST 2020
        //1584523500866
        show04();
        //Wed Mar 18 17:27:23 CST 2020
        //Thu Jan 01 08:00:00 CST 1970
    }
    /*
        void setTime(long time)
          设置此 Date 对象，以表示 1970 年 1 月 1 日 00:00:00 GMT 以后 time 毫秒的时间点。
        此方法相当于Date类的带参数构造方法
           Date(long date)
     */
    private static void show04() {
        Date date = new Date();
        System.out.println(date);
        date.setTime(0L);//传递毫秒值,把毫秒值转换为Date日期
        System.out.println(date);
    }
    /*
        long getTime() 重点
          返回自 1970 年 1 月 1 日 00:00:00 GMT 以来此 Date 对象表示的毫秒数。
     */
    private static void show03() {
        Date date = new Date();
        System.out.println(date);
        long time = date.getTime();// 把日期转换为毫秒值(当前时间和时间原点之间共经历了多少毫秒)
        System.out.println(time);
    }
    /*
        Date类的带参数的构造方法:
            Date(long date)
        注意:
            在时间和日期相关的类中,long类型的值传递就是毫秒值
     */
    private static void show02() {
        //满参
        Date date = new Date(0L);//传递毫秒值,把毫秒值转换为Date日期
        System.out.println(date);
        //满参
        date = new Date(1845612164L);//传递毫秒值,把毫秒值转换为Date日期
        System.out.println(date);
    }

    private static void show01() {
        //空参
        Date date = new Date();//Date() 获取当前系统的日期和时间
        System.out.println(date);
    }
}
