package ketang.demo08system;

import java.util.Arrays;
import java.util.Date;

/*
    java.lang.System类:
        System 类包含一些有用的类字段和方法。它不能被实例化(私有了构造方法)。
        里边的方法都是静态的,通过类名.方法名(参数)可以直接使用
    成员方法:
        static void exit(int status) 终止当前正在运行的 Java 虚拟机(JVM)。
        static long currentTimeMillis() 返回以毫秒为单位的当前时间。
        static void arraycopy(Object src, int srcPos, Object dest, int destPos, int length)  数组复制
 */
public class DemoSystem {
    public static void main(String[] args) {
        show01();//我要与太阳肩并肩
        show02();//执行程序共用了12毫秒值
        show03();//遍历前数组：[6, 7, 8, 9, 10]
                 //遍历后数组：[1, 2, 3, 9, 10]
    }
    /*
        static void arraycopy(Object src, int srcPos, Object dest, int destPos, int length)  数组复制
        参数：
            src - 源数组。
            srcPos - 源数组中的起始位置。
            dest - 目标数组。
            destPos - 目标数据中的起始位置。
            length - 要复制的数组元素的数量。
        需求:
            复制前:把源数组[1,2,3,4,5]的前3个元素复制到目标数组中[6,7,8,9,10]
            复制后:把源数组[1,2,3,4,5]的前3个元素复制到目标数组中[1,2,3,9,10]
     */
    private static void show03() {
        int[] arr1 = {1,2,3,4,5};
        int[] arr2 = {6,7,8,9,10};
        System.out.println("遍历前数组："+ Arrays.toString(arr2));
        System.arraycopy(arr1,0,arr2,0,3);
        System.out.println("遍历后数组："+ Arrays.toString(arr2));

    }
    /*
        static long currentTimeMillis() 返回以毫秒为单位的当前时间。
        需求:
            在控制台输出1-10000，计算这段代码执行了多少毫秒
        作用:
            测试程序的效率
     */
    private static void show02() {
        long l = System.currentTimeMillis();//在程序执行前获取一次毫秒值
        for (int i = 0; i < 1000; i++) {
            System.out.println(i);
        }
        long time = new Date().getTime();
        System.out.println("执行程序共用了"+(time - l)+"毫秒值");
    }

    /*
        static void exit(int status) 终止当前正在运行的 Java 虚拟机(JVM)。
        参数:
            int status:状态码
                0:正常终止
                非0:的状态码表示异常终止。
     */
    private static void show01() {
        System.out.println("我要与太阳肩并肩");
        //System.exit(0);
    }
}
