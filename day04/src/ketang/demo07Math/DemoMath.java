package ketang.demo07Math;
/*
    java.lang.Math类:数学工具类
        Math 类包含用于执行基本数学运算的方法，如初等指数、对数、平方根和三角函数。
        Math类中的方法都是静态方法,通过类名.方法名(参数)可以直接使用
        私有了空参数构造方法,不让我们直接创建对象使用
    成员方法:
        static double abs(double a|int a) 返回 double|int 值的绝对值。
        static double ceil(double a)  向上取整
        static double floor(double a)  向下取整
        static long round(double a) 四合五入 long。
        static int round(float a) 四合五入 int。
        static int max(int a, int b) 返回两个 int 值中较大的一个。
        static double max(double a, double b) 返回两个 double 值中较大的一个。
        static int min(int a, int b) 返回两个 int 值中较小的一个。
        static double min(double a, double b) 返回两个 double 值中较小的一个。
        static double pow(double a, double b)  a的b次方
        static double sqrt(double a)  计算平方根
        static double random()  返回一个随机小数,范围[0.0,1.0)
 */
public class DemoMath {
    public static void main(String[] args) {
        System.out.println(Math.abs(10));
        System.out.println(Math.abs(-10));
        System.out.println("----------");
        System.out.println(Math.ceil(5.1));
        System.out.println(Math.ceil(4.9));
        System.out.println(Math.ceil(-4.9));
        System.out.println(Math.ceil(-5.0));
        System.out.println("----------");
        System.out.println(Math.floor(5.1));
        System.out.println(Math.floor(4.9));
        System.out.println(Math.floor(-5.1));
        System.out.println(Math.floor(-4.9));
        System.out.println("----------");
        System.out.println(Math.round(5.9999999));
        System.out.println(Math.round(4.9));
        System.out.println("----------");
        System.out.println(Math.max(15,50));
        System.out.println(Math.min(20,60));
        System.out.println(Math.max(1.2,12.1));
        System.out.println(Math.min(-14.0,123.0));
        System.out.println("----------");
        System.out.println(Math.pow(3,2));
        System.out.println(Math.pow(3,-2));
        System.out.println(Math.pow(-2,2));
        System.out.println(Math.pow(-2,-2));
        System.out.println("----------");
        System.out.println(Math.sqrt(4));
        System.out.println(Math.sqrt(16));
        System.out.println("----------");
        for (int i = 0; i <8 ; i++) {
            System.out.println(Math.random());
        }
    }
}
