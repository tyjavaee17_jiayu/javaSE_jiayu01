package zuoye.zuoye02.zuoye0202;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        System.out.println("请输入第一个生日：（yyyy-MM-dd）");
        String birthday1 = sc.nextLine();
        Date new1 = sdf.parse(birthday1);
        if (new1.after(new Date())){
            System.out.println("生日必须早于当前日期！");
            return;
        }
        System.out.println("请输入第二个生日：（yyyy-MM-dd）");
        String birthday2 = sc.nextLine();
        Date new2 = sdf.parse(birthday2);
        if (new2.after(new Date())){
            System.out.println("生日必须早于当前日期！");
        }
        if (new1.before(new2)){
            System.out.println("第一个生日大于第二个生日");
        }else {
            System.out.println("第一个生日小于第二个生日");
        }
    }
}
