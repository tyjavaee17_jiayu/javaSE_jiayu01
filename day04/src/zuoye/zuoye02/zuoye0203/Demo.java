package zuoye.zuoye02.zuoye0203;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Demo {
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY年MM月dd日HH时mm分ss秒");
        String format = sdf.format(new Date());
        System.out.println(format);//2020年03月18日22时53分14秒
    }
}
