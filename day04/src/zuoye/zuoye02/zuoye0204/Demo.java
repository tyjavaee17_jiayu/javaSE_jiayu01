package zuoye.zuoye02.zuoye0204;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Demo {
    public static void main(String[] args) throws ParseException {

        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个日期：（yyyy-MM-dd）");
        String s = sc.nextLine();
        SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(s);
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy年MM月dd日");
        String s1 = sdf1.format(date);
        System.out.println(s1);
    }
}
