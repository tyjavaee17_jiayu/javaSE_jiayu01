package zuoye.zuoye02.zuoye0201;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DemoDate {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入生日：（yyyy-MM-dd）");
        String s = sc.nextLine();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthday = sdf.parse(s);

        Date now = new Date();
        if (birthday.after(now)){
            System.out.println("生日必须早于当前日期");
            return;
        }

        long l = now.getTime() - birthday.getTime();

        long day = l / 1000 / 60 / 60 / 24;

        System.out.println("您已经来到世界："+day+ "天");
    }
}
