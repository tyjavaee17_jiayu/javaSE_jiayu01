package zuoye.zuoye03.zuoye0301;

import java.util.Calendar;

/*
        请编写程序，使用Calendar类获取日历对象，并分别获取年、月、日、小时、分、秒，并将它们打印到控制台。
*/
public class Demo {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        System.out.println(c.get(Calendar.YEAR)+ "年");
        System.out.println(c.get(Calendar.MONTH)+ "月");
        System.out.println(c.get(Calendar.DAY_OF_MONTH)+ "日");
        System.out.println(c.get(Calendar.HOUR)+ "时");
        System.out.println(c.get(Calendar.MINUTE)+ "分");
        System.out.println(c.get(Calendar.SECOND)+ "秒");
    }
}
