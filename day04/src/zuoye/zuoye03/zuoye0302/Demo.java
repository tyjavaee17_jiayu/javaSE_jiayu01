package zuoye.zuoye03.zuoye0302;

import java.util.Calendar;

//请编程，计算并打印“1949年10月1日”那天是星期几？
public class Demo {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        c.set(1949,9,1);
        int i = c.get(Calendar.DAY_OF_WEEK);
        System.out.println("星期"+ (i-1));
    }
}
