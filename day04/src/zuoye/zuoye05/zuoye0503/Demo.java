package zuoye.zuoye05.zuoye0503;

import java.util.ArrayList;

//请定义一个集合：List<Integer>，并循环向集合中添加1千万个数字，请计算这个循环运行的时间，并打印。
public class Demo {
    public static void main(String[] args) {
        ArrayList<Integer>list = new ArrayList<>();
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            list.add(i);
        }
        long end = System.currentTimeMillis();
        long l = end - start;
        long l1 = l / 1000;
        System.out.println(l1);
    }
}
