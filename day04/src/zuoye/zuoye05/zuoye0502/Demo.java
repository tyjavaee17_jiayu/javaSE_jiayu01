package zuoye.zuoye05.zuoye0502;

import java.util.Arrays;

//有以下数组：
//		int[] arr = {55,77,0,88,22,44,33};
//	请编程实现：将后四个数字依次前移一位，移动后，最后一位置为0.
public class Demo {
    public static void main(String[] args) {
        int[] arr1 = {55,77,0,88,22,44,33};
        int[] arr2 = {88,22,44,33};
        System.arraycopy(arr2,0,arr1,2,4);
        arr1[6] = 0;
        System.out.println(Arrays.toString(arr1));
    }
}
