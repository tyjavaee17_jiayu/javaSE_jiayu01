package zuoye.zuoye05.zuoye0501;

import java.util.Arrays;

/*
    有以下数组：
		int[] arr = {10,27,8,5,2,1,3,55,88};
	请定义一个新数组，长度为5，并将arr中几个连续的1位数的数字复制到新数组。
    */
public class Demo {
    public static void main(String[] args) {
        int[] arr1 = {10,27,8,5,2,1,3,55,88};
        int[] arr2 = new int[5];
        System.arraycopy(arr1,2,arr2,0,5);
        System.out.println("新数组：" + Arrays.toString(arr2));
    }
}
