package com.zuoye.zuoye02;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

public class Demo02 {
    public static void main(String[] args) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read("day22\\src\\com\\zuoye\\zuoye01\\Book.xml");
        Element rootElement = document.getRootElement();
        List<Element> elements = rootElement.elements();
        for (Element element : elements) {
            System.out.println(element.getName() + element.attributeValue("name")+element.attributeValue("press"+element.attributeValue("date")+element.attributeValue("price")));
        }
    }
}
