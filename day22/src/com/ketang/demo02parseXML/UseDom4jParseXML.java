package com.ketang.demo02parseXML;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

public class UseDom4jParseXML {
    public static void main(String[] args)throws Exception {
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read("day22/beans.xml");
        Element rootElement = document.getRootElement();
        System.out.println(rootElement.getName());
        List<Element> demo02ElementsList = rootElement.elements();
        for (Element demo02Element : demo02ElementsList) {
            System.out.println("\t"+demo02Element.getName());
            String id = demo02Element.attributeValue("id");
            System.out.println("\t\t元素属性id="+ id);
            String className = demo02Element.attributeValue("className");
            System.out.println("\t\t元素属性className=" + className);
            List<Element> propertyElementList = demo02Element.elements();
            for (Element propertyElement : propertyElementList) {
                System.out.println("\t\t\t"+propertyElement.getName());
                String name = propertyElement.attributeValue("name");
                System.out.println("\t\t\t元素的属性值id="+name);
                propertyElement.attributeValue("className");
                System.out.println("\t\t\t元素的属性值value="+className);
                String text = propertyElement.getText();
                System.out.println("\t\t\t元素的文本="+ text);
            }
        }
    }
}
