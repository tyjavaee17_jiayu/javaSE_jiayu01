package com.ketang.demo02parseXML;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.util.List;

public class Demo02UseXpathAndDom4jparseXML {
    public static void main(String[] args) throws Exception{
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read("day22/beans.xml");

        List<Element> list01 = document.selectNodes("//*");
        for (Element element : list01) {
            System.out.println(element.getName());
        }
        System.out.println("----------------------------");
        List<Element> list02 = document.selectNodes("/beans/bean");
        for (Element element : list02) {
            System.out.println(element.getName()+element.attributeValue("value"));
        }

        System.out.println("----------------------------");
        List<Element> list03 = document.selectNodes("/beans/bean");
        for (Element element : list03) {
            System.out.println(element.getName()+element.attributeValue("id"));
        }
        System.out.println("----------------------------");
        List<Element> list04 = document.selectNodes("//bean");
        for (Element ele : list04) {
            System.out.println(ele.getName()+"\t"+ele.attributeValue("id"));
        }
        System.out.println("----------------------------");
        List<Element> list05 = document.selectNodes("//beans[className]");
        for (Element element : list05) {
            System.out.println(element.getName()+"\t"+element.attributeValue("id"));
        }
    }
}
