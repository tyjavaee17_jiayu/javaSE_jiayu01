package com.zuoye.zuoye01.zuoye0102;

public class ArrayUtils {
    public int getMax(int[] arr){
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            max = arr[i] > max ? arr[i] : max;
        }
        return max;
    }
}
