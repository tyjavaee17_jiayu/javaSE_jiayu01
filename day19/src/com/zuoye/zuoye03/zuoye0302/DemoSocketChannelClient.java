package com.zuoye.zuoye03.zuoye0302;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class DemoSocketChannelClient {
    public static void main(String[] args) throws IOException {
        SocketChannel socketChannel = SocketChannel.open();

        socketChannel.connect(new InetSocketAddress("127.0.0.1",6666));

        socketChannel.write(ByteBuffer.wrap("你好服务器！！！".getBytes()));

        ByteBuffer buffer = ByteBuffer.allocate(1024);
        socketChannel.read(buffer);
        String s = new String(buffer.array(), 0, buffer.limit());
        System.out.println("接收到服务器的信息为：" + s);
    }
}
