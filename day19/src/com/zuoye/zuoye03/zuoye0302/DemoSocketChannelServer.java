package com.zuoye.zuoye03.zuoye0302;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class DemoSocketChannelServer {
    public static void main(String[] args) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();

        serverSocketChannel.configureBlocking(false);

        serverSocketChannel.bind(new InetSocketAddress(6666));

        while (true) {
            SocketChannel socketChannel = serverSocketChannel.accept();

            if ( socketChannel != null) {
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                socketChannel.read(buffer);
                String s = new String(buffer.array(), 0, buffer.limit());
                System.out.println("接收到客户端的信息为：" + s);

                socketChannel.write(ByteBuffer.wrap("收到，你也好！！！".getBytes()));
            }
        }
    }
}
