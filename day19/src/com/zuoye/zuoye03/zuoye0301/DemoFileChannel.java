package com.zuoye.zuoye03.zuoye0301;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class DemoFileChannel {
    public static void main(String[] args) throws IOException {
        RandomAccessFile inRAF = new RandomAccessFile("D:\\Program Files (x86)\\WeChat_C1018.exe", "r");
        RandomAccessFile outRAF = new RandomAccessFile("E:\\WeChat_WeChat_C1018.exe", "rw");
        FileChannel inRAFChannel = inRAF.getChannel();
        FileChannel outRAFChannel = outRAF.getChannel();
        long size = inRAFChannel.size();
        MappedByteBuffer inMap = inRAFChannel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        MappedByteBuffer outMap = outRAFChannel.map(FileChannel.MapMode.READ_WRITE, 0, size);
        for (int i = 0; i < size; i++) {
            byte b = inMap.get(i);
            outMap.put(i,b);
        }
        outRAFChannel.close();
        inRAFChannel.close();
        outRAF.close();
        inRAF.close();
    }
}
