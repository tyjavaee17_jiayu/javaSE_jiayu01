package com.ketang.demo02byteBuffer;

import java.nio.ByteBuffer;

public class Demo03Capacity {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        System.out.println("容量：" + buffer.capacity());

        ByteBuffer buffer1 = ByteBuffer.wrap("我是一个中国人！".getBytes());
        System.out.println(buffer1.capacity());
    }
}
