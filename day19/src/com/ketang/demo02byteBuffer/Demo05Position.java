package com.ketang.demo02byteBuffer;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Demo05Position {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        System.out.println("容量："+buffer.capacity()+"\t限制："+buffer.limit()+"\t位置："+buffer.position());

        System.out.println("--------------------------------");

        buffer.put((byte)11);
        buffer.put((byte)22);
        buffer.put((byte)33);
        buffer.put((byte)44);

        byte[] arr = buffer.array();
        System.out.println(Arrays.toString(arr));
        System.out.println("容量："+buffer.capacity()+"\t限制："+buffer.limit()+"\t位置："+buffer.position());


        System.out.println("--------------------------------");

        buffer.position(2);      //更改当前可写入位置索引
        buffer.put((byte)1);
        buffer.put((byte)2);
        buffer.put((byte)3);
        System.out.println(Arrays.toString(arr));
        System.out.println("容量："+buffer.capacity()+"\t限制："+buffer.limit()+"\t位置："+buffer.position());
    }
}
