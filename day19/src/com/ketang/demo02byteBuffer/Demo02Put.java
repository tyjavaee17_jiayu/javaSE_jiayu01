package com.ketang.demo02byteBuffer;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Demo02Put {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        System.out.println(buffer);
        byte[] arr = buffer.array();
        System.out.println(Arrays.toString(arr));

        System.out.println("-------------------");

        buffer.put((byte)1);
        buffer.put((byte)2);
        buffer.put((byte)3);
        System.out.println(Arrays.toString(arr));

        System.out.println("-------------------");

        byte[] bytes = {11,22,33,44,55,66};
        buffer.put(bytes);
        System.out.println(Arrays.toString(arr));

        System.out.println("-------------------");

        buffer.put(bytes,1,1);
        System.out.println(Arrays.toString(arr));

        System.out.println("-------------------");

        buffer.put(2,(byte)99);
        System.out.println(Arrays.toString(arr));
    }
}
