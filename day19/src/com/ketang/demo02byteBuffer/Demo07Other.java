package com.ketang.demo02byteBuffer;

import java.nio.Buffer;
import java.nio.ByteBuffer;

public class Demo07Other {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put((byte)10);
        buffer.put((byte)20);
        buffer.put((byte)30);
        System.out.println("获取position与limit之间的元素个数："+buffer.remaining());

        System.out.println("--------------------------");

        System.out.println("获取当前缓冲区是否为直接缓冲区:"+ buffer.isDirect());

        System.out.println("--------------------------");

        System.out.println("获取当前缓冲区是否只读。"+buffer.isReadOnly());

        System.out.println("--------------------------");

        buffer.limit(5);
        System.out.println("位置：" + buffer.position() + "\t限制："+ buffer.limit());

        System.out.println("--------------------------");

        /*
            public Buffer clear()：还原缓冲区的状态。
            - 将position设置为：0
            - 将限制limit设置为容量capacity；
            - 丢弃标记mark。
         */
        buffer.clear();
        System.out.println("位置:"+buffer.position()+" 限制"+buffer.limit());

        System.out.println("--------------------------");
        /*
            public Buffer flip()：缩小limit的范围。 获取读取的有效数据0到position之间的数据
            - 将limit设置为当前position位置； [0, 1, 2, 0, 0, 0, 0, 0, 0, 0]  position=3 limit=10=3
            - 将当前position位置设置为0；   position=0 limit=3  new String(bytes,0,len)
            - 丢弃标记。
         */
        Buffer flip = buffer.flip();//获取位置和限制之间存储的数据
        System.out.println(flip);
        System.out.println("位置:"+buffer.position()+" 限制"+buffer.limit());
    }
}
