package com.ketang.demo02byteBuffer;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.Arrays;
/*
    限制-limit
        - public int limit()：获取此缓冲区的限制。
        - public Buffer limit(int newLimit)：设置此缓冲区的限制。
 */
public class Demo04Limit {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        System.out.println("容量：" + buffer.capacity()+"\t限制：" +buffer.limit());

        System.out.println("-----------------------");

        Buffer limit2 = buffer.limit(4);    //4索引之后将不可在使用,包含4
        System.out.println("容量：" + limit2.capacity()+"\t限制：" +limit2.limit());

        System.out.println("-----------------------");

        buffer.put((byte)1);
        buffer.put((byte)1);
        buffer.put((byte)1);
        buffer.put((byte)1);

        byte[] array = buffer.array();
        System.out.println(Arrays.toString(array));
    }
}
