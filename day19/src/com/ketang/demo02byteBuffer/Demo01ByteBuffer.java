package com.ketang.demo02byteBuffer;

import java.nio.ByteBuffer;
/*
    java.nio.ByteBuffer:字节缓冲区,里边封装了一个字节数组
    创建对象的方式:使用静态方法获取
        - public static ByteBuffer allocate(int  capacity)：使用一个“容量”来创建一个“间接字节缓存区”——程序的“堆”空间中创建。
        - public static ByteBuffer allocateDirect(int capacity)：使用一个“容量”来创建一个“直接字节缓存区”——系统内存。
        - public static ByteBuffer wrap(byte[] byteArray)：使用一个“byte[]数组”创建一个“间接字节缓存区”。
 */
public class Demo01ByteBuffer {
    public static void main(String[] args) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(10);//创建间接内存   JVM 堆内存

        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(10);//创建直接内存  系统内存

        ByteBuffer wrap = ByteBuffer.wrap("我爱Java".getBytes());   //创建间接内存  数组
    }
}
