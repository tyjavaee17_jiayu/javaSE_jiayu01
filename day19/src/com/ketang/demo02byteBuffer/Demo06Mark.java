package com.ketang.demo02byteBuffer;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Demo06Mark {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put((byte)10);
        buffer.put((byte)20);
        buffer.mark();
        System.out.println("当前mark标记的位置为："+buffer.position());
        byte[] arr = buffer.array();
        System.out.println(Arrays.toString(arr));

        System.out.println("----------------------------");

        buffer.put((byte)30);
        buffer.put((byte)40);
        buffer.put((byte)50);
        System.out.println("当前的位置为："+buffer.position());
        System.out.println(Arrays.toString(arr));

        System.out.println("----------------------------");

        System.out.println("当前的位置为："+buffer.position());

        buffer.reset();     //设置position的位置为mark标记的位置

        System.out.println("被mark修改的标记位置为："+buffer.position());

        System.out.println("-----------------------------");

        buffer.put((byte)100);
        System.out.println(Arrays.toString(arr));
    }
}
