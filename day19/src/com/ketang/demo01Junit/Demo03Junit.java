package com.ketang.demo01Junit;

import org.junit.*;

public class Demo03Junit {
    @Test
    public void show01(){
        System.out.println("show01方法");
    }
    @Test
    public void show02(){
        System.out.println("show02方法");
    }
    @Test
    public void show03(){
        System.out.println("show03方法");
    }
    @Before
    public void before(){
        System.out.println("before方法");
    }
    @After
    public void after(){
        System.out.println("after方法");
    }
    @BeforeClass
    public static void beforeClass(){
        System.out.println("beforeclass方法");
    }
    @AfterClass
    public static void afterClass(){
        System.out.println("afterclass方法");
    }
}
