package com.ketang.demo03Channel;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class Demo02FileChannel {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        //1.创建读取文件的RandomAccessFile对象,构造方法中封装要读取的数据源和设置只读("r")模式
        RandomAccessFile inRAF = new RandomAccessFile("D:\\Program Files (x86)\\PCQQ2020.exe", "r");
        //2.创建写入文件的RandomAccessFile对象,构造方法中封装要写入的目的地和设置读写("rw")模式
        RandomAccessFile outRAF = new RandomAccessFile("E:\\PCQQ2020.exe", "rw");
        //3.使用读取文件的RandomAccessFile对象中的getChannel方法,获取读取文件的FileChannel对象
        FileChannel inRAFChannel = inRAF.getChannel();
        //4.使用写入文件的RandomAccessFile对象中的getChannel方法,获取写入文件的FileChannel对象
        FileChannel outRAFChannel = outRAF.getChannel();
        //5.使用读取文件的FileChannel对象中的方法size,获取要读取的文件的大小(字节)
        long size = inRAFChannel.size();
        //6.使用读取文件的FileChannel对象中的方法map,创建读取文件的直接缓冲区MappedByteBuffer
        MappedByteBuffer inMap = inRAFChannel.map(FileChannel.MapMode.READ_ONLY, 0, size);
        //7.使用写入文件的FileChannel对象中的方法map,创建写入文件的直接缓冲区MappedByteBuffer
        MappedByteBuffer outMap = outRAFChannel.map(FileChannel.MapMode.READ_WRITE, 0, size);
        //8.创建for循环,循环size次
        for (int i = 0; i < size; i++) {
            //9.使用读取文件的直接缓冲区MappedByteBuffer中的方法get,读取数据源指定索引出的字节
            byte b = inMap.get(i);
            //10.使用写入文件的直接缓冲区MappedByteBuffe中的方法put,把读取到的字节写入到目的地指定的索引处
            outMap.put(i,b);
        }
        //11.释放资源
        outRAFChannel.close();
        inRAFChannel.close();
        outRAF.close();
        inRAF.close();
        long end = System.currentTimeMillis();
        System.out.println("复制文件共用时间："+ (end - start) + "毫秒");
    }
}
