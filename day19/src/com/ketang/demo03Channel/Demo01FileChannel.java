package com.ketang.demo03Channel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Demo01FileChannel {
    public static void main(String[] args) throws IOException {
        //FileChannel的使用步骤(重点):
        // 1.创建FileInputStream对象,构造方法中绑定要读取的数据源
        FileInputStream fis = new FileInputStream("D:\\test1.txt");
        // 2.创建FileOutputStream对象,构造方法中绑定要写入的目的地
        FileOutputStream fos = new FileOutputStream("E:\\test1.txt");
        // 3.使用FileInputStream对象中的方法getChannel,获取读取的FileChannel对象
        FileChannel fisChannel = fis.getChannel();
        // 4.使用FileOutputStream对象中的方法getChannel,获取写入FileChannel对象
        FileChannel fosChannel = fos.getChannel();
        // 5.使用读取的FileChannel对象中的方法read,读取文件
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int len = 0;
        while((len = fisChannel.read(buffer))!= -1){
         // 6.使用写入的FileChannel对象中的方法write,把读取到数据写入到文件中
            //limit设置当前position的位置(1000),position设置0
            //buffer.flip();

            fosChannel.write(buffer);
            //初始化ByteBuffer的状态
            //把position设置为0,将limit设置为容量capacity(1024)
            buffer.clear();
        }
        //7.释放资源
        fosChannel.close();
        fisChannel.close();
        fos.close();
        fis.close();

    }
}
