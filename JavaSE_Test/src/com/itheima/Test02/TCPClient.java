package com.itheima.Test02;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class TCPClient {
    public static void main(String[] args) throws Exception{
        Socket socket = new Socket("127.0.0.1", 6666);
        OutputStream os = socket.getOutputStream();
        System.out.println("请输入文件上传的路径：");
        Scanner sc = new Scanner(System.in);
        String path = sc.next();
        while (true){
            if("D:\\LOL.jpg".equals(path)){
                FileInputStream fis = new FileInputStream(path);
                byte[] bytes = new byte[1024];
                int len = 0;
                while ((len = fis.read(bytes))!= -1){
                    os.write(bytes,0,len);
                }
                socket.shutdownOutput();
                InputStream is = socket.getInputStream();
                while ((len = is.read(bytes))!= -1){
                    System.out.println("服务器的反馈："+new String(bytes,0,len));
                }
                fis.close();
                socket.close();
                break;
            }
        }
    }
}
