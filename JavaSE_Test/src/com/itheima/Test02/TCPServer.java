package com.itheima.Test02;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
    public static void main(String[] args) throws Exception{
        ServerSocket serverSocket = new ServerSocket(6666);
        while (true) {
            Socket socket = serverSocket.accept();
            new Thread(()->{
                try {
                    InputStream is = socket.getInputStream();
                    String fileName = "jiayu" + System.currentTimeMillis() + ".jpg";
                    FileOutputStream fos = new FileOutputStream(fileName);
                    byte[] bytes = new byte[1024];
                    int len = 0;
                    while ((len = is.read(bytes))!= -1){
                        fos.write(bytes,0,len);
                    }
                    OutputStream os = socket.getOutputStream();
                    os.write("文件上传成功！".getBytes());

                   fos.close();
                    socket.close();
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }


    }
}
