package com.itheima.Test01;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws Exception{
        String name = null;
        int ChineseScore = 0;
        int MathScore = 0;
        int EnglishScore = 0;
        ArrayList<Student> list = new ArrayList<>();
        for (int i = 1;i <= 3;i++) {
            System.out.println("请录入第"+ i + "个学生信息：");
            Scanner sc = new Scanner(System.in);
            System.out.println("姓名：");
            name = sc.next();
            System.out.println("语文成绩：");
            ChineseScore = sc.nextInt();
            System.out.println("数学成绩：");
            MathScore = sc.nextInt();
            System.out.println("英语成绩：");
            EnglishScore = sc.nextInt();
            list.add(new Student(name, ChineseScore, MathScore, EnglishScore));
        }
        Collections.sort(list, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return (o2.getChineseScore()+o2.getMathScore()+o2.getEnglishScore())-(o1.getChineseScore()+o1.getMathScore()+o1.getEnglishScore());
            }
        });
        //System.out.println(list);
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("JavaSE_Test\\a.txt"));
        bufferedWriter.write("姓名，语文成绩，数学成绩，英语成绩");
        bufferedWriter.newLine();
        for (Student student : list) {
            bufferedWriter.write(student.getName()+","+student.getChineseScore()+","+student.getMathScore()+","+student.getEnglishScore());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        }
        System.out.println("数据写入文件完毕");
        BufferedReader br = new BufferedReader(new FileReader("JavaSE_Test\\a.txt"));
        char[] chars = new char[1024];
        int len = 0;
        while ((len = br.read(chars))!=-1){
            System.out.println(new String(chars,0,len));
        }
        br.close();
        bufferedWriter.close();
    }
}
