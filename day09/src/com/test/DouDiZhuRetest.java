package com.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DouDiZhuRetest {
    public static void main(String[] args) {
        HashMap<Integer,String> poker = new HashMap<>();

        ArrayList<Integer> pokerIndex = new ArrayList<>();

        int index = 0;

        String[] number = {"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
        String[] color = {"♠","♥","♣","♦"};

        /*ArrayList<String> number = new ArrayList<>();
        ArrayList<String> color = new ArrayList<>();
        Collections.addAll(number,"2","A","K","Q","J","10","9","8","7","6","5","4","3");
        Collections.addAll(color,"♠","♥","♣","♦");*/

        poker.put(index,"大王");
        pokerIndex.add(index);//为牌添加对应索引
        index++;

        poker.put(index,"小王");
        pokerIndex.add(index);//为牌添加对应索引
        index++;

        for (String num : number) {
            for (String col : color) {
                poker.put(index,col+num);
                pokerIndex.add(index);//为牌添加对应索引
                index++;
            }
        }
        //System.out.println(poker);
        //System.out.println(pokerIndex);
        Collections.shuffle(pokerIndex);
        //System.out.println(pokerIndex);
        //System.out.println(poker);
        ArrayList<Integer> player01 = new ArrayList<>();//发的牌其实是扑克的索引
        ArrayList<Integer> player02 = new ArrayList<>();//发的牌其实是扑克的索引
        ArrayList<Integer> player03 = new ArrayList<>();//发的牌其实是扑克的索引
        ArrayList<Integer> dipai = new ArrayList<>();//发的牌其实是扑克的索引

        for (int i = 0; i < pokerIndex.size(); i++) {
            Integer pai = pokerIndex.get(i);
            if (i >= 51){
                dipai.add(pai);
            }else if(i % 3 == 0){
                player01.add(pai);
            }else if(i % 3 == 1){
                player02.add(pai);
            }else if(i % 3 == 2){
                player03.add(pai);
            }
        }

        Collections.sort(player01);
        Collections.sort(player02);
        Collections.sort(player03);
        Collections.sort(dipai);

        lookPoker("刘德华",player01,poker);
        lookPoker("周润发",player02,poker);
        lookPoker("周星驰",player03,poker);
        lookPoker("底牌",dipai,poker);


        //System.out.println("刘德华："+player01+poker);
        //System.out.println("周润发："+player02+poker);
        //System.out.println("周星驰："+player03+poker);
        //System.out.println("底牌："+dipai+poker);
    }



        //为了只看牌，不看玩家的牌序列
    public static void lookPoker(String name,ArrayList<Integer> list,HashMap<Integer,String> poker){
        //打印玩家名称,不换行
        System.out.print(name+": ");
        //遍历玩家|底牌集合,获取Map集合的每一个key
        for (Integer key : list) {
            //根据key找到value
            String value = poker.get(key);
            //打印查找到的牌,不换行
            System.out.print(value+", ");
        }
        //打印完每一个玩家|底牌的牌,换行
        System.out.println();
    }
}
