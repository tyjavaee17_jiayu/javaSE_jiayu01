package com.test;

import java.util.HashMap;
import java.util.Map;

public class MapPractice {
    public static void main(String[] args) {
        String s = "aaabbbcccdddeeefff";
        Map<Character,Integer> map = new HashMap<>();
        char[] chars = s.toCharArray();
        for (char c : chars) {
              /* if (map.containsKey(c)){
                   Integer integer = map.get(c);
                   integer++;
                   map.put(c,integer);
               }else{
                    map.put(c,1);*/
            Integer orDefault = map.getOrDefault(c, 0);
            map.put(c,++orDefault);
        }
        System.out.println(map);
    }
}

