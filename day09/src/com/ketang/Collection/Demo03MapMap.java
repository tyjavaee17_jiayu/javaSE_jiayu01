package com.ketang.Collection;

import java.util.HashMap;
import java.util.Set;

public class Demo03MapMap {
    public static void main(String[] args) {
        HashMap<String,Integer> map1 = new HashMap<>();
        map1.put("小红",15);
        map1.put("小黄",16);
        map1.put("小绿",17);
        HashMap<String,Integer> map2 = new HashMap<>();
        map2.put("小黑",18);
        map2.put("小兰",19);
        map2.put("小小",20);
        HashMap<String,HashMap<String,Integer>> allMap = new HashMap();
        allMap.put("S001",map1);
        allMap.put("s002",map2);
        Set<String> numberSet = allMap.keySet();
        for (String number : numberSet) {
            HashMap<String, Integer> idMap = allMap.get(number);
            Set<String> nameSet = idMap.keySet();
            for (String name : nameSet) {
                Integer age = idMap.get(name);
                System.out.println(number+"\t"+name+"\t"+age);
            }
        }
    }
}
