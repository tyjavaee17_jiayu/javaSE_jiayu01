package com.ketang.Collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Demo02ListMap {
    public static void main(String[] args) {
        HashMap<String,Integer> map1 = new HashMap<>();
        map1.put("小红",18);
        map1.put("小明",19);
        map1.put("小黑",20);
        HashMap<String,Integer> map2 = new HashMap<>();
        map2.put("小兰",15);
        map2.put("小香",13);
        map2.put("小小",11);
        ArrayList<HashMap<String,Integer>> list = new ArrayList<>();
        list.add(map1);
        list.add(map2);
        for (HashMap<String, Integer> hashMap : list) {
            Set<String> set = hashMap.keySet();
            System.out.println("---------------");
            for (String key : set) {
                Integer integer = hashMap.get(key);
                System.out.println(key+"\t"+integer);
            }
        }
    }
}
