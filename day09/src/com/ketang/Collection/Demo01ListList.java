package com.ketang.Collection;

import java.util.ArrayList;

public class Demo01ListList {
    public static void main(String[] args) {
        ArrayList<String> list01 = new ArrayList<>();
        list01.add("aaa");
        list01.add("bbb");
        list01.add("ccc");
        list01.add("ddd");
        ArrayList<String> list02 = new ArrayList<>();
        list02.add("xxx");
        list02.add("yyy");
        list02.add("zzz");
        list02.add("qqq");
        ArrayList<ArrayList<String>> allList = new ArrayList<>();
        allList.add(list01);
        allList.add(list02);
        for (ArrayList<String> list : allList) {
            for (String zimu : list) {
                System.out.println(zimu);
            }
        }
    }
}
