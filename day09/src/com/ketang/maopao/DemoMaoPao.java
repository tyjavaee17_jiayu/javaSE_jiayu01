package com.ketang.maopao;

import java.util.Arrays;

public class DemoMaoPao {
    public static void main(String[] args) {
        int[] arr = {2,8,7,9,5,4,6};
        System.out.println("排序前数组的元素：" + Arrays.toString(arr));
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length-1-i; j++) {
               if (arr[j] > arr[j+1]){
                   int temp = arr[j];
                   arr[j] = arr[j+1];
                   arr[j+1] = temp;
               }
            }
        }
        System.out.println("排序后数组的元素：" + Arrays.toString(arr));
    }
}
