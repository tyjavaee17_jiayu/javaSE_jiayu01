package com.ketang.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class DouDiZhu {
    public static void main(String[] args) {
        HashMap<Integer,String> poker = new HashMap<>();
        ArrayList<Integer> pokerIndex = new ArrayList<>();
        int index = 0;
        ArrayList<String> colors = new ArrayList<>();
        ArrayList<String> numbers = new ArrayList<>();
        Collections.addAll(numbers,"2","A","K","Q","J","10","9","8","7","6","5","4","3");
        Collections.addAll(colors,"♠","♥","♣","♦");
        poker.put(index,"大王");
        pokerIndex.add(index);
        index++;
        poker.put(index,"小王");
        pokerIndex.add(index);
        index++;
        for (String number : numbers) {
            for (String color : colors) {
                poker.put(index,color + number);
                pokerIndex.add(index);
                index++;
            }
        }
        Collections.shuffle(pokerIndex);
        ArrayList<Integer> player01 = new ArrayList<>();
        ArrayList<Integer> player02 = new ArrayList<>();
        ArrayList<Integer> player03 = new ArrayList<>();
        ArrayList<Integer> dipai = new ArrayList<>();
        for (int i = 0; i < pokerIndex.size(); i++) {
            Integer pi = pokerIndex.get(i);
            if (i >= 51){
                dipai.add(pi);
            }else if(i%3 == 0){
                player01.add(pi);
            }else if(i%3 == 1){
                player02.add(pi);
            }else if(i%3 == 2){
                player03.add(pi);
            }
        }
        Collections.sort(player01);
        Collections.sort(player02);
        Collections.sort(player03);
        Collections.sort(dipai);

        lookPoker("小红",player01,poker);
        lookPoker("小明",player02,poker);
        lookPoker("小兰",player03,poker);
        lookPoker("底牌",dipai,poker);

    }
    public static void lookPoker(String name,ArrayList<Integer> list,HashMap<Integer,String> poker){
        System.out.print(name+":");
        for (Integer key : list) {
            String value = poker.get(key);
            System.out.print(value+":" );
        }
        System.out.println();
    }
}
