package com.ketang.Map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Demo02Map {
    public static void main(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        map.put("小明",15);
        map.put("小红",12);
        map.put("小兰",17);
        map.put("小黑",16);
        Set<String> keySet = map.keySet();
        System.out.println("-------------------");
        for (String key : keySet) {
            Integer value = map.get(key);
            System.out.println(key+"\t"+value);
        }
        System.out.println("-------------------");
        Iterator<String> it = keySet.iterator();
        while(it.hasNext()){
            String key = it.next();
            Integer value = map.get(key);
            System.out.println(key+"\t"+value);
        }
        System.out.println("-------------------");

    }
}
