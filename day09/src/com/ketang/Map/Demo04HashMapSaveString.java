package com.ketang.Map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Demo04HashMapSaveString {
    public static void main(String[] args) {
        Map<Person,String> map = new HashMap<>();
        map.put(new Person("小红",18),"中国");
        map.put(new Person("小明",18),"美国");
        map.put(new Person("小兰",18),"韩国");
        map.put(new Person("小黑",18),"英国");
        Set<Person> person = map.keySet();
        for (Person person1 : person) {
            String country = map.get(person1);
            System.out.println(person1+"\t"+country);
        }
        System.out.println("-------------------");
        Map<Person,String> map1 = new HashMap<>();
        map1.put(new Person("小红",18),"中国");
        map1.put(new Person("小明",18),"美国");
        map1.put(new Person("小兰",18),"韩国");
        map1.put(new Person("小黑",18),"英国");
        map1.put(new Person("小黑",18),"法国");
        Set<Person> person1 = map1.keySet();
        for (Person person2 : person1) {
            String country = map1.get(person2);
            System.out.println(person2+"\t"+country);
        }
    }
}
