package com.ketang.Map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Demo03Map {
    public static void main(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        map.put("小红",18);
        map.put("小明",19);
        map.put("小兰",20);
        map.put("小黑",21);
        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
        for (Map.Entry<String, Integer> stringIntegerEntry : entrySet) {
            String key = stringIntegerEntry.getKey();
            Integer value = stringIntegerEntry.getValue();
            System.out.println(key+"\t"+value);
        }
        System.out.println("-------------------");
        Iterator<Map.Entry<String, Integer>> it = entrySet.iterator();
        while (it.hasNext()){
            Map.Entry<String, Integer> stringIntegerEntry = it.next();
            String key = stringIntegerEntry.getKey();
            Integer value = stringIntegerEntry.getValue();
            System.out.println(key+"\t"+value);
        }
    }
}
