package com.ketang.Map;

import java.util.Comparator;
import java.util.TreeMap;

public class Demo06TreeMap {
    public static void main(String[] args) {
        TreeMap<Integer,String> map1 = new TreeMap<>();
        map1.put(1,"aaa");
        map1.put(4,"ddd");
        map1.put(2,"bbb");
        map1.put(5,"eee");
        map1.put(3,"ccc");
        System.out.println(map1);
        System.out.println("--------------------");
        TreeMap<Integer,String> map2 = new TreeMap<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int i = o2 - o1;
                return i;
            }
        });
        map2.put(1,"aaa");
        map2.put(4,"ddd");
        map2.put(2,"bbb");
        map2.put(5,"eee");
        map2.put(3,"ccc");
        System.out.println(map2);
    }
}
