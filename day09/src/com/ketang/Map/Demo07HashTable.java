package com.ketang.Map;
/*
    java.util.Hashtable<k,v>集合 implements Map<k,v>接口
        此类实现一个哈希表，该哈希表将键映射到相应的值。任何非 null 对象都可以用作键或值。
    HashMap集合的特点:
        1.HashMap是JDK1.2之后出现的
        2.HashMap允许存储null键和null值
        3.HashMap集合是不同步,效率高(不安全)
        4.HashMap集合底层是一个哈希表
    Hashtable集合的特点:
        1.Hashtable是JDK1.0时期的出现的(最早期的双列集合)
        2.Hashtable是不允许存储null键和null值（会报空指针异常）
        3.Hashtable集合是同步,效率低(安全)
        4.Hashtable集合底层是一个哈希表
   Hashtable集合效率没有HashMap高,所以已经被淘汰了
   但是Hashtable集合的子类Properties集合依然活跃在历史的舞台
 */
public class Demo07HashTable {
    public static void main(String[] args) {

    }
}
