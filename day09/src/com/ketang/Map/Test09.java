package com.ketang.Map;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Test09 {
    public static void main(String[] args) {
        System.out.println("请输入一个字符串：");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Map<Character,Integer> map = new HashMap<>();
        char[] arr = s.toCharArray();
        for (char c : arr) {
            Integer value = map.getOrDefault(c, 0);
            map.put(c,++value);
        }
        System.out.println(map);
    }
}
