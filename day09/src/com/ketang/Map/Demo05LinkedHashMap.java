package com.ketang.Map;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class Demo05LinkedHashMap {
    public static void main(String[] args) {
        HashMap<String,String> map1 = new HashMap<>();
        map1.put("aaa","111");
        map1.put("bbb","222");
        map1.put("ccc","333");
        map1.put("ddd","444");
        System.out.println(map1);
        System.out.println("----------------");
        LinkedHashMap<String,String> map2 = new LinkedHashMap<>();
        map2.put("aaa","111");
        map2.put("bbb","222");
        map2.put("ccc","333");
        map2.put("ddd","444");
        System.out.println(map2);
    }
}
