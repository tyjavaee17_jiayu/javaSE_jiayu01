package com.ketang.Map;

import java.util.HashMap;
import java.util.Map;

public class Demo01Map {
    public static void main(String[] args) {
        Map<String,Integer> map1 = new HashMap<>();
        map1.put("小明",18);
        map1.put("小红",20);
        map1.put("小兰",15);
        map1.put("小黑",22);
        System.out.println(map1.size());
        System.out.println(map1);
        System.out.println("---------------");
        boolean b1 = map1.containsKey("小红");
        System.out.println(b1);
        boolean b2 = map1.containsKey("小小");
        System.out.println(b2);
        boolean b3 = map1.containsValue(15);
        System.out.println(b3);
        boolean b4 = map1.containsValue(19);
        System.out.println(b4);
        System.out.println("----------------");
        Integer i1 = map1.get("小红");
        System.out.println(i1);
        Integer i2 = map1.get("小小");
        System.out.println(i2);
        System.out.println("----------------");
        Integer i3 = map1.remove("小黑");
        System.out.println(i3);
        Integer i4 = map1.remove("小小");
        System.out.println(i4);
        System.out.println(map1);
        System.out.println("----------------");
        Map<String,String> map2 = new HashMap<>();
        map2.put("杨过","小龙女");
        map2.put("郭靖","黄蓉");
        map2.put("尹志平","小龙女");
        System.out.println(map2.put("黄晓明","杨颖"));
        System.out.println(map2);
        System.out.println(map2.put("黄晓明", "赵薇"));
        System.out.println(map2);
    }
}
