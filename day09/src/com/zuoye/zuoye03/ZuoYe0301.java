package com.zuoye.zuoye03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class ZuoYe0301 {
    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list,"a","f","b","c","a","d");
        HashSet<String> set = new HashSet<>();
        set.addAll(list);
        System.out.println(set);
    }
}
