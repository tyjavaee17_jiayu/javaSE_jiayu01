package com.zuoye.zuoye01;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ZuoYe0101 {
    public static void main(String[] args) {
        HashMap<Integer,String> map = new HashMap<>();
        map.put(1,"张三");
        map.put(2,"李四");
        map.put(1,"王五");
        System.out.println(map.size());
        System.out.println("-------------");
        Set<Integer> set = map.keySet();
        for (Integer num : set) {
            String name = map.get(num);
            System.out.println(num + "\t" + name);
        }
        System.out.println("-------------");
        Set<Map.Entry<Integer, String>> entries = map.entrySet();
        for (Map.Entry<Integer, String> entry : entries) {
            Integer key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + "\t" + value);
        }
        System.out.println("-------------");
        String s1 = map.get(1);
        System.out.println(s1);
        String s2 = map.get(10);
        System.out.println(s2);
        System.out.println("-------------");
        boolean b = map.containsKey(10);
        System.out.println(b);
        System.out.println("-------------");
        map.remove(1);
        System.out.println(map);
    }
}
