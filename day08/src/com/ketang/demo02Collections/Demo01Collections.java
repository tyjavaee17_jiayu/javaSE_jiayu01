package com.ketang.demo02Collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Demo01Collections {
    public static void main(String[] args) {
        ArrayList<Integer> list01 = new ArrayList<>();
        list01.add(1);
        list01.add(8);
        list01.add(6);
        list01.add(4);
        list01.add(2);
        System.out.println(list01);
        System.out.println("---------------");

        Collections.sort(list01);
        System.out.println(list01);
        System.out.println("---------------");
        Collections.sort(list01, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int i = o2 - o1;//降序
                return i;
            }
        });
        System.out.println(list01);
        System.out.println("----------------");

        Collections.sort(list01, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int i = o1 - o2;//升序
                return i;
            }
        });
        System.out.println(list01);
        System.out.println("----------------");
        Collections.shuffle(list01);
        System.out.println(list01);

        System.out.println("----------------");

        //自然顺序: 编码表的顺序ASCII  0:48     A:65    a:9
        ArrayList<String> list02 = new ArrayList<>();
        list02.add("ab");
        list02.add("12");
        list02.add("ac");
        list02.add("AB");
        list02.add("AC");
        list02.add("CC");
        Collections.sort(list02);
        System.out.println(list02);

    }
}
