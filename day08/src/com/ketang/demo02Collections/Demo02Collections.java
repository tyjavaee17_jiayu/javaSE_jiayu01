package com.ketang.demo02Collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Demo02Collections {
    public static void main(String[] args) {
        ArrayList<Person> list = new ArrayList<>();
        list.add(new Person("zhangsan",18));
        list.add(new Person("lisi",20));
        list.add(new Person("wangwu",21));
        list.add(new Person("zhaoliu",19));
        list.add(new Person("tianqi",22));
        list.add(new Person("zhangsan",18));

        Collections.sort(list, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                int i = o1.getAge() - o2.getAge();
                return i;
            }
        });
        System.out.println(list);

        System.out.println("-----------------");
        Collections.sort(list, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                int i = o1.getAge() - o2.getAge();
                if (i == 0){
                     i = o1.getName().charAt(0) - o2.getName().charAt(0);
                }
                return i;
            }
        });
        System.out.println(list);
    }
}
