package com.ketang.demo02Collections;

import java.util.Arrays;

/*
    可变参数
        是JDK1.5之后出现的新特性
    作用:
        当我们定义一个方法的时候,方法的参数类型已经明确了,但是参数的个数不确定,就可以使用可变参数
    格式:
        修饰符 返回值类型 方法名(数据类型...变量名){
            方法体;
        }
     数据类型...变量名==>可变参数:代表形式参数可以接收任意个参数
        调用参数是可变参数的方法,参数的个数可以传递任意个(不传递,1,2,3,4...n...)
     原理:



        可变参数底层就是一个数组,传递不同个数的参数,就会创建不同长度的数组,来接收这些参数



 */
public class Demo03VarArgs {
    public static void main(String[] args) {
        int sum01 = getSum();
        System.out.println(sum01);
        int sum02 = getSum(1, 2, 6);
        System.out.println(sum02);
        System.out.println("--------------");
        Integer[] integers = method(1, 2, 3);
        System.out.println(Arrays.toString(integers));
        String[] strings = method("a", "b", "c", "d");
        System.out.println(Arrays.toString(strings));
    }

    /*
        可变参数的注意事项
        1.一个方法的参数列表只能写一个可变参数
        2.方法的参数列表有多个参数,可变参数必须写在末尾
        可变参数终极写法
        public static void method(Object...obj){}
     */
    public static int getSum(int... arr){
        int sum = 0;
        for (int a : arr){
            sum += a;
        }
        return sum;
    }
    public static <E> E[]method(E...e){
        return e;
    }
}
