package com.ketang.demo01List;

import java.util.LinkedList;
import java.util.List;

/*
    java.util.LinkedList<E> implements List<E>接口
    List 接口的链接列表实现。
    LinkedList集合底层是一个双向链表:查询慢,增删快
    双向:是一个有序的集合
    LinkedList集合中有一些操作首尾元素的特有方法:
        public void addFirst(E e) :将指定元素插入此列表的开头。
        public void push(E e) :将元素推入此列表所表示的堆栈。
        public void addLast(E e) :将指定元素添加到此列表的结尾。

        public E getFirst() :返回此列表的第一个元素。
        public E getLast() :返回此列表的最后一个元素。

        public E removeFirst() :移除并返回此列表的第一个元素。
        public E pop() :从此列表所表示的堆栈处弹出一个元素。
        public E removeLast() :移除并返回此列表的最后一个元素。
        public boolean isEmpty() ：如果列表不包含元素，则返回true。
  注意:
    使用LinkedList集合特有的方法,不能使用多态创建集合对象
        List<String> list = new LinkedList<>(); 弊端:不能使用子类特有的功能
        Collection<String> list = new LinkedList<>(); 弊端:不能使用子类特有的功能
 */
public class Demo02LinkedList {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        list.add("aaa");
        list.add("ddd");
        list.add("ccc");
        list.add("bbb");
        list.add("eee");
        System.out.println(list);
        System.out.println("--------------");
        list.addFirst("111");
        list.addLast("222");
        System.out.println(list);
        System.out.println("--------------");
        list.push("333");
        System.out.println(list);
        System.out.println("--------------");
        String first = list.getFirst();
        System.out.println(first);
        String last = list.getLast();
        System.out.println(last);
        System.out.println("--------------");
        String removeFirst = list.removeFirst();
        System.out.println(removeFirst);
        System.out.println(list);
        String removeLast = list.removeLast();
        System.out.println(removeLast);
        System.out.println(list);
        System.out.println("--------------");
        String pop = list.pop();
        System.out.println(pop);
        System.out.println(list);
        System.out.println("--------------");
        boolean listEmpty = list.isEmpty();
        System.out.println(listEmpty);
        System.out.println("--------------");

        list.clear();

        boolean listEmpty1 = list.isEmpty();
        System.out.println(listEmpty1);
    }

}
