package com.ketang.demo01List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Demo01List {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        list.add("ddd");
        list.add("eee");
        System.out.println(list);
        System.out.println("---------------");
        list.add(4,"xixi");
        System.out.println(list);
        System.out.println("---------------");
        String s1 = list.get(3);
        System.out.println("s1 = "+s1);
        String s2 = list.get(0);
        System.out.println("s2 = "+s2);
        System.out.println(list);
        System.out.println("---------------");
        String s3 = list.remove(4);
        System.out.println("s3 = " + s3);
        System.out.println(list);
        System.out.println("---------------");
        String s4 = list.set(1, "哈哈");
        System.out.println("s4 = " + s4);
        System.out.println(list);
        System.out.println("---------------");
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i);
            System.out.println(s);
        }
        System.out.println("---------------");
        for (String s : list) {
            System.out.println(s);
        }
        System.out.println("---------------");
        Iterator<String> it = list.iterator();
        while(it.hasNext()){
            String s = it.next();
            System.out.println(s);
        }
    }
}
