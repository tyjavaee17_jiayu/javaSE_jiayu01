package com.ketang.demo03Set;

import java.util.HashSet;
import java.util.Iterator;

/*
    java.util.HashSet<E>集合 implements Set<E>接口
        此类实现 Set 接口，由哈希表（实际上是一个 HashMap 实例）支持。
        它不保证 set 的迭代顺序；特别是它不保证该顺序恒久不变。此类允许使用 null 元素。
    HashSet特点:
        1.不允许存储重复的元素
        2.不包含带索引的方法(不能使用普通的for循环遍历Set集合)
        3.是一个无序的集合(存储的元素和取出的元素顺序有可能不一致)
        4.底层是一个哈希表
            JDK1.8之前:数组+单向链表
            JDK1.8之后:数组+单向链表|数组+红黑树(可以提高查询的效率)
 */
public class DemoHashSet {
    public static void main(String[] args) {
        HashSet<String> hs = new HashSet<>();
        hs.add("aa");
        hs.add("bb");
        hs.add("cc");
        hs.add("dd");
        hs.add("aa");
        for (String s:hs) {
            System.out.println(s);
        }
        System.out.println("---------------");
        Iterator<String> it = hs.iterator();
        while (it.hasNext()){
            String s = it.next();
            System.out.println(s);
        }
    }
}
