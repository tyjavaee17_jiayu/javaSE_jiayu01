package com.ketang.demo03Set;

public class DemoHashCode {
    public static void main(String[] args) {
        Person p1 = new Person();
        int h1 = p1.hashCode();
        System.out.println(h1);
        Person p2 = new Person();
        int h2 = p2.hashCode();
        System.out.println(h2);
        System.out.println(p1 == p2);
    }
}
