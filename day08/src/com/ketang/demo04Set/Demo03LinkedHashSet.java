package com.ketang.demo04Set;

import java.util.HashSet;
import java.util.LinkedHashSet;
/*
    java.util.LinkedHashSet<E> extends HashSet<E>
        具有可预知迭代顺序的 Set 接口的哈希表和链接列表实现。
        此实现与 HashSet 的不同之外在于，后者维护着一个运行于所有条目的双重链接列表。
    LinkedHashSet集合特点:
        1.不允许存储重复的元素
        2.没有带索引的方法
        3.底层是哈希表+单向链表
            JDK1.8之前:数组+单向链表+单向链表
            JDK1.8之后:数组+单向链表|红黑树+单向链表
            结构就是一个双向链表,可以保证迭代的顺序,是一个有序的集合
 */
public class Demo03LinkedHashSet {
    public static void main(String[] args) {
        HashSet<String> hs = new HashSet<>();
        hs.add("aaa");
        hs.add("bbb");
        hs.add("ccc");
        hs.add("ddd");
        hs.add("eee");
        System.out.println(hs);//[aaa, ccc, bbb, eee, ddd]

        System.out.println("---------------");
        LinkedHashSet<String> lkhs = new LinkedHashSet<>();
        lkhs.add("aaa");
        lkhs.add("bbb");
        lkhs.add("ccc");
        lkhs.add("ddd");
        lkhs.add("eee");
        lkhs.add("ccc");
        System.out.println(lkhs);//[aaa, bbb, ccc, ddd, eee]
    }
}
