package com.ketang.demo04Set;

import java.util.HashSet;

public class Demo02HashSetSavePerson {
    public static void main(String[] args) {
        HashSet<Person> hsp = new HashSet<>();
        Person s1 = new Person("a", 18);
        Person s2 = new Person("a", 18);
        System.out.println(s1.hashCode());//3025
        System.out.println(s2.hashCode());//3025
        hsp.add(s1);
        hsp.add(s2);
        hsp.add(new Person("b",11));
        hsp.add(new Person("c",12));
        hsp.add(new Person("d",13));
        hsp.add(new Person("e",14));
        hsp.add(new Person("f",15));
        System.out.println(hsp.size());
        for (Person person : hsp) {
            System.out.println(person);
        }
    }
}
