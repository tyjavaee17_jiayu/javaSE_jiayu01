package com.ketang.demo04Set;

import java.util.Comparator;
import java.util.TreeSet;

/*
    java.util.TreeSet<E>集合 implements Set<E>接口
        基于Set接口的红黑树的实现
        使用元素的自然顺序对元素进行排序(内部会使用Comparator比较器对元素进行默认的升序排序)，
        或者根据创建 set 时提供的 Comparator 进行排序，具体取决于使用的构造方法。
   构造方法:
        TreeSet() 构造一个新的空 set，该 set 根据其元素的自然顺序进行排序。
        TreeSet(Comparator<? super E> comparator) 构造一个新的空 TreeSet，它根据指定比较器进行排序。
   TreeSet集合的特点:
        1.不允许存储重复的元素
        2.没有带索引的方法
        3.底层是一个红黑树
        4.可以根据比较器产生的规则对元素进行排序
 */
public class Demo04TreeSet {
    public static void main(String[] args) {
        TreeSet<Integer> ts1 = new TreeSet<>();
        ts1.add(12);
        ts1.add(158);
        ts1.add(23);
        ts1.add(-10);
        ts1.add(45);
        ts1.add(98);
        System.out.println(ts1);
        System.out.println("-----------------");
        TreeSet<Integer> ts2 = new TreeSet<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int i = o1 - o2;
                return i;
            }
        });
        ts2.add(100);
        ts2.add(40);
        ts2.add(54);
        ts2.add(68);
        ts2.add(71);
        System.out.println(ts2);
    }
}
