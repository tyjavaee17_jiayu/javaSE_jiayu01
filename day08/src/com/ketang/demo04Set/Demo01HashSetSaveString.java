package com.ketang.demo04Set;

import java.util.HashSet;

public class Demo01HashSetSaveString {
    public static void main(String[] args) {
        HashSet<String> hs = new HashSet<>();
        String s1 = new String("abc");
        String s2 = new String("abc");
        hs.add(s1);
        hs.add(s2);
        hs.add("重地");
        hs.add("通话");
        hs.add("abc");
        System.out.println(hs);
    }
}
