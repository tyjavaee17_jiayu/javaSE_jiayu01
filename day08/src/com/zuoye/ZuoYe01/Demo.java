package com.zuoye.ZuoYe01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(51);
        list.add(23);
        list.add(78);
        list.add(69);
        Collections.shuffle(list);
        System.out.println(list);
        System.out.println("------------------");
        Collections.sort(list);
        System.out.println(list);
        System.out.println("------------------");
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int i = o2 - o1;
                return i;
            }
        });
        System.out.println(list);
    }
}
