package com.zuoye.ZuoYe02.zuoye0203;

import java.util.LinkedHashSet;

public class Demo {
    public static void main(String[] args) {
        LinkedHashSet<Integer> lhs= new LinkedHashSet<>();
        lhs.add(20);
        lhs.add(30);
        lhs.add(50);
        lhs.add(10);
        lhs.add(30);
        lhs.add(20);
        System.out.println(lhs.size());
        System.out.println(lhs);
        System.out.println("---------------");
        for (Integer lh : lhs) {
            if (lh > 25){
                System.out.println(lh);
            }
        }
    }
}
