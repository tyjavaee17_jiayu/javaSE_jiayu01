package com.zuoye.ZuoYe02.zuoye0202;

import java.util.HashSet;

public class Demo0202 {
    public static void main(String[] args) {
        HashSet<Student> hs = new HashSet<>();
        Student s1 = new Student("张三","男",20);
        Student s2 = new Student("李四","女",21);
        Student s3 = new Student("张三","男",20);
        hs.add(s1);
        hs.add(s2);
        hs.add(s3);
        for (Student h : hs) {
            System.out.println(h);
        }
    }
}
