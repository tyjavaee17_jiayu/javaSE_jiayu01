package com.zuoye.ZuoYe02.zuoye0201;

import java.util.HashSet;
import java.util.Iterator;

public class Demo0201 {
    public static void main(String[] args) {
        HashSet<String> hs = new HashSet<>();
        hs.add("刘备");
        hs.add("关羽");
        hs.add("张飞");
        hs.add("刘备");
        hs.add("张飞");
        int size = hs.size();
        System.out.println(size);
        System.out.println("---------------");
        Iterator<String> it = hs.iterator();
        while(it.hasNext()){
            String s = it.next();
            System.out.println(s);
        }
        System.out.println("---------------");
        for (String h : hs) {
            System.out.println(h);
        }
    }
}
