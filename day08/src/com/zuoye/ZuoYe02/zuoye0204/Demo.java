package com.zuoye.ZuoYe02.zuoye0204;

import java.util.TreeSet;

public class Demo {
    public static void main(String[] args) {
        TreeSet<Integer> ts = new TreeSet<>();
        ts.add(30);
        ts.add(20);
        ts.add(50);
        ts.add(10);
        ts.add(30);
        ts.add(20);
        System.out.println(ts.size());
        System.out.println(ts);
        System.out.println("--------------");
        for (Integer t : ts) {
            if (t > 25){
                System.out.println(t);
            }
        }
    }
}
