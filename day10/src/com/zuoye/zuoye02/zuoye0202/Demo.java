package com.zuoye.zuoye02.zuoye0202;

import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        String[] arr = {"星期一","星期二","星期三","星期四","星期五","星期六","星期日"};
        System.out.println("请输入一个整数1--7的值：");
        int num = new Scanner(System.in).nextInt();
        try {
            String week = arr[num - 1];
            System.out.println(week);
        } catch (Exception e) {
                System.out.println("输入错误！");
        }
    }
}
