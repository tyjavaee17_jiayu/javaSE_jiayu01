package com.zuoye.zuoye02.zuoye0201;

import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        System.out.println("请输入第一个整数：");
        int in1 = new Scanner(System.in).nextInt();
        System.out.println("请输入第二个整数：");
        int in2 = new Scanner(System.in).nextInt();
        try {
            int zhi = in1 / in2;
            System.out.println(zhi);
        } catch (Exception e) {
            System.out.println("第二个数不能为0！");
        }
    }
}
