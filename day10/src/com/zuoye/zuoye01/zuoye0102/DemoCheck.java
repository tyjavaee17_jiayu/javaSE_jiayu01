package com.zuoye.zuoye01.zuoye0102;

import java.util.Arrays;

public class DemoCheck {
    public static void main(String[] args) {
        int[] arr = {431,54,25,25,26,45,2,4,65,3,64,6,46,7,54};
        //冒泡排序
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length-1-i; j++) {
                if (arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j + 1 ];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        int index = checkElement(arr, 2);
        System.out.println(index);
        System.out.println("----------------");
        int index1 = checkElement(arr, 200);
        System.out.println(index1);
    }
    //二分查找法
    public static int checkElement(int[] arr, int number){
        int min = 0;
        int max = arr.length-1;
        while (min <= max){
            int mid = (min + max)/2;
            if (number > arr[mid]){
                min = mid +1;
            }else if (number < arr[mid]){
                max = mid -1;
            }
            else{
                return mid;
            }
        }
        return -1;
    }
}
