package com.zuoye.zuoye03;

public class Student {
    private String name;
    private int age;
    private String sex;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }

    public Student() {
    }

    public Student(String name, int age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws AgeException {
       if (age > 15 && age < 50){
           this.age = age;
       }else{
           throw new AgeException("性别异常！");
       }
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) throws SexException {
        if (sex.equals("男")&& sex.equals("女")){
            this.sex = sex;
        }else{
            throw new SexException("性别异常！");
        }
    }
}
