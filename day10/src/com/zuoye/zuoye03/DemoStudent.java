package com.zuoye.zuoye03;

public class DemoStudent {
    public static void main(String[] args) {
        Student s1 = new Student();
        try {
            s1.setAge(66);
            System.out.println(s1.getAge());
        } catch (AgeException e) {
            System.out.println("年龄异常！");
        }
    }
}
