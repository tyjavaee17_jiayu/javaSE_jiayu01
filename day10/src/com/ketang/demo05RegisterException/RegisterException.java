package com.ketang.demo05RegisterException;
/*
    自定义异常:
        java给我们提供的异常类不够我们使用,就需要我们紫荆定义一些异常相关的类
    注意事项:
        1.自定义异常的类名,一般都是以Exception结尾,说明这个类是一个异常相关的类(见名知意)
        2.自定义异常
            a.必须继承Exception:自定义的异常就是一个编译期异常
                使用:如果在方法内部抛出了编译期异常,那么我们就必须处理这个异常
                    1.使用throws声明抛出异常,最终抛出给方法的调用者处理
                    2.使用try...catch自己捕获处理异常
           b.必须继承RuntimeException:z自定义的异常就是一个运行期异常
                使用:如果在方法内部抛出了运行期异常,我们无需处理,默认交给JVM处理(中断处理)
   格式:
       public class xxxException  extends Exception|extends RuntimeExcetpion{
            //定义一个空参数的构造方法
            public xxxException(){
                super();
            }

            //定义一个带异常信息的构造方法
            //我们查询异常相关类的源码发现,每个异常类都会定义一个带异常信息的构造方法,把异常信息传递给父类处理
            public xxxException(String message){
                super(message);
            }
       }
 */
public class RegisterException  extends Exception{
    public RegisterException() {
    }

    public RegisterException(String message) {
        super(message);
    }
}
