package com.ketang.demo05RegisterException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class DemoRegisterException {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list,"张三","李四","柳岩","迪丽热巴");
        System.out.println("请输入您要注册的用户名：");
        String regName = new Scanner(System.in).nextLine();
        try {
            checkName(list,regName);
        } catch (RegisterException e) {
            e.printStackTrace();
        }
    }
    public static void checkName(ArrayList<String> list, String regName) throws RegisterException {
        for (String name : list) {
            if (name.equals(regName)){
                throw  new RegisterException("您设置的名字已经有人占用！");
            }
        }
        list.add(regName);
        System.out.println("恭喜您，注册成功！！！");
    }
}
