package com.ketang.demo02BinarySearch;

import java.util.Arrays;

public class DemoBinarySearch {
    public static void main(String[] args) {
        int[] arr = {32,40,57,68,94,100,120};
        int index = binarySearch(arr, 120);
        System.out.println(index);

    }
    public static int binarySearch(int[] arr,int number){
            int min = 0;
            int max = arr.length-1;
            int mid = 0;
            while(min <= max){
                mid = (min + max)/2;
                if (number > arr[mid]){
                    min = mid + 1;
                }else if(number < arr[mid]){
                    max = mid - 1;
                }else{
                    return mid;
                }
            }
            return -1;
    }
}
