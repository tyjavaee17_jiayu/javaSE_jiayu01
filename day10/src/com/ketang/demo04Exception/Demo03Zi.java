package com.ketang.demo04Exception;
/*
    异常处理的注意事项:
    2.子父类异常的处理
    a.如果父类的方法抛出了多个异常
        子类重写父类方法时,子类可以抛出和父类相同的异常
        子类重写父类方法时,抛出父类异常的子类
        子类重写父类方法时,可以不抛出异常
    b.父类方法没有抛出异常，子类重写父类该方法时也不可抛出异常。
        此时子类产生异常，只能捕获处理，不能声明抛出


    注意:记住以下的话
        子父类异常,我们无需考虑,
        父类抛出什么异常,子类重写父类的方法也抛出什么异常

 */
public class Demo03Zi extends Demo02Fu {
    //子类重写父类方法时,子类可以抛出和父类相同的异常
    public void show01()throws ClassCastException,NullPointerException{}
    /*
        子类重写父类方法时,抛出父类异常的子类
        ClassCastException extends RuntimeException
        NullPointerException extends RuntimeException

     */
    //子类不能抛出父类异常
    //public void show02()throws RuntimeException{}
    //public void show02()throws ClassCastException{}
    public void show02()throws NullPointerException{}

    //子类重写父类方法时,可以不抛出异常
    public void show03(){}
    //b.父类方法没有抛出异常，子类重写父类该方法时也不可抛出异常。
    public void show04(){}

    public void show05(){
        //此时子类产生异常，只能捕获处理，不能声明抛出
        try {
            throw  new Exception("异常了！");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
