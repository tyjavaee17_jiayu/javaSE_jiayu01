package com.ketang.demo04Exception;

import java.util.ArrayList;

/*
    异常处理的注意事项
    3.多个异常使用捕获又该如何处理呢？
        1. 多个异常分别处理。
        2. 多个异常一次捕获，多次处理。
        3. 多个异常一次捕获一次处理。
 */
public class Demo04Exception {
    public static void main(String[] args) {
        //1. 多个异常分别处理。每一个异常都使用一个try...catch处理
        try {
            int[] arr = new int[3];
            System.out.println(arr[3]);
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            ArrayList<String> list = new ArrayList<>();
            list.add("a");
            System.out.println(list.get(3));
        }catch (Exception e){
            e.printStackTrace();
        }
        //2. 多个异常一次捕获，多次处理。
        //注意:
        // 一个try多个catch,catch中定义的异常变量不能是下边定义异常变量的父类
        try {
            int[] arr = new int[3];
            System.out.println(arr[3]);

            ArrayList<String> list = new ArrayList<>();
            list.add("a");
            System.out.println(list.get(3));
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("集合索引越界了"+ e);
        }catch (IndexOutOfBoundsException e){
            System.out.println("数组索引越界了"+ e);
        }
        /*
            3. 多个异常一次捕获一次处理。
            弊端:
                无论有多少种异常产生,只能在catch中写一种异常的处理逻辑
         */
        try {
            int[] arr = new int[3];
            System.out.println(arr[5]);

            ArrayList<String> list = new ArrayList<>();
            list.add("a");
            System.out.println(list.get(3));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
