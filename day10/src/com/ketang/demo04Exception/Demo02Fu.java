package com.ketang.demo04Exception;

public class Demo02Fu {
    public void show01()throws ClassCastException,NullPointerException{}
    public void show02()throws RuntimeException{}
    public void show03()throws Exception{}
    public void show04(){}
    public void show05(){}
}
