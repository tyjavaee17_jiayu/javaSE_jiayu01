package com.ketang.demo04Exception;
/*
    异常注意事项
        1.运行时(期)异常被抛出可以不处理。
            即不捕获(不使用try...catch来捕获处理异常)
            也不声明抛出(不使用throws关键字声明抛出)。
       运行时异常,我们无需处理,默认会交给JVM处理==>中断
       运行时异常,处理没有意义
       使用数组,超出了数组索引的使用范围
       就算我们使用try...catch处理了这个异常,那么实际上数组的索引还是超出了数组的索引范围
       这时候我们应该修改代码 ,不让索引越界,从根本上解决问题
 */
public class Demo01Exception {
    public static void main(String[] args) {
        try {
            int[] arr = new int[4];
            System.out.println(arr[5]);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        System.out.println("后续代码！");
    }
}
