package com.ketang.demo03Exception;

public class Demo03Throw {
    public static void main(String[] args) {
        int[] arr = new int[3];
        int element = getElement(null,0);
        System.out.println(element);
        //ArrayIndexOutOfBoundsException: 3
    }
    public static int getElement(int[] arr , int index){
        if (arr == null){
            throw new NullPointerException("小伙子，你什么东西都没传这怎么搞！");

        }
        if (index > arr.length-1 || index < 0){
            throw  new ArrayIndexOutOfBoundsException("小伙子，你是不是记错要找的范围了！");
        }
        int e = arr[index];
        return e;
    }
}
