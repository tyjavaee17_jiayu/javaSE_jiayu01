package com.ketang.demo03Exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Demo09finally {
    public static void main(String[] args) throws ParseException {
        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = sdf.parse("2020-03-20");
            System.out.println(date);
        } catch (ParseException e) {
            System.out.println(e.toString());
        }finally {
            System.out.println("我才不管他们是否异常，我就是要执行！！");
        }
        //后续代码执行
        System.out.println("后续代码！！");
        System.out.println("后续代码！！");
        System.out.println("后续代码！！");
        System.out.println("后续代码！！");*/

        try {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf1.parse("2020-0226");
        } finally {
            System.out.println("我才不管他们是否异常，我就是要执行！！");
        }
        //后续代码不执行
        System.out.println("后续代码！！");
        System.out.println("后续代码！！");
        System.out.println("后续代码！！");
        System.out.println("后续代码！！");
    }
}
