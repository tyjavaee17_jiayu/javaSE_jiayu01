package com.ketang.demo03Exception;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo06TryCatch {
    public static void main(String[] args) {
        try {
            readFile(null);
        }catch (FileNotFoundException e){//子类
            System.out.println("老弟，按着你传递的路径我没有找到我的真爱！");
        }catch (IOException e) {//父类
            System.out.println("兄弟你的文件里什么都没有！");
        }
        System.out.println("后续代码！");
        System.out.println("后续代码！");
        System.out.println("后续代码！");
        System.out.println("后续代码！");
    }
    public static void readFile(String path) throws FileNotFoundException, IOException {
        if (path == null){
            throw new IOException("兄弟你的文件里什么都没有！");

        }
        if (!"d:\\a.txt".equals(path)){
            throw new FileNotFoundException("小弟，按着你传递的路径我没有找到我的真爱！");
        }
        System.out.println("读取d:\\a.txt文件");
    }
}
