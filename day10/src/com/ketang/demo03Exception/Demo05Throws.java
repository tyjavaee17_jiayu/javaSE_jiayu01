package com.ketang.demo03Exception;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo05Throws {
    public static void main(String[] args) throws IOException {
        //readFile("d:\\a.txt");//读取d:\a.txt文件
        //readFile("a");//Exception in thread "main" java.io.FileNotFoundException: 小弟，按着你传递的路径我没有找到我的真爱！
        readFile(null);//Exception in thread "main" java.io.IOException: 兄弟你的文件里什么都没有！
    }
    public static void readFile(String path) throws IOException {
        if (path == null){
            throw new IOException("兄弟你的文件里什么都没有！");

        }
        if (!"d:\\a.txt".equals(path)){
            throw new FileNotFoundException("小弟，按着你传递的路径我没有找到我的真爱！");
        }
        System.out.println("读取d:\\a.txt文件");
    }
}
