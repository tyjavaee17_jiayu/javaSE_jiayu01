package com.ketang.demo03Exception;

public class Demo04Throws {
    public static void main(String[] args) throws Exception {
        int[] arr = new int[3];
        int element = getElement(arr,5);
        System.out.println(element);
        //Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 小伙子，你是不是记错要找数据的索引范围了
    }
    public static int getElement(int[] arr , int index)throws Exception{
        if (arr == null){
            throw new NullPointerException("小伙子，你什么东西都没传这怎么搞！");

        }
        if (index > arr.length-1 || index < 0){
            throw  new ArrayIndexOutOfBoundsException("小伙子，你是不是记错要找数据的索引范围了！");
        }
        int e = arr[index];
        return e;
    }
}
