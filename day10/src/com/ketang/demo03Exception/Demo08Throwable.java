package com.ketang.demo03Exception;

public class Demo08Throwable {
    public static void main(String[] args) {
        try {
            throw  new Exception("异常！！");
        }catch (Exception e){
            System.out.println(123);//自定义一异常逻辑   123
            System.out.println("--------------");
            String message = e.getMessage();
            System.out.println(message);//异常！！
            System.out.println("--------------");
            String s = e.toString();
            System.out.println(s);//java.lang.Exception: 异常！！
            System.out.println("--------------");
            e.printStackTrace();//java.lang.Exception: 异常！！
            //                        at com.ketang.demo03Exception.Demo08Throwable.main(Demo08Throwable.java:6)
        }
    }
}
