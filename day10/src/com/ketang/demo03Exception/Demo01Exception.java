package com.ketang.demo03Exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Demo01Exception {
    public static void main(String[] args) {
        //ArrayIndexOutOfBoundsException
        /*try {
            int[] arr = new int[3];
            System.out.println(arr[3]);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //Thu Mar 26 17:20:25 CST 2020
        //Unparseable date: "202003-20"   格式错误引发的异常
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = new Date();
//        try {
//            Date parse = sdf.parse("202003-20");
//            System.out.println(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        //Compilation completed with 1 error and 0 warnings in 440 ms (moments ago)
        int[] arr = new int[1024*1000000000];
        System.out.println("11111");
        System.out.println("11111");
        System.out.println("11111");
        System.out.println("11111");
        System.out.println("11111");
    }
}
