package com.ketang.demo03Exception;

import java.util.Objects;

public class Demo07Objects {
    public static void main(String[] args) {
        //Person p1 = new Person("张三",18);
        //System.out.println(p1);
         Person p2 = null;
        //Object obj3 = checkObject(p2);//Exception in thread "main" java.lang.NullPointerException: 您传进来空空如也！
        Person p3 = Objects.requireNonNull(p2,"这个主人有点懒，什么都没传进来！");//;Exception in thread "main" java.lang.NullPointerException: 这个主人有点懒，什么都没传进来！
        System.out.println(p3);
    }
    public static Object checkObject(Object obj){
        if (obj == null){
            throw new NullPointerException("您传进来空空如也！");
        }
        return obj;
    }
}
