package com.ketang.demo03Exception;

public class Demo02Exception {
    public static void main(String[] args) {
        int[] arr = new int[3];
        int element = getElement(arr, 3);
        System.out.println(element);
        //ArrayIndexOutOfBoundsException: 3
    }
    public static int getElement(int[] arr , int index){
        int e = arr[index];
        return e;
    }
}
