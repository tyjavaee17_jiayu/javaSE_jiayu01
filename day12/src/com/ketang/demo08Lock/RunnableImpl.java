package com.ketang.demo08Lock;

import java.util.concurrent.locks.ReentrantLock;

public class RunnableImpl implements Runnable {
    private static int ticket = 100;
    ReentrantLock lock = new ReentrantLock();
    @Override
    public void run() {
        while (true){
            lock.lock();
            if (ticket > 0){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+ "正在卖第"+ ticket+"张电影票");
                ticket--;
            } else{
                break;
            }
            lock.unlock();
        }
    }
}
