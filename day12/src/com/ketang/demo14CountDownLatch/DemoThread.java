package com.ketang.demo14CountDownLatch;

import java.util.concurrent.CountDownLatch;

public class DemoThread {
    public static void main(String[] args) {
        CountDownLatch cdl = new CountDownLatch(1);
        new MyThreadAC(cdl).start();

        new MyThreadB(cdl).start();
    }
}
