package com.ketang.demo14CountDownLatch;

import java.util.concurrent.CountDownLatch;

public class MyThreadB extends Thread {
    private CountDownLatch countDownLatch;

    public MyThreadB(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        System.out.println("B");
        countDownLatch.countDown();
    }
}
