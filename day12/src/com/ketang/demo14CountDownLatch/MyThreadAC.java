package com.ketang.demo14CountDownLatch;

import java.util.concurrent.CountDownLatch;

public class MyThreadAC extends Thread{
    private CountDownLatch countDownLatch;

    public MyThreadAC(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        System.out.println("A");
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("C");
    }
}
