package com.ketang.demo10CopyOnWriteArraySet;

import java.util.concurrent.CopyOnWriteArraySet;

public class MyThread extends Thread {
    public static CopyOnWriteArraySet<Integer> set = new CopyOnWriteArraySet<>();

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            set.add(i);
        }
        System.out.println("Thread-0线程往集合中添加元素结束！");
    }
}
