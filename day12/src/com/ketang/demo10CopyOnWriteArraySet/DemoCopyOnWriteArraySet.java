package com.ketang.demo10CopyOnWriteArraySet;

public class DemoCopyOnWriteArraySet {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();
        System.out.println("main方法线程会继续执行");
        for (int i = 1000; i < 2000; i++) {
            try {
                Thread.sleep(1);
                MyThread.set.add(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("main线程往集合中添加数据结束！");
        System.out.println("mian线程休息5秒钟，等待Thread-0线程也执行完毕");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("最终集合得长度为："+ MyThread.set.size());
    }
}
