package com.ketang.demo16CyclicBarrier;

import java.util.concurrent.CyclicBarrier;

public class DemoCyclicBarrier {
    public static void main(String[] args) {
        CyclicBarrier cb = new CyclicBarrier(5, new MeetingThread());
        PersonThread p1 = new PersonThread(cb);
        PersonThread p2 = new PersonThread(cb);
        PersonThread p3 = new PersonThread(cb);
        PersonThread p4 = new PersonThread(cb);
        PersonThread p5 = new PersonThread(cb);
        p1.start();
        p2.start();
        p3.start();
        p4.start();
        p5.start();


    }
}
