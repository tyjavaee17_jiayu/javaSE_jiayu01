package com.ketang.demo16CyclicBarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class PersonThread extends Thread{
    private CyclicBarrier cyclicBarrier;

    public PersonThread(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10);
            cyclicBarrier.await();
            System.out.println(Thread.currentThread().getName()+"线程来到了会议室！");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }


    }
}
