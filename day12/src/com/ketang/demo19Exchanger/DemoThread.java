package com.ketang.demo19Exchanger;

import java.util.concurrent.Exchanger;

public class DemoThread {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger<>();
        new MyThreadA(exchanger).start();
        new MyThreadB(exchanger).start();
    }
}
