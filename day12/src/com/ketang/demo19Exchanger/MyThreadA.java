package com.ketang.demo19Exchanger;

import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class MyThreadA extends Thread{
    private Exchanger<String> exchanger;

    public MyThreadA(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        System.out.println("线程A开始执行！");
        System.out.println("线程A给了线程B100元钱，并且从线程B得到了一张电影票");
        String result = null;
        try {
           result = exchanger.exchange("100");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("线程A得到得东西：" + result);
    }
}
