package com.ketang.demo19Exchanger;

import java.util.concurrent.Exchanger;

public class MyThreadB extends Thread {
    private Exchanger<String> exchanger;

    public MyThreadB(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        System.out.println("线程B开始执行了...");
        System.out.println("线程B给线程A一张电影票,并且从线程A得到100元");
        String result = null;
        try {
            result = exchanger.exchange("一张电影票");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程B得到得东西："+result);
    }
}
