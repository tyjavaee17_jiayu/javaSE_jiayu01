package com.ketang.demo11ConcurrentHashMap;

import java.util.concurrent.ConcurrentHashMap;

public class MyThread extends Thread{
    public static ConcurrentHashMap<Integer,Integer> map = new ConcurrentHashMap<>();

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
                Thread.sleep(10);
                map.put(i,i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Thread-0线程执行结束");
    }
}
