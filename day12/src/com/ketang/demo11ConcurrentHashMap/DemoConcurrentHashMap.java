package com.ketang.demo11ConcurrentHashMap;

public class DemoConcurrentHashMap {
    public static void main(String[] args) throws InterruptedException {
        MyThread mt = new MyThread();
        mt.start();
        for (int i = 0; i < 1000; i++) {
            MyThread.map.put(i,i);
        }
        System.out.println("main线程休息1秒钟，等待Thread-0执行完毕！");
        Thread.sleep(1000);
        System.out.println("最终集合得长度为："+ MyThread.map.size());
    }
}
