package com.ketang.demo02AtomicIntegerAtom;

public class DemoAtom {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();

        System.out.println("main线程开启新的线程之后，会继续往下执行main方法中的代码");
        System.out.println("mian线程开始改变money的值");
        for (int i = 0; i < 10000; i++) {
            MyThread.money.incrementAndGet();
        }
        System.out.println("main线程暂停1秒，等待Thread-0线程完毕！");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("最终money值为："+ MyThread.money);
    }
}
