package com.ketang.demo02AtomicIntegerAtom;

import java.util.concurrent.atomic.AtomicInteger;

public class MyThread extends Thread {
    public static AtomicInteger money = new AtomicInteger(0);

    @Override
    public void run() {
        System.out.println("Thread-0线程开始改变money的值");
        for (int i = 0; i < 10000; i++) {
            money.getAndIncrement();
        }
        System.out.println("Thread-0线程执行完毕，结束了！");
    }
}
