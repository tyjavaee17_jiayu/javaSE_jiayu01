package com.ketang.demo03AtomicIntegerArray;

public class MyThread extends Thread{
    public static int[] arr = new int[1000];

    @Override
    public void run() {
        for (int i = 0; i < arr.length; i++) {
            arr[i]++;
        }
    }
}
