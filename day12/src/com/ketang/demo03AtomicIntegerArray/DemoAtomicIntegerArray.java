package com.ketang.demo03AtomicIntegerArray;

import java.util.Arrays;

public class DemoAtomicIntegerArray {
    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new MyThread().start();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("main线程睡眠时间结束，遍历数组，获取数组中的元素");
        System.out.println(Arrays.toString(MyThread.arr));
    }
}
