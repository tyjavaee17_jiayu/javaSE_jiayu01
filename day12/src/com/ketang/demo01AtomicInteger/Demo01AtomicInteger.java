package com.ketang.demo01AtomicInteger;

import java.util.concurrent.atomic.AtomicInteger;

public class Demo01AtomicInteger {
    public static void main(String[] args) {
        AtomicInteger ai1 = new AtomicInteger();
        AtomicInteger ai2 = new AtomicInteger(10);
        System.out.println(ai1.get());
        System.out.println(ai2.get());

        System.out.println("-------------");

        int i2 = ai2.getAndIncrement();
        System.out.println("i2=" + i2);
        System.out.println(ai2.get());

        System.out.println("-------------");
        int i3 = ai2.incrementAndGet();
        System.out.println("i3=" + i3);
        System.out.println(ai2.get());

        System.out.println("-------------");

        int i4 = ai2.addAndGet(100);
        System.out.println("i4=" + i4);
        System.out.println(ai2.get());

        System.out.println("-------------");

        int i5 = ai2.getAndAdd(100);
        System.out.println("i5=" + i5);
        System.out.println(ai2.get());
    }
}
