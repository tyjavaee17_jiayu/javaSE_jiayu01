package com.ketang.demo12HashTableAndConcurrentHashMap;

import java.util.concurrent.ConcurrentHashMap;

public class MyThread extends Thread {
    public static ConcurrentHashMap<Integer,Integer> table = new ConcurrentHashMap<>();
    //public static Hashtable<Integer,Integer> table = new Hashtable<>();

    @Override
    public void run() {
        long s = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            table.put(i,i);
        }
        long e = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + "线程存储共耗时："+ (e - s)+ "毫秒");
    }
}
