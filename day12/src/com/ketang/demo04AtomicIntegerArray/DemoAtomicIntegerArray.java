package com.ketang.demo04AtomicIntegerArray;

public class DemoAtomicIntegerArray {
    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new MyThread().start();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("main线程睡眠结束，遍历数组，获取数组中的元素");
        for (int i = 0; i < MyThread.integerArray.length(); i++) {
            System.out.println(MyThread.integerArray.get(i) + "  ");
        }
    }
}
