package com.ketang.demo04AtomicIntegerArray;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class MyThread extends Thread {
    public static int[] arr = new int[1000];
    public static AtomicIntegerArray integerArray = new AtomicIntegerArray(arr);

    @Override
    public void run() {
        for (int i = 0; i < arr.length; i++) {
            integerArray.addAndGet(i,1);
        }
    }
}
