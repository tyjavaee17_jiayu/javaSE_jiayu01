package com.ketang.demo17synchronized;

public class ClassRoom {
    public void intoClassRoom() throws InterruptedException {
        synchronized (this){
            System.out.println(Thread.currentThread().getName()+"进教室参观！");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName()+"离开了教室！");
        }
    }

}
