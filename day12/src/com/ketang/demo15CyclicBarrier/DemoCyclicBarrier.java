package com.ketang.demo15CyclicBarrier;

public class DemoCyclicBarrier {
    public static void main(String[] args) {
        PersonThread p1 = new PersonThread();
        PersonThread p2 = new PersonThread();
        PersonThread p3 = new PersonThread();
        PersonThread p4 = new PersonThread();
        p1.start();
        p2.start();
        p3.start();
        p4.start();
        MeetingThread mt = new MeetingThread();
        mt.start();
    }
}
