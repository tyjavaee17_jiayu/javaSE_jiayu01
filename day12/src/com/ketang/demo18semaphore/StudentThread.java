package com.ketang.demo18semaphore;

public class StudentThread extends Thread {
    private ClassRoom classRoom;

    public StudentThread(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    @Override
    public void run() {
        try {
            classRoom.intoClassRoom();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
