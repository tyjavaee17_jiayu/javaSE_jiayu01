package com.ketang.demo18semaphore;

public class Demo {
    public static void main(String[] args) {
        ClassRoom cl = new ClassRoom();
        for (int i = 0; i < 5; i++) {
            new StudentThread(cl).start();
        }
    }
}
