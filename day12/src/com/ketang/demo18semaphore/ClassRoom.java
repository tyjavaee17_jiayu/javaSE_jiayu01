package com.ketang.demo18semaphore;

import java.util.concurrent.Semaphore;

public class ClassRoom {
    private Semaphore semaphore = new Semaphore(3);

    public void intoClassRoom() throws InterruptedException {
            semaphore.acquire();
            System.out.println(Thread.currentThread().getName()+"进教室参观！");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName()+"离开了教室！");
            semaphore.release();
    }
}
