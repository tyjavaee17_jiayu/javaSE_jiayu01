package com.ketang.demo09CopyOnWriteArrayList;

public class DemoCopyOnWriteArrayList {
    public static void main(String[] args) throws InterruptedException {
        MyThread mt = new MyThread();
        mt.start();
        System.out.println("main线程会继续执行");
        for (int i = 0; i < 1000; i++) {
                Thread.sleep(5);
                MyThread.list.add(i);
        }
        System.out.println("main线程往集合中添加数据接收！");
        System.out.println("main线程休息5秒钟，等待Thread-0线程也执行完毕");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("最终集合得长度为："+ MyThread.list.size());
    }
}
