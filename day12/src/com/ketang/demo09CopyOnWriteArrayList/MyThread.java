package com.ketang.demo09CopyOnWriteArrayList;

import java.util.concurrent.CopyOnWriteArrayList;

public class MyThread extends Thread {
    public static CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list.add(i);
        }
        System.out.println("Thread-0线程往集合中添加元素结束！");
    }
}
