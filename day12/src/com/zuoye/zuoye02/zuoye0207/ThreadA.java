package com.zuoye.zuoye02.zuoye0207;

import java.util.concurrent.Exchanger;

public class ThreadA extends Thread{
    private Exchanger<String> exchanger;

    public ThreadA(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        try {
            String s = exchanger.exchange("一条娱乐新闻");
            System.out.println("线程B给线程A："+ s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
