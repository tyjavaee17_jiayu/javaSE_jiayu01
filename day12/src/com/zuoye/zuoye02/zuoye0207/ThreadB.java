package com.zuoye.zuoye02.zuoye0207;

import java.util.concurrent.Exchanger;

public class ThreadB extends Thread{
    private Exchanger<String> exchanger;

    public ThreadB(Exchanger<String> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        try {
            String s = exchanger.exchange("一条体育新闻");
            System.out.println("线程A给线程B："+ s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
