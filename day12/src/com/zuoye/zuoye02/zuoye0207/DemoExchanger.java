package com.zuoye.zuoye02.zuoye0207;

import java.util.concurrent.Exchanger;

public class DemoExchanger {
    public static void main(String[] args) {
        Exchanger<String> e = new Exchanger<>();
        new ThreadA(e).start();
        new ThreadB(e).start();
    }
}
