package com.zuoye.zuoye02.zuoye0203;

import java.util.Map;
import java.util.Set;

public class DemoConcurrentHashMap {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();
        for (int i = 10001; i <=20000 ; i++) {
            try {
                MyThread.map.put(i,i);
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Set<Map.Entry<Integer, Integer>> entries = MyThread.map.entrySet();
        for (Map.Entry<Integer, Integer> entry : entries) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println(key +"\t"+ value);
        }
        System.out.println("集合的长度为：" + MyThread.map.size());
    }
}
