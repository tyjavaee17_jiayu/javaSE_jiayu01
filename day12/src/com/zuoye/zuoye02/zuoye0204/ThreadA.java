package com.zuoye.zuoye02.zuoye0204;

import java.util.concurrent.CountDownLatch;

public class ThreadA extends Thread {
    private CountDownLatch countDownLatch;

    public ThreadA(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        System.out.println("打印开始");
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("计算完毕");
    }
}
