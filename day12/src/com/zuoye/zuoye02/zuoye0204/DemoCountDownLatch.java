package com.zuoye.zuoye02.zuoye0204;

import java.util.concurrent.CountDownLatch;

public class DemoCountDownLatch {
    public static void main(String[] args) {
        CountDownLatch cdl = new CountDownLatch(1);
        new ThreadA(cdl).start();
        new ThreadB(cdl).start();
    }
}
