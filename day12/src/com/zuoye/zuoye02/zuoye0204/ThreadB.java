package com.zuoye.zuoye02.zuoye0204;

import java.util.concurrent.CountDownLatch;

public class ThreadB extends Thread {
    private CountDownLatch countDownLatch;

    public ThreadB(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int i = 1; i <=100 ; i++) {
            sum += i;
        }
        System.out.println("1--100所有数的累加和：" + sum);
        countDownLatch.countDown();
    }
}
