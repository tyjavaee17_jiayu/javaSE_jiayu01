package com.zuoye.zuoye02.zuoye0206;

public class DemoSemaphore {
    public static void main(String[] args) {
        Room room = new Room();
        for (int i = 1; i <= 10; i++) {
            new VisiterThread(room).start();
        }
    }
}
