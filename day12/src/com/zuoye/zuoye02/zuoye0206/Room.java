package com.zuoye.zuoye02.zuoye0206;

import java.util.concurrent.Semaphore;

public class Room {
    private Semaphore sp = new Semaphore(3);
    public void LookRoom(){
        try {
            sp.acquire();
            System.out.println(Thread.currentThread().getName() +"游客进展览室参观");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() +"游客离开展览室");
            sp.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
