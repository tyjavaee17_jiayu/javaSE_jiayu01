package com.zuoye.zuoye02.zuoye0206;

public class VisiterThread extends Thread{
    private Room room;

    public VisiterThread(Room room) {
        this.room = room;
    }

    @Override
    public void run() {
        room.LookRoom();
    }
}
