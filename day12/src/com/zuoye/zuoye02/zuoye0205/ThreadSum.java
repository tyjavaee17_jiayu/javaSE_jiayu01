package com.zuoye.zuoye02.zuoye0205;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreadSum  extends Thread{
    private CyclicBarrier cyclicBarrier;

    public ThreadSum(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        try {
            int sum = 0;
            for (int i = 1; i <=10000 ; i++) {
                sum +=i;
            }
            System.out.println("所有1--10000整数的累加和为："+sum);
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
