package com.zuoye.zuoye02.zuoye0205;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreadJiShu extends Thread{
    private CyclicBarrier cyclicBarrier;

    public ThreadJiShu(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        try {
            int sum = 0;
            for (int i = 1; i <=10000 ; i++) {
                if (i % 2 == 1){
                    sum +=i;
                }
            }
            System.out.println("所有1--10000的奇数的累加和为：" + sum);
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
