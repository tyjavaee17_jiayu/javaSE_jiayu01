package com.zuoye.zuoye02.zuoye0205;

import java.util.concurrent.CyclicBarrier;

public class DemoCyclicBarrier {
    public static void main(String[] args) {
        CyclicBarrier cb = new CyclicBarrier(3,new PrintThread());
        ThreadSum sum = new ThreadSum(cb);
        sum.start();
        ThreadOuShu ouShu = new ThreadOuShu(cb);
        ouShu.start();
        ThreadJiShu jiShu = new ThreadJiShu(cb);
        jiShu.start();
    }
}
