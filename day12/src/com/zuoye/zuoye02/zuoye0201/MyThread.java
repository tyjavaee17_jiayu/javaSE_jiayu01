package com.zuoye.zuoye02.zuoye0201;

import java.util.concurrent.CopyOnWriteArrayList;

public class MyThread extends Thread {
    public static CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();

    @Override
    public void run() {
        for (int i = 1; i <= 10000; i++) {
            list.add(i);
        }
    }
}
