package com.zuoye.zuoye02.zuoye0201;

public class DemoCopyOnWriteArrayList {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();
        for (int i = 10001; i <=20000 ; i++) {
            MyThread.list.add(i);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Integer integer : MyThread.list) {
            System.out.println(integer);
        }
        System.out.println("集合的长度为：" + MyThread.list.size());
    }
}
