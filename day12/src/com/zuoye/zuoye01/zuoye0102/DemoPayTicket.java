package com.zuoye.zuoye01.zuoye0102;

public class DemoPayTicket {
    public static void main(String[] args) {
        SynchronisedRunnableImpl r = new SynchronisedRunnableImpl();
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        Thread t3 = new Thread(r);

        t1.start();
        t2.start();
        t3.start();

    }
}
