package com.zuoye.zuoye01.zuoye0102;

public class SynchronisedRunnableImpl implements Runnable {
    private static int ticket = 100;
    Object obj = new Object();
    @Override
    public void run() {
        while (true){
            synchronized (obj) {
                if (ticket > 0){
                    System.out.println(Thread.currentThread().getName()+ "正在卖第"+ ticket+"张电影票");
                    ticket--;
                } else{
                    break;
                }
            }
        }
    }
}
