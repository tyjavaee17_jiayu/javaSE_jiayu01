package com.zuoye.zuoye01.zuoye0103;

public class DemoPayTicket {
    public static void main(String[] args) {
        SynchronisedMethodRunnableImpl r = new SynchronisedMethodRunnableImpl();
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        Thread t3 = new Thread(r);

        t1.start();
        t2.start();
        t3.start();

    }
}
