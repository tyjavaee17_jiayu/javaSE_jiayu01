package com.zuoye.zuoye01.zuoye0103;

public class SynchronisedMethodRunnableImpl implements Runnable {
    private static int ticket = 100;
    @Override
    public void run() {
        while (true){
            payTiket();
        }
    }
    public synchronized void payTiket(){
        if (ticket > 0){
            System.out.println(Thread.currentThread().getName()+ "正在卖第"+ ticket+"张电影票");
            ticket--;
        }
    }
}
