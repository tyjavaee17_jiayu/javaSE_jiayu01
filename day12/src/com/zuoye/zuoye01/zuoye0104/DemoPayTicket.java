package com.zuoye.zuoye01.zuoye0104;

public class DemoPayTicket {
    public static void main(String[] args) {
        LockRunnableImpl r = new LockRunnableImpl();
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        Thread t3 = new Thread(r);

        t1.start();
        t2.start();
        t3.start();

    }
}
