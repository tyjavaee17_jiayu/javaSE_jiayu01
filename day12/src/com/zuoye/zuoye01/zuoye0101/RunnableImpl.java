package com.zuoye.zuoye01.zuoye0101;

public class RunnableImpl implements Runnable {
    private static int ticket = 100;
    @Override
    public void run() {
        while (true){
            if (ticket > 0){
                System.out.println(Thread.currentThread().getName()+ "正在卖第"+ ticket+"张电影票");
                ticket--;
            } else{
                break;
            }
        }
    }
}
