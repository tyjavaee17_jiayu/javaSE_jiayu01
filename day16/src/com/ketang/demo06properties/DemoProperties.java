package com.ketang.demo06properties;

import java.io.*;
import java.util.Properties;
import java.util.Set;

public class DemoProperties {
    public static void main(String[] args) throws IOException {
        //show01();
        //show02();
        show03();
    }

    private static void show03() throws IOException {
        Properties p = new Properties();
        p.load(new FileReader("day16\\prop.properties"));
        Set<String> set = p.stringPropertyNames();
        for (String key : set) {
            String value = p.getProperty(key);
            System.out.println(key + value);
        }
    }

    private static void show02() throws IOException {
        Properties p = new Properties();
        p.setProperty("迪丽热巴","168");
        p.setProperty("古力娜扎","165");
        p.setProperty("哈尼克孜","160");
        p.setProperty("赵丽颖","172");

        p.store(new FileOutputStream("day16\\prop1.txt"),"save data");
        p.store(new FileWriter("day16\\prop2.txt"),"save data");
        //使用的存储键值对的文件,叫配置文件,结尾一般都使用.properties
        p.store(new FileWriter("day16\\prop.properties"),"");

    }

    private static void show01() {
        Properties p = new Properties();
        p.setProperty("迪丽热巴","168");
        p.setProperty("古力娜扎","165");
        p.setProperty("哈尼克孜","160");
        p.setProperty("赵丽颖","172");
        Set<String> set = p.stringPropertyNames();
        for (String key : set) {
            String value = p.getProperty(key);
            System.out.println(key + value);
        }
    }
}
