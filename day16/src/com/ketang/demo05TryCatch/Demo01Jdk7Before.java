package com.ketang.demo05TryCatch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo01Jdk7Before {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("day16\\c.txt");
            int len = 0;
            while((len = fis.read())!= -1){
                System.out.println((char) len);
            }
        }catch (IOException e) {
            e.printStackTrace();
        } finally{
            if (fis!=null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("后续代码！");
    }
}
