package com.ketang.demo05TryCatch;

import java.io.*;

public class Demo02Jdk7After {
    public static void main(String[] args) {
        try (
                FileWriter fw = new FileWriter("day16\\g.txt");
        ){
            fw.write("哈哈哈哈哈");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try(
                FileOutputStream fos = new FileOutputStream("day16\\h.txt");
                FileInputStream fis = new FileInputStream("day16\\a.txt");
                ) {
            byte[] bytes = new byte[1024];
            int len = 0;
            while ((len = fis.read(bytes))!= -1){
                fos.write(bytes,0,len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
