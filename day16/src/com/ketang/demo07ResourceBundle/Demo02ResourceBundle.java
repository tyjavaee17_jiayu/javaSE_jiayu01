package com.ketang.demo07ResourceBundle;

import java.util.ResourceBundle;

public class Demo02ResourceBundle {
    public static void main(String[] args) {
        ResourceBundle bundle = ResourceBundle.getBundle("prop1");
        String username = bundle.getString("username");
        System.out.println(username);
        String password = bundle.getString("password");
        System.out.println(password);
    }
}
