package com.ketang.demo02CopyFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DemoCopyFile {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        FileInputStream fis = new FileInputStream("D:\\test1.txt");
        FileOutputStream fos = new FileOutputStream("E:\\test1.txt");

        byte[] bytes = new byte[1024];
        int len = 0;
        while((len = fis.read(bytes))!= -1){
            fos.write(bytes,0,len);
        }

        fos.close();
        fis.close();
        long end = System.currentTimeMillis();
        System.out.println("复制文件共需要：" + (end - start)+"毫秒");
    }
}
