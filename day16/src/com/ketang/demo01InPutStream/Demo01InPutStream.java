package com.ketang.demo01InPutStream;

import java.io.FileInputStream;
import java.io.IOException;
/*
            布尔表达式:((len = fis.read())!=-1)
                1.fis.read():读取文件中的一个字节
                2.len = fis.read():把读取到的字节赋值给变量len
                3.(len = fis.read())!=-1:判断变量len的值是否为-1
                    不是-1执行循环体,打印len
                    是-1结束while循环
*/
public class Demo01InPutStream {
    public static void main(String[] args) throws IOException {
        FileInputStream fs = new FileInputStream("day16\\a.txt");
        int len = 0;
        while((len = fs.read())!= -1){      //一次打印一个字节
            System.out.print((char)len);
        }
        fs.close();
    }
}
