package com.ketang.demo03Reader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo01FileInPutStream {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("D:\\test1.txt");
        int len = 0;
        while ((len = fis.read())!= -1){
            System.out.println((char)len);
        }
        fis.close();
    }
}
