package com.ketang.demo04Writer;

import java.io.FileWriter;
import java.io.IOException;

public class Demo04Writer {
    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter("day16\\f.txt");
        for (int i = 1; i <=10 ; i++) {
            fw.write("Hello"+ i + "\r\n");
        }
        fw.flush();
        fw.close();
    }
}
