package com.ketang.demo04Writer;

import java.io.FileWriter;
import java.io.IOException;

public class Demo02Writer {
    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter("day16\\d.txt");
        fw.write(65);
        fw.write(66);
        fw.flush();
        fw.write(67);
        fw.write(68);
        fw.close();
    }
}
