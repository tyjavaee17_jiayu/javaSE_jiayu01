package com.ketang.demo04Writer;

import java.io.FileWriter;
import java.io.IOException;

public class Demo03Writer {
    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter("day16\\e.txt");
        char[] chars = {'a','b','c','d','e','f','g','h','1','2','3','中','国'};
        fw.write(chars);
        fw.write(chars,0,6);

        fw.write("啊哈哈哈哈哈");
        fw.write("啊啊啊啊啊啊啊啊啊啊啊啊啊",0,5);
        fw.flush();
        fw.close();

    }
}
