package com.zuoye.zuoye03.zuoye0302;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class DemoProperties {
    public static void main(String[] args) throws IOException {
        Properties pro = new Properties();
        pro.load(new FileReader("day16\\test2.txt"));
        Set<String> set = pro.stringPropertyNames();
        for (String key : set) {
            String value = pro.getProperty(key);
            System.out.println(key + "=" + value);
        }
    }
}
