package com.zuoye.zuoye03.zuoye0301;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class DemoProperties {
    public static void main(String[] args) throws IOException {
        Properties pro = new Properties();
        pro.setProperty("品名","Iphone Pro Max");
        pro.setProperty("颜色","暗夜绿");
        pro.setProperty("存储容量","256G");
        pro.setProperty("价格","10899");
        pro.store(new FileWriter("day16\\test2.txt"),"");
    }
}
