package com.zuoye.zuoye01.zuoye0102;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("day16\\a.txt");
        FileOutputStream fos = new FileOutputStream("day16\\k.txt");
        byte[] arr = new byte[1024];
        int len = 0;
        while ((len = fis.read(arr))!= -1){
            fos.write(arr,0,len);
        }
        fos.close();
        fis.close();
    }
}
