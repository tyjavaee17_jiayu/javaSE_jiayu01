package com.zuoye.zuoye01.zuoye0101;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("day16\\a.txt");
        FileOutputStream fos = new FileOutputStream("day16\\j.txt");
        int len = 0;
        while ((len = fis.read())!= -1){
            fos.write(len);
        }
        fos.close();
        fis.close();
    }
}
