package com.zuoye.zuoye01.zuoye0104;

import java.io.FileReader;
import java.io.IOException;

public class Demo {
    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("day16\\student.txt");
        int len = 0;
        while ((len = fr.read())!= -1){
            System.out.println((char)len);
        }
        fr.close();
    }
}
