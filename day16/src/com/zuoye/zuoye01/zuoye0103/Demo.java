package com.zuoye.zuoye01.zuoye0103;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) throws IOException {
        List<Student> stuList = new ArrayList<>();
        FileWriter fw = new FileWriter("day16\\student.txt");

        stuList.add(new Student("张三","男",20,89));
        stuList.add(new Student("李四","女",19,99));

        for (Student student : stuList) {
            fw.write(student.getName()+","+student.getSex()+","+student.getAge()+","+student.getScore()+"\r\n");
        }
        fw.close();
    }
}
