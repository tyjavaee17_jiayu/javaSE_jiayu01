package com.itheima.utils;

import com.itheima.superClass.Person;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/*
    注意:
        工具类中的变量和方法都是静态的,方便使用
        通过类名.静态成员变量名|类名.静态成员方法(参数)
        私有空参数构造方法:不让外界创建对象使用
 */
public class Utils {
    private Utils() {
    }
    //定义学员的初始id值
    public static int stuId;
    //定义教师的初始id值
    public static int teaId;
    //后期使用io流|数据库中jdbc技术,读取初始值
    static {
        stuId=0;
        teaId=0;
    }

    /*
        定义根据出生日期计算年龄的静态方法(了解)
        参数:
            String birthday:出生日期 格式: yyyy-MM-dd
        返回值:
            int age 计算出来的年龄
         注意:
            工具类中如果有异常try...catch
     */
    public static int birthdayToAge(String birthday){
        //把字符串格式的生日解析为Date类型
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthdayDate = null;//把变量定义在try的外部,为了提高变量的作用域
        try {
            birthdayDate = sdf.parse(birthday);
        } catch (ParseException e) {
            //e.printStackTrace();
            return -1;//输入的生日不符合模式
        }
        //获取当前系统的时间的日历
        Calendar calendarNow = Calendar.getInstance();
        //判断当前时间是否小于出生日期,是返回-1
        if(calendarNow.before(birthdayDate)){
            //还没有出生
            return -1;
        }
        //获取当前日历中的年月日
        int yearNow = calendarNow.get(Calendar.YEAR);
        int monthNow = calendarNow.get(Calendar.MONTH);
        int dateNow = calendarNow.get(Calendar.DATE);

        //把出生日期Date转换为日历对象
        calendarNow.setTime(birthdayDate);

        //获取生日的年月日
        int yearBirth = calendarNow.get(Calendar.YEAR);
        int monthBirth = calendarNow.get(Calendar.MONTH);
        int dateBirth = calendarNow.get(Calendar.DATE);
        //计算年龄
        //使用当前的年份与出生年份相减,初步计算出年龄
        int age = yearNow-yearBirth;
        //使用当前的月份与出生日期的月份相比
        if(monthNow<=monthBirth){
            //如果月份小于等于出生月份,则年龄上减1,表是不满周岁
            if(monthNow==monthBirth){
                //如果月份相等,在比较日,如果当前日小于出生日,则年龄也减1,表是不满周岁
                if(dateNow<dateBirth){
                    age--;
                }
            }else{
                age--;
            }
        }
        //如果当前月份大于出生月份,直接返回age
        return age;
    }

    public static void main(String[] args) {
        int age = birthdayToAge("2000-04-01");
        System.out.println(age);
    }

    /*
        遍历ArrayList集合,打印学生|教师信息的静态方法
        ? extends Person:泛型的上限限定
        传递进来的集合的泛型?只能是参数list集合泛型Person的子类或者本身
        ?可以是Person的子类Student
        ?可以是Person的子类Teacher
         ?可以是Person本身
     */
    public static void printArrayList(ArrayList<? extends Person> list){
        System.out.println("*****************************************************************************************");
        //打印表头
        System.out.println("编号\t\t姓名\t\t性别\t\t生日\t\t年龄\t\t描述");
        //遍历list集合获取每一个Person的子类对象
        for (int i = 0; i < list.size(); i++) {
            Person person = list.get(i);
            System.out.println(person);//打印对象名,调用对象的toString方法
        }
        System.out.println("*****************************************************************************************");
    }


    /*
        打印学生|教师信息的静态方法
     */
    public static void printPerson(Person person){
        System.out.println("***************************************************************");
        System.out.println(person);//打印对象名,调用对象的toString方法
        System.out.println("***************************************************************");
    }
}
