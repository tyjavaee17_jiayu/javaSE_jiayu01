package com.itheima.superClass;

import com.itheima.utils.Utils;

public abstract class Person {
    private int id;
    private String name;
    private String sex;
    private String birthday;
    private int age;

    public Person() {
    }

    public Person(int id, String name, String sex, String birthday) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return this.id +"\t\t\t"
                 +this.name + "\t\t"
                 + this.sex + "\t\t"
                 + this.birthday + "\t\t"
                 + this.getAge()+"\t\t"+
                "我是一名："+getType()+"，我的工作是："+getWork()+"!"
               ;
    }
    public abstract String getType();
    public abstract String getWork();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getAge() {
        this.age = Utils.birthdayToAge(birthday);
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
