package com.itheima.main;

import com.itheima.Student.Student;
import com.itheima.Teacher.Teacher;
import com.itheima.utils.Utils;

import java.util.ArrayList;
import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Student> students = new ArrayList<>();
        ArrayList<Teacher> teachers = new ArrayList<>();
        while(true){
            System.out.println("----------------欢迎使用信息管理系统---------------");
            System.out.println("1.学生信息管理");
            System.out.println("2.教师信息管理");
            System.out.println("3.退出");
            System.out.println("请输入您要选择的功能：");
            int num = sc.nextInt();
            switch (num){
                case 1:
                    studentManager(students,sc);
                    break;
                case 2:
                    teacherManager(teachers,sc);
                    break;
                case 3:
                    System.out.println("感谢使用本系统，欢迎再次使用！");
                    System.exit(0);
                default:
                    System.out.println("您输入的功能选项："+num+"不存在！");
                    break;
            }
        }
    }

    public static void teacherManager(ArrayList<Teacher> teachers,Scanner sc) {
        while (true){
            System.out.println("----------------欢迎进入教师信息管理系统---------------");
            System.out.println("1.添加教师信息");
            System.out.println("2.修改教师信息");
            System.out.println("3.删除教师信息");
            System.out.println("4.查询教师信息");
            System.out.println("5.返回主菜单");
            System.out.println("请输入你要选择的功能：");
            int num = sc.nextInt();
            switch(num){
                case 1:
                    addTeacher(teachers,sc);
                    break;
                case 2:
                    updateTeacher(teachers,sc);
                    break;
                case 3:
                    deletTeacher(teachers,sc);
                    break;
                case 4:
                    selectTeacher(teachers,sc);
                case 5:
                    return;
                default:
                    System.out.println("您输入的功能选项有误！");
            }
        }
    }
    private static void selectTeacher(ArrayList<Teacher> teachers, Scanner sc) {
        if(teachers.size() == 0){
            System.out.println("您系统中还未添加教师信息，请先添加教师信息");
        }else {
            Utils.printArrayList(teachers);
        }
    }

    private static void deletTeacher(ArrayList<Teacher> teachers, Scanner sc) {
        System.out.println("-----------欢迎来到删除教师信息界面------------");
        selectTeacher(teachers,sc);

        System.out.println("请输入您要删除的教师的ID：");
        int id = sc.nextInt();

        for (int i = 0; i < teachers.size(); i++) {
            Teacher teacher = teachers.get(i);

            if (teacher.getId() == id){

                Utils.printPerson(teacher);

                System.out.println("您真的要删除ID为"+id+"的教师信息吗？（y|n）");

                String yesOrNo = sc.next();

                if (yesOrNo.equalsIgnoreCase("y")){
                    teachers.remove(i);
                    System.out.println("恭喜您删除ID为【"+id+"】的教师成功！");
                    return;
                }else{

                    System.out.println("您的删除操作已取消！");
                    return;
                }
            }
        }

        System.out.println("您输入的教师ID有误！");

    }

    private static void updateTeacher(ArrayList<Teacher> teachers, Scanner sc) {
        System.out.println("-----------欢迎来到修改教师信息界面------------");

        selectTeacher(teachers,sc);

        System.out.println("请输入您要修改的教师的ID");
        int id = sc.nextInt();

        for (int i = 0; i <teachers.size(); i++) {
           Teacher teacher = teachers.get(i);

            if (teacher.getId() == id ){

                Utils.printPerson(teacher);

                System.out.println("请输入新的教师姓名：");
                String stuName = sc.next();
                System.out.println("请输入新的教师性别：");
                String stuSex = sc.next();
                System.out.println("请输入新的教师生日：【yyyy-MM-dd】");
                String stuBirthday = sc.next();

                if (!"0".equals(stuName)){
                    teacher.setName(stuName);
                }if (!"0".equals(stuSex)){
                    teacher.setName(stuSex);
                }
                if (!"0".equals(stuBirthday)){
                    teacher.setName(stuBirthday);
                }

                System.out.println("恭喜您添加的ID为"+id+"的教师信息已成功！");
                return;
            }

        }

        System.out.println("您输入的教师信息的ID有误！");
    }

    private static void addTeacher(ArrayList<Teacher> teachers, Scanner sc) {
        System.out.println("-----------欢迎来到添加教师信息界面------------");

        System.out.println("请输入教师的姓名：");
        String name = sc.next();

        System.out.println("请输入教师的性别：");
        String sex = sc.next();

        System.out.println("请输入教师的生日：【yyyy-MM-dd】");
        String birthday = sc.next();

        Teacher s = new Teacher(++Utils.teaId,name,sex,birthday);

        teachers.add(s);

        System.out.println("恭喜您，添加教师 【"+name+"】 信息成功！");

    }


    public static void studentManager(ArrayList<Student> students,Scanner sc) {
        while (true){
            System.out.println("----------------欢迎进入学生信息管理系统---------------");
            System.out.println("1.添加学生信息");
            System.out.println("2.修改学生信息");
            System.out.println("3.删除学生信息");
            System.out.println("4.查询学生信息");
            System.out.println("5.返回主菜单");
            System.out.println("请输入你要选择的功能：");
            int num = sc.nextInt();
            switch(num){
                case 1:
                    addStudent(students,sc);
                    break;
                case 2:
                    updateStudent(students,sc);
                    break;
                case 3:
                    deletStudent(students,sc);
                    break;
                case 4:
                    selectStudent(students,sc);
                case 5:
                    return;
                default:
                    System.out.println("您输入的功能选项有误！");
            }
        }
    }

    private static void selectStudent(ArrayList<Student> students, Scanner sc) {
        if(students.size() == 0){
            System.out.println("您系统中还未添加学生信息，请先添加学生信息");
        }else {
            Utils.printArrayList(students);
        }
    }

    private static void deletStudent(ArrayList<Student> students, Scanner sc) {
        System.out.println("-----------欢迎来到删除学生信息界面------------");
        selectStudent(students,sc);
            //.获取用户输入的要删除学员的id值
        System.out.println("请输入您要删除的学生的ID：");
        int id = sc.nextInt();
        //.遍历存储学员集合,获取每一个学员
        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);
            //.使用用输入的学员的id和遍历得到的学员的id依次比较
            //.如果用户输入的学员的id和遍历得到的学员的id一致,就删除学员
            if (student.getId() == id){
                //.使用Utils工具类中的方法打印要删除的学员的信息
                Utils.printPerson(student);
                //.给用户一个友好提示"您真的要删除id为"+id+"的学员信息吗?(y|n)?"
                System.out.println("您真的要删除ID为"+id+"的学生信息吗？（y|n）");
                //.获取用户输入的yesAndNo的选项
                String yesOrNo = sc.next();
                //.对用输入的选项进行判断
                if (yesOrNo.equalsIgnoreCase("y")){
                    //   输入:y|Y
                    //       在集合中删除对应id的学员
                    //       提示用户"恭喜您,id为"+id+"的学员删除成功!"
                    //       结束方法
                    students.remove(i);
                    System.out.println("恭喜您删除ID为【"+id+"】的学生成功！");
                    return;
                }else{
                    //   输入:n|N
                    //       提示用户"您的删除操作已经取消!"
                    //       结束方法
                    System.out.println("您的删除操作已取消！");
                    return;
                }
            }
        }
            //9.循环结束了还没有匹配要删除的学员id,则提示用户"您输入的学员id"+id+"有误!"
        System.out.println("您输入的学生ID有误！");

    }

    private static void updateStudent(ArrayList<Student> students, Scanner sc) {
        System.out.println("-----------欢迎来到修改学生信息界面------------");
        //先查询所有学员的信息,提高用户的体验
        selectStudent(students,sc);
        //1.获取用户输入的要修改学员的id
        System.out.println("请输入您要修改的学生的ID");
        int id = sc.nextInt();
        //2.遍历存储学员的集合,获取每一个学员
        for (int i = 0; i <students.size(); i++) {
            Student student = students.get(i);
            //3.使用用户输入的id和遍历得到的学员的id依次比较
            if (student.getId() == id ){
            //4.如果用户输入的id和遍历得到的id一致,修改学员信息
            //5.先调用Utils工具类中的打印学员信息的方法
                Utils.printPerson(student);
            //6.获取用户输入的新的学员的信息(保留原值输入0)
                System.out.println("请输入新的学生姓名：");
                String stuName = sc.next();
                System.out.println("请输入新的学生性别：");
                String stuSex = sc.next();
                System.out.println("请输入新的学生生日：【yyyy-MM-dd】");
                String stuBirthday = sc.next();
            //7.增加判断,用户输入非0的值,则用新的信息替换原来的信息
                if (!"0".equals(stuName)){
                    student.setName(stuName);
                }if (!"0".equals(stuSex)){
                    student.setName(stuSex);
                }
                if (!"0".equals(stuBirthday)){
                    student.setName(stuBirthday);
                }
            //8.提示用户学员的信息修改成功
                System.out.println("恭喜您添加的ID为"+id+"的学生信息已成功！");
                return;//结束方法
            }

        }
        //9.循环结束还没有匹配的id,则提示用户您输入的学员id有误
        System.out.println("您输入的学生信息的ID有误！");
    }

    private static void addStudent(ArrayList<Student> students, Scanner sc) {
        System.out.println("-----------欢迎来到添加学生信息界面------------");
            //1.获取用输入的学员信息
            //    a.姓名
        System.out.println("请输入学生的姓名：");
        String name = sc.next();
        //    b.性别
        System.out.println("请输入学生的性别：");
        String sex = sc.next();
            //    c.出生日期(yyyy-MM-dd)
        System.out.println("请输入学生的生日：【yyyy-MM-dd】");
        String birthday = sc.next();
            //2.创建Student对象,封装学员信息
        Student s = new Student(++Utils.stuId,name,sex,birthday);
            //3.把Student对象添加到存储学生的ArrayList集合中
        students.add(s);
            //4.给用户一个添加成功的提示信息
        System.out.println("恭喜您，添加学生 "+name+" 信息成功！");

    }
}
