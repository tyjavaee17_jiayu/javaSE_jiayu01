package com.ketang.demo04PrintStream;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Dmeo02PrintStream {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("输出语句的目的地是在控制台输出！");
        PrintStream ps = new PrintStream("day17\\f.txt");
        System.setOut(ps);
        System.out.println("输出语句的内容写到打印流的目的地f.txt中");
        ps.close();
    }
}
