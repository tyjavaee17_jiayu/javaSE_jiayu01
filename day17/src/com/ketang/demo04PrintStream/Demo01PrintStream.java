package com.ketang.demo04PrintStream;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Demo01PrintStream {
    public static void main(String[] args) throws FileNotFoundException {
        PrintStream ps = new PrintStream("day17\\e.txt");
        ps.write(97);

        ps.println(97);
        ps.println('A');
        ps.println(true);
        ps.println(1.1);
        ps.println("中国");

        ps.close();
    }
}
