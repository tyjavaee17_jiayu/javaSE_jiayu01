package com.ketang.demo02reverseStream;

import java.io.FileReader;
import java.io.IOException;

public class Demo01FileReader {
    public static void main(String[] args) throws IOException {
        //FileReader fr = new FileReader("day17\\我是UTF-8文件.txt");
        FileReader fr = new FileReader("day17\\我是GBK文件.txt");
        int len = 0;
        while((len = fr.read())!= -1){
            System.out.println((char) len);
        }
        fr.close();
    }
}
