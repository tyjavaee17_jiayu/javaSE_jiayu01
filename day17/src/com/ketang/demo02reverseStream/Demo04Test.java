package com.ketang.demo02reverseStream;

import java.io.*;

/* 练习：转换文件编码
        将GBK编码的文本文件，转换为UTF-8编码的文本文件。*/
public class Demo04Test {
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream("day17\\我是GBK文件.txt"), "GBK");
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("day17\\我是UTF-8文件.txt",true), "UTF-8");
        int len = 0;
        while ((len = isr.read())!= -1){
            osw.write(len);
        }
        osw.close();
        isr.close();
    }
}
