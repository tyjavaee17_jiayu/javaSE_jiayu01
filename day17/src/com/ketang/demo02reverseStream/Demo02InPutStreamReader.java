package com.ketang.demo02reverseStream;

import java.io.*;
/*
    java.io.InputStreamReader:字符转换输入流 extends Reader
    作用:
        InputStreamReader 是字节流通向字符流的桥梁：它使用指定的 charset 读取字节并将其解码为字符。
        解码:字节==>字符
*/
public class Demo02InPutStreamReader {
    public static void main(String[] args) throws IOException {
         read_UTF8();
         read_GBK();
    }
    /*
        使用字符转换输入流读取GBK编码的文件
     */
    private static void read_GBK() throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream("day17\\我是GBK文件.txt"), "GBK");
        int len = 0;
        while ((len = isr.read())!=-1){
            System.out.println((char) len);
        }
    }

    /*
        使用字符转换输入流读取UTF-8编码的文件
     */
    private static void read_UTF8() throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream("day17\\我是UTF-8文件.txt"),"UTF-8");
        int len = 0;
        while ((len = isr.read())!= -1){
            System.out.println((char) len);
        }
    }
}
