package com.ketang.demo02reverseStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/*
    java.io.OutputStreamWriter:字符转换输出流 extends Writer
    作用:
        OutputStreamWriter 是字符流通向字节流的桥梁：可使用指定的 charset 将要写入流中的字符编码成字节。
        编码:字符==>字节
*/
public class Demo03OutPutStreamWriter {
    public static void main(String[] args) throws IOException {
        writeUTF8();
        writeGBK();
    }

    private static void writeGBK() throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("day17\\我是GBK文件.txt",true), "GBK");
        osw.write("你好，哈哈");
        osw.flush();
        osw.close();
    }

    private static void writeUTF8() throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("day17\\我是UTF-8文件.txt"), "UTF-8");
        osw.write("你好");
        osw.flush();
        osw.close();
    }
}
