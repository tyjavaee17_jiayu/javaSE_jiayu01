package com.ketang.demo05Warpper;

public class PhoneWarpper extends Phone{
    private Phone phone;

    public PhoneWarpper(Phone phone) {
        this.phone = phone;
    }

    @Override
    public void call() {
        System.out.println("打电话之前听彩铃");
        phone.call();
    }

    @Override
    public void sendMessage() {
        phone.sendMessage();
    }
}
