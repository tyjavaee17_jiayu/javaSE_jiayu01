package com.ketang.demo05Warpper;

public class DemoWarpper {
    public static void main(String[] args) {
        FIrstPhone p1 = new FIrstPhone();
        p1.call();
        p1.sendMessage();
        System.out.println("--------------------");
        PhoneWarpper pw1 = new PhoneWarpper(p1);
        pw1.call();
        pw1.sendMessage();

        System.out.println("--------------------");
        PhoneWarpper pw2 = new PhoneWarpper(new SecondPhone());
        pw2.call();
        pw2.sendMessage();

        System.out.println("--------------------");

    }
}
