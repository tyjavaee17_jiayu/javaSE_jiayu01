package com.ketang.demo03SerializableStream;

import java.io.*;
import java.util.ArrayList;

/*
    把多个对象存储到集合中
    对集合继续序列化和反序列化
 */
public class Demo04Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ArrayList<Person> list = new ArrayList<>();
        list.add(new Person("张三",18));
        list.add(new Person("李四",19));
        list.add(new Person("王五",20));
        list.add(new Person("赵六",21));

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("day17\\list.txt"));
        oos.writeObject(list);
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("day17\\list.txt"));
        Object object = ois.readObject();
        ArrayList<Person> list2 = (ArrayList<Person>) object;
        for (Person person : list2) {
            System.out.println(person);
        }
    }
}
