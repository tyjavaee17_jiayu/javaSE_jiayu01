package com.ketang.demo06Utils;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo01IoUtils {
    public static void main(String[] args) throws IOException {
        int fileSize = IOUtils.copy(new FileInputStream("day16\\student.txt"), new FileOutputStream("day17\\student01.txt"));
        System.out.println(fileSize);


        //(适合文件大小为2GB以上)
        //IOUtils.copyLarge(new FileInputStream(""),new FileOutputStream(""))
    }
}
