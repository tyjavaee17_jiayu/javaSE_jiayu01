package com.ketang.demo06Utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;

public class Demo02FileUtils {
    public static void main(String[] args) throws IOException {

    /*
        static String readFileToString(File file) 读取一个文件以字符串的形式返回
     */

        String s = FileUtils.readFileToString(new File("day17\\csb01.txt"));
        System.out.println(s);

    /*
        static void writeStringToFile(File file, String data) 把字符串写到一个文件中
     */

         FileUtils.writeStringToFile(new File("day17\\as.txt"),"我爱你");

    /*
        static void copyFile(File srcFile, File destFile) 文件复制
     */

    FileUtils.copyFile(new File("day16\\yyy.txt"),new File("day17\\yyy.txt"));

     /*
        static void copyFileToDirectory(File srcFile, File destDir) 把一个文件复制到另外一个文件夹中
     */

     FileUtils.copyFileToDirectory(new File("day17\\yyy.txt"),new File("d:\\"));

     /*
        static void copyDirectoryToDirectory(File srcDir, File destDir) 文件夹复制
        注意:
            底层使用递归技术
            IO的参数数据源和目的地不能是文件夹
     */

     FileUtils.copyDirectoryToDirectory(new File("d:\\多级目录"),new File("e:\\"));
    }
}
