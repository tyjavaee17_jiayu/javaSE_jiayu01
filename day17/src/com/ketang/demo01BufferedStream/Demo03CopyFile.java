package com.ketang.demo01BufferedStream;

import java.io.*;

public class Demo03CopyFile {
    public static void main(String[] args) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("day17\\c.txt"));
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("day17\\a.txt"));
        int len= 0;
        while ((len = bis.read())!=-1){
            bos.write(len);
        }
        bos.close();
        bis.close();
    }
}
