package com.ketang.demo01BufferedStream;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Demo04BufferedReader {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("day17\\b.txt"));
        //int read = br.read();
        //System.out.println((char) read);
        /*int len = 0;
        while ((len = br.read())!= -1){
            System.out.println((char) len);
        }*/

        String line;
        while ((line = br.readLine())!= null){
            System.out.println(line);
        }
        br.close();
    }
}
