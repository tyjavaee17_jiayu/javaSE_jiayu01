package com.ketang.demo01BufferedStream;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Demo06Test {
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader("day17\\csb01.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("day17\\csb02.txt"));
        String line;
        while ((line = br.readLine())!= null){
            list.add(line);
        }
        Collections.sort(list,(o1,o2)->o1.charAt(0)-o2.charAt(0));
        for (String s : list) {
            bw.write(s);
            bw.newLine();
        }
        bw.close();
        br.close();
    }
}
