package com.ketang.demo01BufferedStream;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo02BufferedOutPutStream {
    public static void main(String[] args) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("day17\\b.txt"));
        bos.write("中国".getBytes());
        bos.flush();
        bos.close();
    }
}
