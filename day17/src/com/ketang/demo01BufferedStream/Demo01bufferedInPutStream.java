package com.ketang.demo01BufferedStream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Demo01bufferedInPutStream {
    public static void main(String[] args) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("day17\\a.txt"));
        method01(bis);
        bis.close();
    }
    private static void method01(BufferedInputStream bis) throws IOException {
        bis.read();
        int len = 0;
        while((len = bis.read())!= -1){
            System.out.println((char)len);
        }
    }
}
