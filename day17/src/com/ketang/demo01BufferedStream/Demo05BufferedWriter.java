package com.ketang.demo01BufferedStream;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Demo05BufferedWriter {
    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("day17\\d.txt"));
        for (int i = 1; i <= 10; i++) {
            bw.write("HelloWorld" + i);
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }
}
