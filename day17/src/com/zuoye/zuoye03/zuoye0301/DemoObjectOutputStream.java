package com.zuoye.zuoye03.zuoye0301;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class DemoObjectOutputStream {
    public static void main(String[] args) throws IOException {
        Student stu = new Student("迪丽热巴", "女", 18);
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("day17\\test5_1.txt"));
        oos.writeObject(stu);
        oos.flush();
        oos.close();
    }
}
