package com.zuoye.zuoye03.zuoye0302;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DmeoObjectInputStream {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("day17\\test5_1.txt"));
        Object obj = ois.readObject();
        System.out.println(obj);
        ois.close();
    }
}
