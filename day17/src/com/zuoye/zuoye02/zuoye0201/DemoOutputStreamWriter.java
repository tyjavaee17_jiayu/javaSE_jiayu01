package com.zuoye.zuoye02.zuoye0201;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class DemoOutputStreamWriter {
    public static void main(String[] args) throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("day17\\test4_1.txt"), "GBK");
        osw.write("我要学号Java，我要月薪过万！");
        osw.flush();
        osw.close();
    }
}
