package com.zuoye.zuoye02.zuoye0202;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class DemoInputStreamReader {
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream("day17\\test4_1.txt"),"GBK");
        int len = 0;
        while ((len = isr.read())!= -1){
            System.out.print((char) len);
        }
        isr.close();
    }
}
