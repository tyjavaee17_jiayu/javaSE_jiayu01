package com.zuoye.zuoye01.zuoye0105;

import com.zuoye.zuoye01.zuoye0104.Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;

public class DemoBufferedWrter {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("day17\\test3_4.txt"));
        List<Student> stuList = new ArrayList<>();

        String line;

        while ((line = br.readLine())!= null){
            String[] arr = line.split(",");
            Student student = new Student(arr[0], arr[1], Integer.parseInt(arr[2]), Integer.parseInt(arr[3]));
            stuList.add(student);
        }

        for (Student student : stuList) {
            System.out.println(student);
        }

        br.close();
    }
}
