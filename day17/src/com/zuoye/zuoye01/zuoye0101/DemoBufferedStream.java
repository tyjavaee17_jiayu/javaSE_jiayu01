package com.zuoye.zuoye01.zuoye0101;

import sun.java2d.pipe.BufferedBufImgOps;

import java.io.*;

public class DemoBufferedStream {
    public static void main(String[] args) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("d:\\yyy.txt"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("e:\\yyy.txt"));
        int len = 0 ;
        while ((len = bis.read())!= -1){
            bos.write(len);
            bos.flush();
        }
        bos.close();
        bis.close();
    }
}
