package com.zuoye.zuoye01.zuoye0103;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DemoBufferedReader {
    public static void main(String[] args) throws IOException {
        List<String> list = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader("day17\\test3_2.txt"));
        String line;
        while ((line = br.readLine())!= null){
            list.add(line);
        }
        for (String s : list) {
            System.out.println(s);
        }
        br.close();
    }
}
