package com.zuoye.zuoye01.zuoye0102;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DemoBufferedWriter {
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        list.add("迪丽热巴");
        list.add("古力娜扎");
        list.add("周杰伦");
        list.add("蔡徐坤");

        BufferedWriter bw = new BufferedWriter(new FileWriter("day17\\test3_2.txt"));
        for (String s : list) {
            bw.write(s);
            bw.newLine();
        }
       bw.close();
    }
}
