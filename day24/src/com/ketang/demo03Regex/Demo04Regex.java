package com.ketang.demo03Regex;

public class Demo04Regex {
    public static void main(String[] args) {
        String str = "258";
        String regrx = "[0-9][0-9][0-9]";
        boolean b = str.matches(regrx);
        System.out.println("b=" + b);

        regrx = "\\d\\d\\d";
        boolean b1 = str.matches(regrx);
        System.out.println("b1= " + b1);

        regrx = "1[358]\\d\\d\\d\\d\\d\\d\\d\\d\\d";
        str = "13800138000";
        str = "23800238000";
        str = "138001380000";
        boolean b2 = str.matches(regrx);
        System.out.println("b2= " + b2);

        regrx = "h.d";
        str = "h%d";
        str = "h中d";
        str = "1中d";
        str = "hd";
        boolean b3 = str.matches(regrx);
        System.out.println("b3 = " + b3);

        regrx = "had\\.";
        str = "hadA";
        str = "had.";
        boolean b4 = str.matches(regrx);
        System.out.println("b4 = " + b4);
    }
}
