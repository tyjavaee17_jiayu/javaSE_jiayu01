package com.ketang.demo03Regex;
/*
    正则表达式-数量词
        语法示例：
        1. X? : 0次或1次
        2. X* : 0次到多次  任意次
        3. X+ : 1次或多次
        4. X{n} : 恰好n次 X=n次
        5. X{n,} : 至少n次  X>=n次
        6. X{n,m}: n到m次(n和m都是包含的)    n =< X <= m
 */
public class Demo05Regex {
    public static void main(String[] args) {
        String str = "1234";
        String regex = "\\d{4}";
        boolean b1 = str.matches(regex);
        System.out.println("b1=" + b1);

        regex = "\\d+";
        str = "1212121212122121212121";
        boolean b2 = str.matches(regex);
        System.out.println("b2=" + b2);

        regex = "\\d{5}";
        str = "123456";
        boolean b3 = str.matches(regex);
        System.out.println("b3= " + b3);

        regex = "\\d*\\.\\d*";
        str = "111.000";
        boolean b4 = str.matches(regex);
        System.out.println("b4=" +b4);

        regex = "\\d*.?\\d*";
        str = "100000";
        str = "11.22";
        str = "11..00";
        boolean b5 = str.matches(regex);
        System.out.println("b5=" + b5);

        regex = "[+-]?\\d+";
        str = "-1";
        str = "55555";
        boolean b6 = str.matches(regex);
        System.out.println("b6=" + b6);

        regex = "[1-9]\\d{4,14}";
        str = "1025485687";
        str = "11212121212f";
        boolean b7 = str.matches(regex);
        System.out.println("b7= " +b7);
    }
}
