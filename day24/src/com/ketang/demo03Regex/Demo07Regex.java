package com.ketang.demo03Regex;

public class Demo07Regex {
    public static void main(String[] args) {
     show01();
     show02();
     show03();
     show04();
    }

    private static void show04() {
        String s = "12121212sasasas12ds1d2s1ds2d2sd1s2d1s2d1s";
        System.out.println("原字符串：" + s);
        String replaceAll = s.replaceAll("\\d", "@_@");
        System.out.println("替换后的字符串："+ replaceAll);
        String s1 = s.replaceAll("\\d+", "@_@");
        System.out.println("替换后的字符串："+ s1);
    }

    private static void show03() {
        String s = "192      138    1 1";
        String[] split = s.split(" +");
        System.out.println(split.length);
        for (String s1 : split) {
            System.out.println(s1);
        }
    }

    private static void show02() {
        String s = "192.168.1.1";
        String[] split = s.split("\\.");
        for (String s1 : split) {
            System.out.println(s1);
        }
    }

    private static void show01() {
        String s = "11-22-33-44-55-66";
        String[] split = s.split("-");
        for (String s1 : split) {
            System.out.println(s1);
        }
    }
}
