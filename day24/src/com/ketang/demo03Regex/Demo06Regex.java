package com.ketang.demo03Regex;

public class Demo06Regex {
    public static void main(String[] args) {
        String str = "abc";
        str = "aaaaabababcccccabc";
        String regex = "(abc)*";
        boolean b1 = str.matches(regex);
        System.out.println("b1=" + b1);

        str = "DG8FV-B9TKY-FRT9J-99899-XPQ4G";
        regex = "([0-9A-Z]{5}-){4}[0-9A-Z]{5}";
        boolean b2 = str.matches(regex);
        System.out.println("b2=" + b2);
    }
}
