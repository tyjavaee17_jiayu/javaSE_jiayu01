package com.ketang.demo03Regex;

public class Demo02Regex {
    public static void main(String[] args) {
        String str = "had";
        String str1 = "hud";
        String str2 = "Had";

        String regex = "h[^aeiou]d";
        boolean b1 = str1.matches(regex);
        System.out.println("b1=" + b1);

        regex = "[a-z]ad";
        boolean b2 = str2.matches(regex);
        System.out.println("b2="+b2);

        regex = "[a-dm-p]ad";
        String str3 = "cad";
        boolean b3 = str3.matches(regex);
        System.out.println("b3=" + b3);
    }
}
