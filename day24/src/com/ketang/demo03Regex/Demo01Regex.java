package com.ketang.demo03Regex;

import java.util.Scanner;

public class Demo01Regex {
    public static void main(String[] args) {
        System.out.println("请输入一个QQ密码：");
        String passWord = new Scanner(System.in).nextLine();
        boolean b = checkQQRegex(passWord);
        System.out.println(b);
    }
    public static boolean checkQQRegex(String qq){
        return qq.matches("[1-9][0-9]{4,14}");
    }
}
