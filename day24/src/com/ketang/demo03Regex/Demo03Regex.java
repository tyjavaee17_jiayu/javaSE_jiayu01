package com.ketang.demo03Regex;

public class Demo03Regex {
    public static void main(String[] args) {
        String str = "had";
        String regex = "[[a-z]&&[^aeiou]]ad";
        boolean b = str.matches(regex);
        System.out.println("b=" + b);

        //regex = "[aeiou]ad"简化写法，省略 | 或者 |
        regex = "[a||e||i||o||u]";
        String str1 = "aad";
        boolean matches = str1.matches(regex);
        System.out.println("matches="+matches);
    }
}
