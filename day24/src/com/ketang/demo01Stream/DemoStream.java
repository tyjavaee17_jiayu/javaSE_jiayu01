package com.ketang.demo01Stream;

import java.util.stream.Stream;

public class DemoStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("a", "b", "c", "d", "e", "f");
        stream.forEach(System.out::println);
    }
}
