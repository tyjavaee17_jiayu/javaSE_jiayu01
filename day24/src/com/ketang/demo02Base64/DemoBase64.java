package com.ketang.demo02Base64;

import java.util.Base64;

public class DemoBase64 {
    public static void main(String[] args) {
        String s = "我家保险柜的密码";
        System.out.println("原字符："+ s);
        String s1 = Base64.getEncoder().encodeToString(s.getBytes());
        System.out.println("加密后的字符串：" + s1);

        byte[] decode = Base64.getDecoder().decode(s1);
        String s2 = new String(decode);
        System.out.println("机密后的字符串：" + s2);
    }
}
