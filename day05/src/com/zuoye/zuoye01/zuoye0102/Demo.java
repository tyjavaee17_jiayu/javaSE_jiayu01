package com.zuoye.zuoye01.zuoye0102;

import java.math.BigDecimal;

public class Demo {
    public static void main(String[] args) {
        Student s1 = new Student("张三","男",20,79.5);
        Student s2 = new Student("李四","女",21,80.2);
        Student s3 = new Student("王五","男",22,77.9);
        Student s4 = new Student("周六","男",20,55.8);
        Student s5 = new Student("赵七","女",21,99.9);
        BigDecimal bd = new BigDecimal("0");
         bd = bd.add(BigDecimal.valueOf(s1.getScore()));
         bd = bd.add(BigDecimal.valueOf(s2.getScore()));
         bd = bd.add(BigDecimal.valueOf(s3.getScore()));
         bd = bd.add(BigDecimal.valueOf(s4.getScore()));
         bd = bd.add(BigDecimal.valueOf(s5.getScore()));
        BigDecimal bd1 = new BigDecimal("5");
        double v = bd.divide(bd1, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out.println("平均分时："+ v);

    }
}
