package com.zuoye.zuoye01.zuoye0101;

import java.math.BigDecimal;

//有以下double数组：
//		double[] arr = {0.1,0.2,2.1,3.2,5.56,7.21};
//	请编程计算它们的总值及平均值(四舍五入保留小数点后2位)
public class ZuoYe0101 {
    public static void main(String[] args) {
        double[] arr = {0.1,0.2,2.1,3.2,5.56,7.21};
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        BigDecimal bd = new BigDecimal(sum);
        BigDecimal bd1 = new BigDecimal(arr.length);
        System.out.println("数组的和：" + bd);
        BigDecimal divide = bd.divide(bd1, 2, BigDecimal.ROUND_HALF_UP);
        System.out.println("数组的平均值时：" + divide);


    }
}
