package com.zuoye.zuoye03.zuoye0302;

import java.util.Scanner;

//请定义学员类，有以下成员属性：
//	姓名：String类型
//	年龄：int
//	身高：double
//	婚否：boolean
//	性别：char
//
//请从控制台接收以下数据：
//	姓名：王哈哈
//	年龄：24
//	身高：1.82
//	婚否：false
//	性别：男
//	以上数据要求全部使用String类型接收
//
//请创建“学员对象”，并将所有数据转换后，存储到这个对象中，
//最后打印此对象的所有属性。
public class Demo {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);

        System.out.println("请输入姓名:");
        String nameStr=sc.nextLine();

        System.out.println("请输入年龄:");
        String ageStr=sc.nextLine();

        System.out.println("请输入身高:");
        String heightStr=sc.nextLine();

        System.out.println("请输入婚否:");
        String flagStr=sc.nextLine();

        System.out.println("请输入性别:");
        String sexStr=sc.nextLine();

        int age = Integer.parseInt(ageStr);
        double height = Double.parseDouble(heightStr);
        boolean flag = Boolean.parseBoolean(flagStr);
        char sex = sexStr.charAt(0);
        Student s = new Student(nameStr,age,height,flag,sex);
        System.out.println(s);
    }
}
