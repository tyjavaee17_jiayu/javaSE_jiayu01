package com.zuoye.zuoye03.zuoye0302;

public class Student {
    private String name;
    private int age;
    private double height;
    private boolean hunfou;
    private char sex;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", hunfou=" + hunfou +
                ", sex=" + sex +
                '}';
    }

    public Student() {
    }

    public Student(String name, int age, double height, boolean hunfou, char sex) {
        this.name = name;
        this.age = age;
        this.height = height;
        this.hunfou = hunfou;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public boolean isHunfou() {
        return hunfou;
    }

    public void setHunfou(boolean hunfou) {
        this.hunfou = hunfou;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }
}
