package com.zuoye.zuoye04.zuoye0404;

import java.util.Arrays;
import java.util.Scanner;

//程序模拟一个论坛发帖的过程，请用户输入一个发帖内容，例如：
//		“积分看电视普京了几分我特朗普将反恐文件风IE哦特朗普积分哦忘记普京”
//请定义一个关键字数组：
//		String[] keyArray = {“特朗普”,”普京”};
//	请将字符串中包含的数组中的关键字替换为*符号。
public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个帖子：");
        String s = sc.nextLine();
        String[] keyArray = {"特朗普","普京"};
        for (int i = 0; i < keyArray.length; i++) {
            String s1 = keyArray[i];
             s = s.replace(s1, "*");
        }
        System.out.println(s);
    }
}
