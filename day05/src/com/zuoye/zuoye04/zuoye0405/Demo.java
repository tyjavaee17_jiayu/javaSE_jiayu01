package com.zuoye.zuoye04.zuoye0405;

import java.util.Scanner;

//程序从控制台接收一个java文件的文件名，例如：test.java，TEST.java，tEst.JAVA，请编程实现以下功能：
//*_获取，并打印文件名的第一个字符；
//*_获取，并打印文件的后缀名(包括.符号)，例如：.java
//*_无论原文件名什么样，最终将其转换为：Test.java的形式，打印转换后的文件名。
public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个java文件名：");
        String s = sc.nextLine();
        char c = s.charAt(0);
        System.out.println(c);
        System.out.println("-------------");
        int i = s.lastIndexOf(".");
        String substring = s.substring(i);
        System.out.println(substring);
        System.out.println("-------------");
        String s1 = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
        System.out.println(s1);

    }
}
