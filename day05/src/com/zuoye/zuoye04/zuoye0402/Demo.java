package com.zuoye.zuoye04.zuoye0402;

import java.util.Scanner;

//请用户从控制台输入一个java文件的名字，例如：Test.java，请编程实现以下功能：
//*_判断此文件名是否以“.java”结尾，并打印结果
//*_获取此文件中.符号的索引位置，并打印结果
public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个java文件的名称：");
        String s = sc.nextLine();
        boolean b = s.endsWith(".java");
        System.out.println("是否含有.java的文件："+ b);
        int indexOf = s.indexOf(".");
        System.out.println(".的索引值是："+ indexOf);
    }
}
