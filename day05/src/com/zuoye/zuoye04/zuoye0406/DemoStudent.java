package com.zuoye.zuoye04.zuoye0406;

import java.util.Scanner;

public class DemoStudent {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入学生的信息：");
        String nameStr = sc.nextLine();
       /* System.out.println("请输入学生的年龄：");
        String ageStr = sc.nextLine();
        System.out.println("请输入学生的性别：");
        String sexStr = sc.nextLine();
        int age = Integer.parseInt(ageStr);
        char sex = sexStr.charAt(0);
        Student s = new Student(nameStr,age,sex);
        System.out.println(s);*/
        String replaceAll = nameStr.replaceAll(" ", "");
        String[] split = replaceAll.split(",");

        String name = split[0];
        int age = Integer.parseInt(split[1]);
        char sex = split[2].charAt(0);
        Student s = new Student(name,age,sex);
        System.out.println(s);
    }
}
