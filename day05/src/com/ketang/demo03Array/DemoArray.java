package com.ketang.demo03Array;

import java.util.Arrays;

/*
    java.util.Arrays类:操作数组的工具类
        此类包含用来操作数组（比如排序和搜索）的各种方法。
        &Arrays类中的方法都是静态的,可以通过类名.方法名(参数)直接使用&
    Arrays类中的成员方法:
        static String toString(Object[] a) 返回指定数组内容的字符串表示形式。
            对数组进行遍历,把数组中的元素组合成一个字符串返回
        static void sort(Object[] a) 根据元素的自然顺序对指定对象数组按升序进行排序。
 */
public class DemoArray {
    public static void main(String[] args) {
        int[] arr = {1,8,6,5,6,9,10,16};
        String s = Arrays.toString(arr);
        System.out.println(s);
        System.out.println("--------------");
        System.out.println("排序前的数组："+ Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println("排序后的数组："+ Arrays.toString(arr));

    }
}
