package com.ketang.demo06;
/*
    类名作为方法参数和返回值
 */
public class Demo {
    public static void main(String[] args) {
     show01(new Person());

     System.out.println("------------");

     Person pp = show02();
     pp.eat();

    }
    //类名作为方法返回值
    private static Person show02() {
        return  new Person();
    }

    //类名作为方法参数
    private static void show01(Person p) {
        p.eat();
    }

}
