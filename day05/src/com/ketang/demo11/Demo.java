package com.ketang.demo11;
/*
    接口作为成员变量
    好处:
        可以赋值不同的实现类对象,扩展性强
 */
public class Demo {
    public static void main(String[] args) {
        Student s1 = new Student();
        s1.setName("迪丽热巴");
        s1.setAge(18);
        s1.setFly(new MaQue());
        System.out.println(s1);

        System.out.println("-------------");
        Student s2 = new Student("古力娜扎",18,new Ying());
        System.out.println(s2);
    }
}
