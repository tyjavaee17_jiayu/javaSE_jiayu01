package com.ketang.demo11;

public class Student {
    private String name;
    private int age;
    private Fly fly;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", fly=" + fly +
                '}';
    }

    public Student() {
    }

    public Student(String name, int age, Fly fly) {
        this.name = name;
        this.age = age;
        this.fly = fly;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Fly getFly() {
        return fly;
    }

    public void setFly(Fly fly) {
        this.fly = fly;
    }
}
