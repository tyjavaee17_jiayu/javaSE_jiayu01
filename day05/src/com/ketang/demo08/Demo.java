package com.ketang.demo08;
/*
    接口作为方法参数和返回值
 */
public class Demo {
    public static void main(String[] args) {
        show01(new MaQue());
        show01(new Ying());
        System.out.println("-----------------");
        Fly f1 = getFly1();
        f1.fly();
        Fly f2 = getFly2();
        f2.fly();

    }
    //接口作为方法返回值

    /*
       定义一个方法,方法的返回值类型使用Fly接口
       在方法中就需要返回Fly接口的任意实现类对象
    */
    private static Fly getFly1() {
        return new MaQue();
    }
    //接口作为方法返回值
    private static Fly getFly2() {
        return new Ying();
    }

    /*
        定义一个方法,方法的参数使用Fly接口类型的变量
        调用方法需要传递Fly接口的实现类对象Fly变量赋值
        多态:
            Fly f =new Ying();
            Fly f =new MaQue()
     */
    //接口作为方法参数
    private static void show01(Fly f) {
        f.fly();
    }
}
