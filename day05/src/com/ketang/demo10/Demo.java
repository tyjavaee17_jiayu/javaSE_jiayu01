package com.ketang.demo10;
/*
    抽象类作为成员变量
    好处:
        可以赋值不同的子类对象,扩展性强
 */
public class Demo {
    public static void main(String[] args) {
        Person p = new Person();
        p.setName("光头强");
        p.setAge(18);
        p.setAnimal(new Dog());
        System.out.println(p);

        System.out.println("--------------");

        Person p1 = new Person("熊大",18,new Cat());
        System.out.println(p1);

    }
}
