package com.ketang.demo01BigInteger;

import java.math.BigInteger;

public class DemoBigInteger {
    public static void main(String[] args) {
        BigInteger b1 = new BigInteger("12121212121212121212121212121");
        BigInteger b2 = new BigInteger("12121212121212121212121212121");
        BigInteger add = b1.add(b2);
        BigInteger subtract = b1.subtract(b2);
        BigInteger multiply = b1.multiply(b2);
        BigInteger divide = b1.divide(b2);
        System.out.println(add);
        System.out.println(subtract);
        System.out.println(multiply);
        System.out.println(divide);
    }

}
