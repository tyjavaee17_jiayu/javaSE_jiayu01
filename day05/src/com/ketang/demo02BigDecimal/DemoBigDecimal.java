package com.ketang.demo02BigDecimal;

import java.math.BigDecimal;

public class DemoBigDecimal {
    public static void main(String[] args) {
        BigDecimal b1 = new BigDecimal("0.0012");
        BigDecimal b2 = new BigDecimal("0.003");
        BigDecimal add = b1.add(b2);
        BigDecimal subtract = b1.subtract(b2);
        BigDecimal multiply = b1.multiply(b2);
        BigDecimal divide = b1.divide(b2);
        System.out.println(add);
        System.out.println(subtract);
        System.out.println(multiply);
        System.out.println(divide);
        System.out.println("-------------------");
        BigDecimal b3 = new BigDecimal("0.7");
        // //BigDecimal.ROUND_HALF_UP   （四舍五入模式）
        BigDecimal divide1 = b1.divide(b3, 3, BigDecimal.ROUND_HALF_UP);
        System.out.println(divide1);
    }
}
