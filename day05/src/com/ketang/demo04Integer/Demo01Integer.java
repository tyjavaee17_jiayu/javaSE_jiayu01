package com.ketang.demo04Integer;
/*
    包装类:
        使用一个类把基本类型的数据装起来
        在类中定义一些方法,才操作这些基本数据类型的值
    基本数据类型:4类8种
        byte    short   int     long    float   double  char    boolean
    基本数据类型对应的包装类: java.lang包
        Byte    Short   Integer Long    Float   Double  Character Boolean
    java.lang.Integer类
        Integer 类在对象中包装了一个基本类型 int 的值。
 */
public class Demo01Integer {
    public static void main(String[] args) {
        //Integer(int value)  传递一个整数
        Integer in1 = new Integer(10);
        System.out.println(in1);
        //Integer(String s) 传递一个字符串类型的整数
        Integer in2 = new Integer("100");
        System.out.println(in2);
        //static Integer valueOf(int i) 传递一个整数
        Integer in3 = Integer.valueOf(10);
        System.out.println(in3);
        //static Integer valueOf(String s) 传递一个字符串类型的整数
        Integer in4 = Integer.valueOf("100");
        System.out.println(in4);
        System.out.println("----------------------");
        //拆箱
        int i = in1.intValue();
        System.out.println(i);
    }
}
