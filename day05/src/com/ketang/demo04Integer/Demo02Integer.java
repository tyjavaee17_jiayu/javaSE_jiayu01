package com.ketang.demo04Integer;
/*
    自动装箱和自动拆箱:在JDK1.5以后,装箱和拆箱可以自动进行
        基本数据类型的值和包装类可以自动转换
                装箱:基本类型-->包装类

                拆箱:包装类-->基本类型

 */
public class Demo02Integer {
    public static void main(String[] args) {
        Integer i = 10;//隐含了一个创建对象的过程
        i = i + 1;//自动拆箱
        System.out.println(i);
    }
}
