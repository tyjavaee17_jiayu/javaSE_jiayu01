package com.ketang.demo04Integer;
/*
        基本类型与字符串之间的转换(重点)
        1.基本数据类型-->字符串类型
        a.基本数据类型的值+"":工作中最常用  1+""==>"1"
        b.使用包装类中的静态方法toString
static String toString(int i) 返回一个表示指定整数的 String 对象。
        c.使用String类中的静态方法valueOf
static String valueOf(int i) 返回 int 参数的字符串表示形式。
        2.字符串类型-->基本数据类型
        在每个包装类中都有一个parseXXX方法,可以把字符串格式的基本数据类型的值,转换为基本数据类型
        Integer类: static int parseInt(String s)
        Double类:  static double parseDouble(String s)
        ...
        注意:
        字符串必须传递基本数据类型的字符串,否则会抛出数字格式化异常
        */
public class Demo03Integer {
    public static void main(String[] args) {
        //1.基本数据类型-->字符串类型
        //a.基本数据类型的值+"":工作中最常用  1+""==>"1"
        String s1 = 1 + "abc";
        System.out.println(s1+10);

        //b.使用包装类中的静态方法toString
        //static String toString(int i) 返回一个表示指定整数的 String 对象。
        String s2 = Integer.toString(10);
        System.out.println(s2+20);

        //c.使用String类中的静态方法valueOf
        //static String valueOf(int i) 返回 int 参数的字符串表示形式

        Integer s3 = Integer.valueOf(20);
        System.out.println(s3+50);
        //2.字符串类型-->基本数据类型（解析）

        int i = Integer.parseInt("666");
        System.out.println(i+6.6);

        double v = Double.parseDouble("6.6");
        System.out.println(v);

        byte b = Byte.parseByte("66");
        System.out.println(b);

        short i1 = Short.parseShort("6");
        System.out.println(i1);

        long l = Long.parseLong("66666666666666666");
        System.out.println(l);

        float v1 = Float.parseFloat("6.6");
        System.out.println(v1);

        boolean aTrue = Boolean.parseBoolean("true");
        System.out.println(aTrue);
    }
}
