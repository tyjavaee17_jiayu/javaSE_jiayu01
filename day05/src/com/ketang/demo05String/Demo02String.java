package com.ketang.demo05String;

import java.util.Arrays;

public class Demo02String {
    public static void main(String[] args) {
        show01();
        System.out.println("--------------");
        show02();
        System.out.println("--------------");
        show03();
        System.out.println("--------------");
        show04();
        System.out.println("--------------");
        show05();
        System.out.println("--------------");
        show06();
        System.out.println("--------------");
        show07();
        System.out.println("--------------");
        show08();
        System.out.println("--------------");
        show09();
        System.out.println("--------------");
        show10();
    }
    /*
        String[] split(String regex)  根据自定的字符串对字符串进行切割
     */
    private static void show10() {
        String s12 = "a b c d e f g";
        String[] s = s12.split(" ");
        System.out.println(Arrays.toString(s));
    }
    /*
       String trim() 去掉字符串两端的空格 "       abc   def   www    "==>"abc   def   www"
    */
    private static void show09() {
        String s11 = "          asd         gfhg       ghf        ";
        String trim = s11.trim();
        System.out.println(trim);
    }

    private static void show08() {
        String s9 = "我爱你abc";
        String ss = s9.toUpperCase();
        System.out.println(ss);
        String s10 = "我爱你ABC";
        String sss = s10.toLowerCase();
        System.out.println(sss);
    }
    /*
        char[] toCharArray() 将此字符串转换为一个新的字符数组。
       byte[] getBytes()  查询系统默认的编码把字符串转换为字节数组
     */
    private static void show07() {
        String s8 = "开始索引";
        char[] chars = s8.toCharArray();
        System.out.println(chars);
        byte[] bytes = s8.getBytes();
        System.out.println(Arrays.toString(bytes));
    }

    private static void show06() {
        String s7 = "过河拆桥，无中生有!";
        String s = s7.substring(5);//从开始索引beginIndex截取字符串到字符串的末尾
        System.out.println(s);
        String s1 = s7.substring(5, 9);//从开始索引beginIndex到结束索引endIndex截取字符串;包含头,不包含尾
        System.out.println(s1);
        String s2 = s7.substring(5, 10);
        System.out.println(s2);
    }

    private static void show05() {
        String s6 = "你好aaa我好aaa大家好aaa就是好！";
        String s = s6.replace("aaa", "AAA");
        System.out.println(s);
    }
    //如果大的字符串中有重复的被查找的字符串,找到了第一个就不在找了
    //    indexOf(String str)
    // 如果大的字符串中有重复的被查找的字符串,找到了第最后一个就不在找了
    //    lastIndexOf(int ch)
    private static void show04() {
        String s5 = "你好aaa我好aaa大家好aaa就是好！";
        int index = s5.indexOf("aaa");
        System.out.println(index);
        int lastIndexOf1 = s5.lastIndexOf("aaa");
        System.out.println(lastIndexOf1);
        int lastIndexOf = s5.lastIndexOf("就是好");
        System.out.println(lastIndexOf);
        int index1 = s5.indexOf("东方红");
        System.out.println(index1);
        int lastIndexOf2 = s5.lastIndexOf("东方红");
        System.out.println(lastIndexOf2);

    }

    private static void show03() {
        String s4 = "HelloWorld.java";
        boolean b = s4.startsWith("Hello");
        System.out.println(b);
        boolean b1 = s4.endsWith(".java");
        System.out.println(b1);
    }
    // 只要是String类中的方法参数是CharSequence,都可以传递字符串(多态)
    private static void show02() {
        String s3 = "东方红，太阳升，雄赳赳，气昂昂！";
        boolean b = s3.contains("太阳升");
        System.out.println(b);
        boolean b1 = s3.contains("达康书记");
        System.out.println(b1);
    }

    private static void show01() {
        String s1 = "abc";
        String s2 = "123";
        String s = s1.concat(s2);
        System.out.println(s);
    }

}
