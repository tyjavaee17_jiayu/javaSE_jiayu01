package com.ketang.demo05String;

public class Demo01String {
    public static void main(String[] args) {
        String s1 = new String("abcd");
        System.out.println("s1=" + s1);
        byte[] arr1 = {97,98,99,100,101};
        String s2 = new String(arr1);
        System.out.println("s2=" + s2);
        char[] arr2 = {'A','B','C','D','E'};
        String s3 = new String(arr2);
        System.out.println("s3=" + s3);
        String s4 = "abc";
        System.out.println("s4=" + s4);
        String s5 = "abc";
        System.out.println("s5=" + s5);
        String s6 = "abcd";
        System.out.println("s6=" + s6);
        System.out.println(s4==s5);
        System.out.println(s5==s6);
    }
}
