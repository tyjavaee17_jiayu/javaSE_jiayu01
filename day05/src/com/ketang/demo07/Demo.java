package com.ketang.demo07;
/*
    抽象类作为方法参数和返回值
 */
public class Demo {
    public static void main(String[] args) {
     show01(new Cat());
     show01(new Dog());
        System.out.println("-------------");
        Animal animal1 = getAnimal1();
        animal1.eat();
        Animal animal2 = getAnimal2();
        animal2.eat();
    }
    //抽象类作为方法返回值

    /*
    定义一个方法,方法的返回值类型使用Animal
            但是Animal是一个抽象类无法直接创建对象返回
    所以可以返回Animal的任意子类对象
     */
    private static Animal getAnimal1() {
        return new Cat();
    }
    private static Animal getAnimal2() {
        return new Dog();
    }
    //抽象类作为方法参数
    private static void show01(Animal a) {
        a.eat();
    }
}
