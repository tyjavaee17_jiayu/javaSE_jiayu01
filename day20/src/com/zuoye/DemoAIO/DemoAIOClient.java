package com.zuoye.DemoAIO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Future;

public class DemoAIOClient {
    public static void main(String[] args) throws IOException {
        AsynchronousSocketChannel socketChannel = AsynchronousSocketChannel.open();

        Future<Void> future = socketChannel.connect(new InetSocketAddress("127.0.0.1", 6666));

        System.out.println(future.isDone());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(future.isDone());
        if (future.isDone()){
            socketChannel.write(ByteBuffer.wrap("你好，服务器！！！".getBytes()));
        }
        socketChannel.close();
    }
}
