package com.zuoye.DemoAIO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.TimeUnit;

public class DemoAIOServer {
    public static void main(String[] args) throws IOException {
        AsynchronousServerSocketChannel serverSocketChannel = AsynchronousServerSocketChannel.open();

        AsynchronousServerSocketChannel socketChannel = serverSocketChannel.bind(new InetSocketAddress(6666));

        serverSocketChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Object>() {
            @Override
            public void completed(AsynchronousSocketChannel result, Object attachment) {
                System.out.println("客户端连接成功！！！");
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                result.read(buffer, 10, TimeUnit.SECONDS, null, new CompletionHandler<Integer, Object>() {
                    @Override
                    public void completed(Integer result, Object attachment) {
                        System.out.println("接收到来自客户端的数据！！！");
                        buffer.flip();
                        String s = new String(buffer.array(), 0, buffer.limit());
                        System.out.println("来自客户端的数据是："+s);
                    }

                    @Override
                    public void failed(Throwable exc, Object attachment) {
                        System.out.println("还没接受到来自客户端的数据！！！");
                    }
                });
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                System.out.println("客户端连接失败！！！");
            }
        });
        while (true){}
    }
}
