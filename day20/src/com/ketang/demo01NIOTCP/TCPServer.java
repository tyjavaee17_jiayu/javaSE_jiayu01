package com.ketang.demo01NIOTCP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class TCPServer {
    public static void main(String[] args) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(6666));
        serverSocketChannel.configureBlocking(false);

        System.out.println("服务器等待客户端链接。。。。。");
        SocketChannel socketChannel = serverSocketChannel.accept();
        System.out.println("有客户端连接服务器。。。。");
    }
}
