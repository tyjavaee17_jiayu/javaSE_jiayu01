package com.ketang.demo05Selector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Set;

public class TCPServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocketChannel channel01 = ServerSocketChannel.open();
        ServerSocketChannel channel02 = ServerSocketChannel.open();
        ServerSocketChannel channel03 = ServerSocketChannel.open();

        channel01.bind(new InetSocketAddress(7777));
        channel02.bind(new InetSocketAddress(8888));
        channel03.bind(new InetSocketAddress(9999));

        channel01.configureBlocking(false);
        channel02.configureBlocking(false);
        channel03.configureBlocking(false);

        Selector selector = Selector.open();

        channel01.register(selector, SelectionKey.OP_ACCEPT);
        channel02.register(selector, SelectionKey.OP_ACCEPT);
        channel03.register(selector, SelectionKey.OP_ACCEPT);

        //Selector的keys()方法:已注册通道的集合
        Set<SelectionKey> keys = selector.keys();
        System.out.println("已注册的通道的数量："+ keys.size());

        while(true){
            //Selector的select()方法:获取连接的客户端的数量,没有客户端连接服务区会一直阻塞
            int select = selector.select();
            System.out.println("连接的客户端的数量：" + select);

            //Selector的selectedKeys()方法:当前已连接的通道的集合
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            System.out.println("已连接的通道的数量："+ selectionKeys.size());

            //获取完一个客户端的连接,睡眠2秒
            Thread.sleep(2000);
        }
    }
}
