package com.ketang.demo06Selector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class TCPServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocketChannel channel01 = ServerSocketChannel.open();
        ServerSocketChannel channel02 = ServerSocketChannel.open();
        ServerSocketChannel channel03 = ServerSocketChannel.open();

        channel01.bind(new InetSocketAddress(7777));
        channel02.bind(new InetSocketAddress(8888));
        channel03.bind(new InetSocketAddress(9999));

        channel01.configureBlocking(false);
        channel02.configureBlocking(false);
        channel03.configureBlocking(false);

        Selector selector = Selector.open();

        channel01.register(selector, SelectionKey.OP_ACCEPT);
        channel02.register(selector, SelectionKey.OP_ACCEPT);
        channel03.register(selector, SelectionKey.OP_ACCEPT);

       /* //Selector的keys()方法:已注册通道的集合
        Set<SelectionKey> keys = selector.keys();
        System.out.println("已注册的通道的数量："+ keys.size());*/

        while(true){
            //Selector的select()方法:获取连接的客户端的数量,没有客户端连接服务区会一直阻塞
            System.out.println("服务器等待客户端连接。。。");
            int select = selector.select();
            System.out.println("连接的客户端的数量：" + select);

            //Selector的selectedKeys()方法:当前已连接的通道的集合
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            System.out.println("已连接的通道的数量："+ selectionKeys.size());

            Iterator<SelectionKey> it = selectionKeys.iterator();
            while (it.hasNext()){
                SelectionKey selectionKey = it.next();
                ServerSocketChannel channel = (ServerSocketChannel) selectionKey.channel();
                System.out.println("获取当前通道ServerSocketChannel监听的端口号：" + channel.getLocalAddress());
                SocketChannel socketChannel = channel.accept();
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                int len = socketChannel.read(buffer);
                buffer.flip();
                String msg = new String(buffer.array(), 0, buffer.limit());
                System.out.println("服务器收到客户端读取的数据是："+ msg);

                it.remove();


            }
            //获取完一个客户端的连接,睡眠2秒
            Thread.sleep(2000);
        }
    }
}
