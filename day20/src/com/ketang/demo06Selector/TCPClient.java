package com.ketang.demo06Selector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class TCPClient {
    public static void main(String[] args) {
        new Thread(()->{
            while (true){
                try (SocketChannel socketChannel = SocketChannel.open();){
                    System.out.println("客户端开始连接7777端口.。。");
                    socketChannel.connect(new InetSocketAddress("127.0.0.1",7777));
                    System.out.println("客户端连接7777端口成功。。。。");
                    break;
                } catch (IOException e) {
                    System.out.println("客户端连接7777端口失败。。。");
                }
            }
        }).start();

        new Thread(()->{
            while (true){
                try (SocketChannel socketChannel = SocketChannel.open();){
                    System.out.println("客户端开始连接8888端口.。。");
                    socketChannel.connect(new InetSocketAddress("127.0.0.1",8888));
                    System.out.println("客户端连接8888端口成功。。。。");
                    break;
                } catch (IOException e) {
                    System.out.println("客户端连接8888端口失败。。。");
                }
            }
        }).start();

        new Thread(()->{
            while (true){
                try (SocketChannel socketChannel = SocketChannel.open();){
                    System.out.println("客户端开始连接9999端口.。。");
                    socketChannel.connect(new InetSocketAddress("127.0.0.1",9999));
                    System.out.println("客户端连接9999端口成功。。。。");
                    break;
                } catch (IOException e) {
                    System.out.println("客户端连接9999端口失败。。。");
                }
            }
        }).start();
    }
}
