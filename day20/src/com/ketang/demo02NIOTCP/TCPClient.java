package com.ketang.demo02NIOTCP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
/*
    成员方法:
        SelectableChannel configureBlocking​(boolean block) 设置阻塞模式 true:阻塞(不写默认)  false:非阻塞
        boolean connect​(SocketAddress remote) 根据服务器的ip地址和端口号连接服务器
            客户端设置为阻塞模式:connect方法会多次尝试连接服务器
                连接服务器成功connect方法返回true
                连接服务器器失败,会抛出连接异常
            客户端设置为非阻塞模式:connect方法只会连接一次服务器
                connect方法无论连接成功还是失败都返回false
                所以客户端设置为非阻塞模式没有意义
 */
public class TCPClient {
    public static void main(String[] args)  {
        while (true){
            try {
                SocketChannel socketChannel = SocketChannel.open();
                //使用SocketChannel对象中的方法connect根据服务器的ip地址和端口号连接服务器
                boolean connect = socketChannel.connect(new InetSocketAddress("127.0.0.1", 8888));
                System.out.println(connect);
                System.out.println("客户端连接服务器成功，结束轮询。。。");
                break;
            } catch (IOException e) {
                System.out.println("客户端连接失败，休息2秒钟，干点别的事情。。。。");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
