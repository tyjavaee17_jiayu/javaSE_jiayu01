package com.ketang.demo02NIOTCP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class TCPServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(8888));
        //设置服务器的非阻塞模式 SelectableChannel configureBlocking​(boolean block)
        // 设置阻塞模式 true:阻塞(不写默认)  false:非阻塞
        serverSocketChannel.configureBlocking(false);
        while (true){
            System.out.println("服务器等待客户端的连接。。。");
            SocketChannel socketChannel = serverSocketChannel.accept();
            if (socketChannel!= null){
                System.out.println("有客户端连接服务器。。。。");
                break;
            }else{
                System.out.println("没有客户端连接服务器，休息2秒钟，干点其他事情，在继续轮询");
                Thread.sleep(2000);
            }
        }
    }
}
