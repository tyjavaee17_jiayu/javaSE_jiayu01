package com.ketang.demo07AIO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.TimeUnit;

public class TCPServer {
    public static void main(String[] args) throws IOException {
        AsynchronousServerSocketChannel serverSocketChannel = AsynchronousServerSocketChannel.open();

        serverSocketChannel.bind(new InetSocketAddress(7777));

        System.out.println("accept方法开始执行了。。。");

        serverSocketChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Object>() {
            @Override
            public void completed(AsynchronousSocketChannel result, Object attachment) {

                System.out.println("客户端连接服务器成功。。。");

                ByteBuffer buffer = ByteBuffer.allocate(1024);

                result.read(buffer, 7, TimeUnit.SECONDS, null, new CompletionHandler<Integer, Object>() {
                    @Override
                    public void completed(Integer result, Object attachment) {

                        System.out.println("服务器读取客户端发送信息成功，执行方法");

                        buffer.flip();//缩小limit范围

                        String msg = new String(buffer.array(), 0, buffer.limit());

                        System.out.println("服务器读取到客户端的发送的信息："+msg);
                    }

                    @Override
                    public void failed(Throwable exc, Object attachment) {
                        System.out.println("服务器读取客户端发送失败成功,执行的方法");
                    }
                });
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                System.out.println("客户端连接服务器失败...");
            }
        });
        while (true){}//让程序一直执行
    }
}
