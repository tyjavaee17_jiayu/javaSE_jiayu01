package com.ketang.demo03NIOTCP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class TCPServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(9999));
        serverSocketChannel.configureBlocking(false);//非阻塞
        while (true){
            System.out.println("服务器等待客户端的连接。。。");
            SocketChannel socketChannel = serverSocketChannel.accept();
            if (socketChannel != null){
                System.out.println("有客户端连接服务器。。。");
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                socketChannel.read(buffer);
                buffer.flip();
                String msg = new String(buffer.array(), 0, buffer.limit());
                System.out.println("服务器读取客户端发送的数据："+ msg);

                socketChannel.write(ByteBuffer.wrap("收到谢谢".getBytes()));
                break;
            }else{
                System.out.println("没有客户端连接服务器，休息2秒钟，干点别的事情，再继续轮询。。。");
                Thread.sleep(2000);
            }
        }
    }
}
