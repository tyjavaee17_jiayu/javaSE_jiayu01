package com.ketang.demo03NIOTCP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class TCPClient {
    public static void main(String[] args){
        //客户端轮询连接服务器,连接成功,结束轮询
        while (true){
            try {
                SocketChannel socketChannel = SocketChannel.open();

                boolean connect = socketChannel.connect(new InetSocketAddress("127.0.0.1", 9999));

                //int write​(ByteBuffer src) 连接服务器成功,给服务器发送数据
                ByteBuffer buffer = ByteBuffer.wrap("你好，服务器".getBytes());

                //System.out.println("容量："+ buffer.capacity());
                //System.out.println("索引："+ buffer.position());
                //System.out.println("限定：" + buffer.limit())  ;

                socketChannel.write(buffer);

                //int read​(ByteBuffer dst) 读取服务器回写的数据
                ByteBuffer buffer1 = ByteBuffer.allocate(1024);

                socketChannel.read(buffer1);

                buffer1.flip();

                String msg = new String(buffer1.array(), 0, buffer1.limit());
                System.out.println("客户端收到的服务器发送的数据："+   msg);

                System.out.println("客户端连接服务器成功，结束轮询。。。");

                break;
            } catch (IOException e) {

                System.out.println("客户端连接服务器失败，休息2秒钟，干点别的事情。。。");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e1) {

                }
            }
        }
    }
}
