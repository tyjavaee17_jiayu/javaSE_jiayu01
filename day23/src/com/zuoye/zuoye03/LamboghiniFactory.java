package com.zuoye.zuoye03;

public class LamboghiniFactory implements Factory{
    @Override
    public Car getInstance() {
        return new Lamboghini();
    }
}
