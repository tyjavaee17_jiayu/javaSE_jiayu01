package com.zuoye.zuoye03;

public class BenZFactory implements Factory {
    @Override
    public Car getInstance() {
        return new BenZ();
    }
}
