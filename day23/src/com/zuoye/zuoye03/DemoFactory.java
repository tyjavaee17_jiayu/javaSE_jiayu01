package com.zuoye.zuoye03;

public class DemoFactory {
    public static void main(String[] args) {
        BenZFactory benZFactory = new BenZFactory();
        Car benZInstance = benZFactory.getInstance();
        benZInstance.outLook();

        LamboghiniFactory lamboghiniFactory = new LamboghiniFactory();
        Car lamboghiniFactoryInstance = lamboghiniFactory.getInstance();
        lamboghiniFactoryInstance.outLook();

    }
}
