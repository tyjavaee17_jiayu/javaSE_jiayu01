package com.zuoye.zuoye03;

import com.zuoye.zuoye03.Car;

public interface Factory {
    public abstract Car getInstance();
}
