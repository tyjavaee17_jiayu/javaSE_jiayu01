package com.zuoye.zuoye02;

import java.util.ArrayList;
import java.util.Random;

public class Student {
    public Student() {
    }
    private static final int MAX = 6;
    private static ArrayList<Student> list = new ArrayList<>();
    static {
        for (int i = 0; i < MAX; i++) {
            list.add(new Student());
        }
    }
    public static Student getInstance(){
        int index = new Random().nextInt(list.size());
        return list.get(index);
    }
}
