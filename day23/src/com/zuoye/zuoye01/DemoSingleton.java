package com.zuoye.zuoye01;

public class DemoSingleton {
    public static void main(String[] args) {
        Student student = Student.getInstance();
        System.out.println(student);

        Student student1 = Student01.getInstance();
        System.out.println(student1);
    }
}
