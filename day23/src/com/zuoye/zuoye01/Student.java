package com.zuoye.zuoye01;
//懒汉式
public class Student {
    public Student() {
    }
    private static Student stu= new Student();
    public static Student getInstance(){
        if (stu == null){
            return new Student();
        }
        return stu;
    }
}
