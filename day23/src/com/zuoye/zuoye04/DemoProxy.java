package com.zuoye.zuoye04;
import  java.lang.reflect.Proxy;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class DemoProxy {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list,"迪丽热巴","迪丽热巴","赵丽颖","迪丽热巴","陈乔恩","赵丽颖","哈尼克孜");

        Collection<String> listProxy = (Collection<String>)Proxy.newProxyInstance(list.getClass().getClassLoader(), list.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String  methodName = method.getName();
                if (methodName.equals("remove")){
                    boolean flag = false;
                    for (int i = list.size()-1; i >= 0; i--) {
                        if (list.get(i).equals(args[0])){
                            list.remove(i);
                            flag = true;
                        }
                    }
                    return flag;
                }else{
                    Object invoke = method.invoke(list,args);
                    return invoke;
                }
            }
        });
        boolean flag = listProxy.remove("迪丽热巴");
        System.out.println("是否删除成功？"+ flag );
        System.out.println(list);
    }

}
