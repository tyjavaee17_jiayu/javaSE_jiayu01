package com.ketang.demo05Multiton;

public class DemoMultiton {
    public static void main(String[] args) {
        Person p = new Person();
        p.setName("迪丽热巴");
        p.setAge(18);
        p.setSex("女");
        System.out.println(p);
    }
}
