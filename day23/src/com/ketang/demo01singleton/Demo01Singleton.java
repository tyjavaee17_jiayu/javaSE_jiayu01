package com.ketang.demo01singleton;

public class Demo01Singleton {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Person person = Person.getInstance();
            System.out.println(person);
        }
    }
}
