package com.ketang.demo01singleton;

public class Person {
    private Person(){

    }
    private static Person p = new Person();
    public static Person getInstance(){
        return p;
    }
}
