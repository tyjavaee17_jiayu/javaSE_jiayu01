package com.ketang.demo09FactoryMethod;

public abstract class Animal {
    public abstract void eat();
}
