package com.ketang.demo09FactoryMethod;

public class DemoFactoryMethod {
    public static void main(String[] args) {
        CatFactory catFactory = new CatFactory();
        Animal catFactoryInstance = catFactory.getInstance();
        catFactoryInstance.eat();
        DogFactory dogFactory = new DogFactory();
        Animal dogFactoryInstance = dogFactory.getInstance();
        dogFactoryInstance.eat();
    }
}
