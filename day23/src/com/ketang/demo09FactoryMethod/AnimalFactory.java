package com.ketang.demo09FactoryMethod;

public class AnimalFactory {
    public static Animal getInstance(String name){
        if ("cat".equals(name)){
            return new Cat();
        }else if ("dog".equals(name)){
            return new Dog();
        }else{
            return null;
        }
    }
}
