package com.ketang.demo09FactoryMethod;

public class CatFactory implements Factory{
    @Override
    public Animal getInstance() {
        return new Cat();
    }
}
