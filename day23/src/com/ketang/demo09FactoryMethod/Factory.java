package com.ketang.demo09FactoryMethod;

public interface Factory {
    public abstract Animal getInstance();
}
