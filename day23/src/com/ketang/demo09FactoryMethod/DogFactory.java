package com.ketang.demo09FactoryMethod;

public class DogFactory implements Factory{
    @Override
    public Animal getInstance() {
        return new Dog();
    }
}
