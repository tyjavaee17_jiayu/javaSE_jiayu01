package com.ketang.demo10proxy;

import java.lang.reflect.Proxy;

public class DemoProxy {
    public static void main(String[] args) {
        CaiXuKun caiXuKun = new CaiXuKun();

        Star caiXuKunProxy = (Star)Proxy.newProxyInstance(caiXuKun.getClass().getClassLoader(), caiXuKun.getClass().getInterfaces(), new InvocationHanderImpl(caiXuKun));

        caiXuKunProxy.changge();
        caiXuKunProxy.tiaowu();
        caiXuKunProxy.yangdianying(100);
        caiXuKunProxy.wanyouxi();
        caiXuKunProxy.chifan();

        WuYiFan wuYiFan = new WuYiFan();
        Star WuYiFanProxy = (Star)Proxy.newProxyInstance(wuYiFan.getClass().getClassLoader(), wuYiFan.getClass().getInterfaces(), new InvocationHanderImpl(wuYiFan));

        WuYiFanProxy.changge();
        WuYiFanProxy.tiaowu();
        WuYiFanProxy.yangdianying(100);
        WuYiFanProxy.wanyouxi();
        WuYiFanProxy.chifan();
    }
}
