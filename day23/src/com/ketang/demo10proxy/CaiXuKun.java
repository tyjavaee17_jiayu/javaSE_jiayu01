package com.ketang.demo10proxy;

public class CaiXuKun implements Star{
    @Override
    public void changge() {
        System.out.println("蔡徐坤在唱歌！");
    }

    @Override
    public void tiaowu() {
        System.out.println("蔡徐坤在跳舞！");
    }

    @Override
    public String yangdianying(int money) {
        System.out.println("蔡徐坤在演电影！" + money);
        return "电影演完了！";
    }

    @Override
    public void chifan() {
        System.out.println("和蔡徐坤一起吃饭！");
    }

    @Override
    public void wanyouxi() {
        System.out.println("和蔡徐坤在一起玩游戏！");
    }
}
