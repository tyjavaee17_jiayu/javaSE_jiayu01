package com.ketang.demo10proxy;

public interface Star {
    public abstract void changge();
    public abstract void tiaowu();
    public abstract String yangdianying(int money);
    public abstract void chifan();
    public abstract void wanyouxi();
}
