package com.ketang.demo10proxy;

public class WuYiFan implements Star{
    @Override
    public void changge() {
        System.out.println("吴亦凡在唱歌！");
    }

    @Override
    public void tiaowu() {
        System.out.println("吴亦凡在跳舞！");
    }

    @Override
    public String yangdianying(int money) {
        System.out.println("吴亦凡在演电影！" + money);
        return "电影演完了！";
    }

    @Override
    public void chifan() {
        System.out.println("和吴亦凡一起吃饭！");
    }

    @Override
    public void wanyouxi() {
        System.out.println("和吴亦凡在一起玩游戏！");
    }
}
