package com.ketang.demo10proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class InvocationHanderImpl implements InvocationHandler {
        private Star star;

    public InvocationHanderImpl(Star star) {
        this.star = star;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        if ("chifan".equals(methodName)){
            throw new RuntimeException("不能和你吃饭！");
        }
        if ("wanyouxi".equals(methodName)){
            throw new RuntimeException("不能和你玩游戏!");
        }
        Object invoke = method.invoke(star, args);
        return invoke;
    }
}
