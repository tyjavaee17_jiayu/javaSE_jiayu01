package com.ketang.demo11Proxy;

import com.sun.corba.se.spi.orbutil.proxy.CompositeInvocationHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

public class Demo02Proxy {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");

        List<String> listProxy = listProxy(list);
        System.out.println(listProxy.size());
        System.out.println(listProxy.set(4, "t"));
        System.out.println(listProxy.remove(2));

    }
    public static List<String> listProxy(List<String> list){
        List<String> listProxy = (List<String>)Proxy.newProxyInstance(list.getClass().getClassLoader(), list.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String methodNameame = method.getName();
                if ("add".equals(methodNameame)) {
                    throw new UnsupportedOperationException("add no run");
                }
                if ("remove".equals(methodNameame)) {
                    throw new UnsupportedOperationException("remove no run");
                }
                if ("get".equals(methodNameame)) {
                    throw new UnsupportedOperationException("get no run");
                }
                Object invoke = method.invoke(list, args);
                return invoke;
            }
        });
        return listProxy;
    }
}
