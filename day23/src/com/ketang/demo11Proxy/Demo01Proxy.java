package com.ketang.demo11Proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Demo01Proxy {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");

        List<String> listProxy = Collections.unmodifiableList(list);

        System.out.println(listProxy.get(0));
        System.out.println(listProxy.size());
    }
}
