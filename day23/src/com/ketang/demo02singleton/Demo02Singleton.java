package com.ketang.demo02singleton;

public class Demo02Singleton {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Person person = Person.getInstance();
            System.out.println(person);
        }
    }
}
