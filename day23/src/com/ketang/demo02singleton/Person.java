package com.ketang.demo02singleton;

public class Person {
    public Person() {
    }
    private static Person p;

    public static Person getInstance(){
        if (p == null){
            p =new Person();
        }
        return p;
    }
}
