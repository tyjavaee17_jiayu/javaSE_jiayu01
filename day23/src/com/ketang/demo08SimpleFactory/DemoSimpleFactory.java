package com.ketang.demo08SimpleFactory;

public class DemoSimpleFactory {
    public static void main(String[] args) {
        Animal cat = AnimalFactory.getInstance("cat");
        cat.eat();

        Animal dog = AnimalFactory.getInstance("dog");
        dog.eat();

        Animal aaa = AnimalFactory.getInstance("aaa");
        aaa.eat();
    }
}
