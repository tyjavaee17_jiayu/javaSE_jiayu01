package com.ketang.demo03singleton;

public class Person {
    private Person(){

    }
    private static Person p;
    public static synchronized Person getInstance(){
        if (p == null){
            p = new Person();
        }
        return p;
    }
}
