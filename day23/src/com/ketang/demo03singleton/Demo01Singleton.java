package com.ketang.demo03singleton;

public class Demo01Singleton {
    public static void main(String[] args) {
        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                Person person = Person.getInstance();
                System.out.println(person);
            }
        }).start();

        new Thread(()->{
            for (int i = 0; i < 10; i++) {
                Person person = Person.getInstance();
                System.out.println(person);
            }
        }).start();
    }
}
