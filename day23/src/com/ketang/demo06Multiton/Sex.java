package com.ketang.demo06Multiton;

public class Sex {
    private String str;

    //1.私有构造方法,不让用户直接创建对象
    public Sex(String str) {
        this.str = str;
    }
    public static final Sex MAN = new Sex("男");
    public static final Sex WOMAN = new Sex("女");

    @Override
    public String toString() {
        return str;
    }
}
