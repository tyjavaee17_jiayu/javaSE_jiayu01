package com.ketang.demo07Enum;

public enum Sex {
    MAN("男"),WOMAN("女");

    private String str;

    //1.私有构造方法,不让用户直接创建对象
    private Sex(String str) {
        this.str = str;
        }

    @Override
    public String toString() {
        return str;
    }
}
