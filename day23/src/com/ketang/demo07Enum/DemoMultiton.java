package com.ketang.demo07Enum;

public class DemoMultiton {
    public static void main(String[] args) {
        Person p = new Person();
        p.setName("迪丽热巴");
        p.setAge(18);
        p.setSex(Sex.WOMAN);
        System.out.println(p);
        Person person = new Person("古力娜扎", 18, Sex.WOMAN);
        System.out.println(person);
    }
}
