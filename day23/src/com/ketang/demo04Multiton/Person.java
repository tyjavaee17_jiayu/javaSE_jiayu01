package com.ketang.demo04Multiton;

import java.util.ArrayList;
import java.util.Random;

public class Person {
    public Person() {
    }
   private static final int MAX = 3;
    private static ArrayList<Person> list = new ArrayList<>();

    static {
        for (int i = 0; i < MAX; i++) {
            list.add(new Person());
        }
    }
    public static Person getInstance(){
        int index = new Random().nextInt(list.size());
        return list.get(index);
    }
}
