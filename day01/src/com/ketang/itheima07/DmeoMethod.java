package day01.com.ketang.itheima07;
/*
    继承后的特点_成员方法
        1.子类有使用子类自己的成员方法
        2.子类没有使用继承自父类的成员方法             //就近原则
        3.子类和父类都没有,编译报错
 */
public class DmeoMethod {
    public static void main(String[] args) {
        Zi1 zi1 = new Zi1();
        zi1.show01();
        zi1.show02();
        System.out.println("------------");

        Zi2 zi2= new Zi2();
        zi2.show01();
        zi2.show02();
    }
}
