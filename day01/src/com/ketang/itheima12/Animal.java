package day01.com.ketang.itheima12;

public class Animal {
    private String name;
    private int age;

    public Animal() {
        this("tom",10);//调用满参数构造方法
        System.out.println("Animal类中的空参数构造方法！");
    }

    public Animal(String name, int age) {
        //this();//调用空参数构造方法
        this.name = name;
        this.age = age;
        System.out.println("Animal类中的满参数构造方法！");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
