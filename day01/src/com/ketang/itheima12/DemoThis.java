package day01.com.ketang.itheima12;
/*
    this关键字:可以访问本类的其他构造方法
    格式:
        this();访问空参数构造方法
        this(参数);访问是带参数构造方法
    注意:
        1.this()|this(参数);访问构造方法必须写在构造方法的第一行,创建对象必须优先执行
        2.构造方法不能相互调用(不能你调用我,我在调用你-->死循环)
 */
public class DemoThis {
    public static void main(String[] args) {
        Animal a1 = new Animal();
        System.out.println(a1.getName()+"\t"+ a1.getAge());

        Animal a2 = new Animal("Jerry",5);
        System.out.println(a2.getName() + "\t" + a2.getAge());
    }
}
