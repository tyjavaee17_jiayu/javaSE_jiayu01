package day01.com.ketang.itheima10;
/*
       继承后的特点_构造方法
       Fu类的空参数构造方法!10
       Zi类的空参数构造方法!10
       在子类的构造方法第一行,有一个默认的super();
       super();作用就是调用父类的空参数构造方法
       子类继承父类,子类想要使用继承自父类的成员,就必须把父类加载到内存中,这样子类才可以使用
    */
public class Zi extends Fu{
    int a = 1;
    int b = 2;

    public Zi() {
        //super();  系统会有默认的super（）构造方法
        System.out.println("Zi类的空参构造方法！" + a);
    }
}
