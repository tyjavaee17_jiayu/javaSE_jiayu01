package day01.com.ketang.itheima03;
//  一般可以作为方法的参数和返回值使用
public class DemoAnimal {
    public static void main(String[] args) {
        Animal a1 = new Animal("Tom");
        a1.eat();
        a1.eat();//重复使用

        new Animal("Jerry").eat();
        new Animal("熊大").eat();
        System.out.println("-------------");
        show(a1);
        show(new Animal("熊二"));
        System.out.println("-------------");
        Animal a4 = getAnimal();
        a4.eat();
    }
    public static void show(Animal a){
        a.eat();
    }
    public static Animal getAnimal(){
        return new Animal("光头强");
    }
}
