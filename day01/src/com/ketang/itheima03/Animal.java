package day01.com.ketang.itheima03;

public class Animal {
    private String name;

    public void eat(){
        System.out.println(name + "在吃饭！");

    }

    public Animal() {
    }

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
