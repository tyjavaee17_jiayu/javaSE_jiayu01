package day01.com.ketang.itheima09;

/*
    方法重写的注意事项:
        1.子类重写父类的方法,保修要保证权限大于等于父类
            java中四大权限修饰符
            public:公共的
            protected:受保护的
            :默认的,不写就是默认的
            private:私有的
       2.子类重写父类的方法,返回值类型,方法名,参数列表必须一抹一样
       3.私有方法不能被重写(父类私有成员子类是不能继承的)

       权限修饰符  //public > protected > 默认 > private
 */
public class Zi extends Fu{
    protected void show02(){
        System.out.println("Zi类的重写Fu的show02方法！");
    }
    public void show04(){
        System.out.println("Zi类重写Fu的show04方法！");
    }
    private void show05(){
        System.out.println("Zi的show05方法");
    }
}
