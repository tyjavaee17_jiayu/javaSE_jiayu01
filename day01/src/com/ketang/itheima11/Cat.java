package day01.com.ketang.itheima11;
/*
    this关键字:本类对象的引用
        this.成员变量:本类的成员变量
        this.成员方法:本类的成员方法
    super关键字:父类对象的引用
        super.成员变量:父类的成员变量
        super.成员方法:父类的成员方法
 */
public class Cat extends Animal{
    String name = "加菲猫";
    @Override
    public void eat(){
        System.out.println("加菲猫在吃饭！");
    }
    public void show(){
        System.out.println(name);
        System.out.println("this.name:" + this.name);
        System.out.println("super.name:" + super.name);
        eat();
        this.eat();
        super.eat();
    }

}
