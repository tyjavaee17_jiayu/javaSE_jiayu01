package day01.com.ketang.itheima13;
/*
    super关键字:调用父类的构造方法
    格式:
        super();调用父类的空参数构造方法
        super(参数);调用父类的带参数构造方法
    注意:
        1.在子类的构造方法中没有写super();有一个默认的super(),用于调用父类的空参数构造方法
        2.super()|super(参数);必须写在子类构造方法有效代码的第一行,构造方法必须优先执行
        3.在子类的构造方法中this和super不能同时使用,都必须写在有效代码的第一行
 */
public class Zi extends Fu{
    public Zi() {
        this(10);
        System.out.println("Zi类的空参数构造方法！");
    }

    public Zi(int a) {
        super(a);
    }
}
