package day01.com.ketang.itheima15;

public class DemoDriver {
    public static void main(String[] args) {
        NewDriver nd = new NewDriver();
        nd.ziShi();
        System.out.println("---------------");
        nd.driver();

        System.out.println("---------------");
        OldDriver od = new OldDriver();
        od.ziShi();
        System.out.println("---------------");
        od.driver();
    }

}
