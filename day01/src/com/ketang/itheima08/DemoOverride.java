package day01.com.ketang.itheima08;
/*
    方法重写:发生在两个类之间,在子类中出现了和父类一模一样的方法,叫方法重写(Override)
        一模一样:
            方法名一样
            参数列表一样
            返回值类型一样
            修饰符一样(子类的修饰符权限大于等于父类的修饰符)
        注解:
            @Override:检查方法是否为重写的方法
    ---------------------------------------------------------
    方法重载:发生在一个类中,在一个类中出现了方法名相同,但是参数列表不同的方法,叫方法重载(Overload)
        参数列表不同:个数,顺序,数据类型
 */
public class DemoOverride {
    public static void main(String[] args) {
        Zi zi = new Zi();
        zi.show01();
        zi.show01(10);

        Fu f = new Fu();
        f.show01();
    }
}
