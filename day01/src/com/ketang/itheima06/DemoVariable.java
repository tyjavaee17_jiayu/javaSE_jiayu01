package day01.com.ketang.itheima06;

public class DemoVariable {
    public static void main(String[] args) {
        Zi1 zi1 = new Zi1();
        System.out.println(zi1.a);
        System.out.println(zi1.b);

        System.out.println("-------------------");

        Zi2 zi2 = new Zi2();
        System.out.println(zi2.a);
        System.out.println(zi2.b);
        //System.out.println(zi2.f);//Cannot resolve symbol 'f'  子类和父类都没有,编译报错
    }
}
