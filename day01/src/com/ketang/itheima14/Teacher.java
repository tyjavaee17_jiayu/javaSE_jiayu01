package day01.com.ketang.itheima14;

public class Teacher extends Employee{
    public Teacher() {
        super();
    }

    public Teacher(String name, int age) {
        super(name, age);
    }
    @Override
    public void work(){
        System.out.println("正在给大家讲解java");
    }
}
