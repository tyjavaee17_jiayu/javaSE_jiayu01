package day01.com.ketang.itheima14;
/*
    定义父类员工类:是根据子类共性抽取形成
    定义成员变量:
        姓名,年龄
    定义成员方法:
        工作的方法
        每个子类工作方法的方法体是不同的,只抽取方法的声明,添加一个abstract关键字修饰,叫抽象方法
        定义格式:
            修饰符 abstract 返回值类型 方法名(参数);
            puablic abstract void work();
            puablic abstract void work(String a);
            puablic abstract int work(String a);
       包含抽象方法的类,必须被abstract修饰,叫抽象类
       定义格式:
            public abstract class 类名{ }
    ----------------------------------------------------------------
    注意:
        1.抽象类是无法直接创建对象使用的
            a.有一些类就是为了不让别人创建对象使用,可以定义为抽象类
            b.抽象类中一般都包含抽象方法,抽象方法没有方法体,创建对调用抽象方法没有意义
        2.需要创建子类继承抽象类,重写抽象类中的抽象方法,创建子类对象使用
    ---------------------------------------------------------------------------
    好处:
        在抽象类中定义类抽象方法,那么子类就必须重写这个抽象方法
        公司中所有的员工都必须工作
 */
public abstract class Employee {
    private String name;
    private int age;

    public Employee() {
    }

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public abstract void work();     //定义抽象的工作方法

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
