package day01.com.ketang.itheima14;

public class DemoAbstractClass {
    public static void main(String[] args) {
        Teacher t = new Teacher();
        t.work();

        BanZhuRen bzr = new BanZhuRen();
        bzr.work();

        //JiuYeZhiDao JYZD = new JiuYeZhiDao() {}    //抽象类是无法创建对象使用的
    }
}
