package day01.com.ketang.itheima02;

public class Person {
    private String name;
    public void show(){
        System.out.println("this:" + this);
        String name = "小强";
        System.out.println("name:" + name);
        System.out.println("this.name:" + this.name);
    }

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
