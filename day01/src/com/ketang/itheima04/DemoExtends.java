package day01.com.ketang.itheima04;

public class DemoExtends {
    public static void main(String[] args) {
        Teacher t = new Teacher();
        t.name = "老白";
        t.age = 40;
        t.work();
        t.print();

        BanZhuRen bzr = new BanZhuRen();
        bzr.name = "美女";
        bzr.age = 18;
        bzr.work();
    }
}
