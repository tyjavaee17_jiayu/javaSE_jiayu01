package day01.com.ketang.itheima05;

public class Fu {
    private String name;
    private int age;

    public Fu() {
    }

    public Fu(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private void sifangqian(){
        System.out.println("父亲老王的私房钱！");

    }
}
