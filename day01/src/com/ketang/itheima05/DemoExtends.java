package day01.com.ketang.itheima05;
/*
    继承的注意事项:
        1.构造方法是不能继承的,构造方法就是创建本类对象使用的
        2.父类私有成员子类是不能继承的
 */
public class DemoExtends {
    public static void main(String[] args) {
        Fu f = new Fu("老王",40);
        System.out.println(f.getName());
        System.out.println(f.getAge());

        Zi z = new Zi();
        z.setName("小白");
        z.setAge(18);
        System.out.println(z.getName());
        System.out.println(z.getAge());
    }
}
