package day01.com.ketang.itheima01;

public class Student {
    private  String name;
    private  int age;

    public Student() {
        System.out.println("Student类的空参构造方法！");
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("Student类的满参构造方法！");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
