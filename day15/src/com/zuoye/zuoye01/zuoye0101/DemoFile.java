package com.zuoye.zuoye01.zuoye0101;

import java.io.File;

public class DemoFile {
    public static void main(String[] args) {
        File file1 = new File("D:\\test1.txt");
        File file2 = new File("D:\\测试目录");

        System.out.println(file1.getAbsolutePath());
        System.out.println(file2.getAbsolutePath());

        System.out.println("---------------");

        System.out.println(file1.getName());
        System.out.println(file2.getName());

        System.out.println("---------------");

        System.out.println(file1.length());
        System.out.println(file2.length());

        System.out.println("---------------");

        System.out.println(file1.exists());
        System.out.println(file2.exists());

        System.out.println("---------------");
        System.out.println(file1.isDirectory());
        System.out.println(file1.isFile());

        System.out.println(file2.isDirectory());
        System.out.println(file2.isFile());

    }
}
