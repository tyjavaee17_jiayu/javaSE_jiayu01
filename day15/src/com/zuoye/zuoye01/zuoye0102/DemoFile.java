package com.zuoye.zuoye01.zuoye0102;

import java.io.File;
import java.io.IOException;

public class DemoFile {
    public static void main(String[] args) throws IOException {
        File file1 = new File("test.txt");
        File file2 = new File("一级目录");
        File file3 = new File("目录A\\目录B\\目录C");
        if (!file1.exists()){
            boolean b1 = file1.createNewFile();
            System.out.println("b1=" + b1);
        }
        if (!file2.exists()){
            boolean b2 = file2.mkdir();
            System.out.println("b1=" + b2);
        }
        if (!file3.exists()){
            boolean b3 = file3.mkdirs();
            System.out.println("b1=" + b3);
        }
    }
}
