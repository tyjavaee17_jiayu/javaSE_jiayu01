package com.zuoye.zuoye01.zuoye0103;

import java.io.File;

public class Demo03File {
    public static void main(String[] args) {
        File file = new File("D:\\多级目录");
        File[] files = file.listFiles();

        for (File file1 : files) {
            if (file1.isFile()){
                System.out.println("【文件】+ 绝对路径："+file1.getName()+file1.getAbsolutePath());
            }else if (file1.isDirectory()){
                System.out.println("【目录】+ 绝对路径："+file1.getName()+ file1.getAbsolutePath());
            }
        }
    }
}
