package com.zuoye.zuoye02.zuoye0202;

public class DemoLeiJia {
    public static void main(String[] args) {
        int leiJia = LeiJia(100);
        System.out.println(leiJia);
    }

    private static int LeiJia(int n) {
        if (n ==1){
            return 1;
        }
        return n + LeiJia(n-1);
    }
}
