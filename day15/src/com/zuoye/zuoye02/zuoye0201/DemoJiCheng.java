package com.zuoye.zuoye02.zuoye0201;

public class DemoJiCheng {
    public static void main(String[] args) {
        int jicheng = jicheng(5);
        System.out.println(jicheng);
    }

    private static int jicheng(int n) {
        if (n == 1){
            return 1;
        }
        return n*jicheng(n-1);
    }
}
