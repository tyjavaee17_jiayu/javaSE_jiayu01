package com.ketang.demo02digui;

public class Demo02DiGui {
    public static void main(String[] args) {
        int sum = sum(10);
        System.out.println(sum);
    }

    private static int sum(int a) {
        if (a ==1){
            return 1;
        }
        return a +sum(a-1);
    }
}
