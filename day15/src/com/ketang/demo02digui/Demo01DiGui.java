package com.ketang.demo02digui;

public class Demo01DiGui {
    public static void main(String[] args) {
        b(1);
    }

    private static void b(int a ) {
        System.out.println(a);
        if (a == 1000){
            return;
        }
        b(++a);
    }
}
