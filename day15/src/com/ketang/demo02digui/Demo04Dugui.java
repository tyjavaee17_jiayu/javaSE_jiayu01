package com.ketang.demo02digui;

import java.io.File;

public class Demo04Dugui {
    public static void main(String[] args) {
        File file = new File("E:\\IdeaProject\\JavaEE\\day15");
        getAllFile(file);
    }

    private static void getAllFile(File dir) {
        System.out.println(dir);
        File[] files = dir.listFiles();
        for (File f : files) {
            if (f.isDirectory()){
                getAllFile(f);
            }else {
                System.out.println(f);
            }
        }
    }

}
