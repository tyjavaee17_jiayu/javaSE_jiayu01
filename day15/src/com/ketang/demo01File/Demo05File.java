package com.ketang.demo01File;

import java.io.File;
import java.io.IOException;


/*
    File类的成员方法_创建删除功能的方法(重点)
        public boolean createNewFile() ：当且仅当具有该名称的文件尚不存在时，创建一个新的空文件。
        public boolean mkdir() ：创建由此File表示的目录。
        public boolean mkdirs() ：创建由此File表示的目录，包括任何必需但不存在的父目录。
        public boolean delete() ：删除由此File表示的文件或目录。
 */
public class Demo05File {
    public static void main(String[] args) throws IOException {
        show01();
        show02();
        show03();
    }

    private static void show03() {
        File f1 = new File("day15\\aa");
        System.out.println(f1.delete());

        File f2 = new File("day15\\a.txt");
        System.out.println(f2.delete());

        File f3 = new File("day15\\aaa");
        System.out.println(f3.delete());
    }

    /*
        public boolean mkdir() 只能创建单级文件夹
        public boolean mkdirs() 既能创建单级文件夹,又能创建多级文件夹
        返回值:boolean
            文件夹不存在,创建成功,返回true
            文件夹存在,不会创建,返回false;构造方法中传递的路径不存在,返回false
        注意:
            1.创建文件夹的名称和路径在构造方法中给出
            2.此方法只能创建文件夹,不能创建文件
     */
    private static void show02()  {
        File f1 = new File("day15\\aa");
        boolean b1 = f1.mkdir();
        System.out.println("b1 = " + b1);  //b1:true  第二次执行b1:false,文件夹存在,不会创建,返回false

        File f2 = new File("day15\\aaa\\bbb\\ccc\\ddd\\eee");
        boolean b2 = f2.mkdirs();
        System.out.println("b2 = " + b2);   //多级文件夹

        File f3 = new File("T:\\aaa");  //false  路径不存在
        System.out.println(f3.mkdir());

        File f4 = new File("day15\\a.txt");
        System.out.println(f4.mkdir());
    }
    /*
        boolean createNewFile()  当且仅当不存在具有此抽象路径名指定名称的文件时，不可分地创建一个新的空文件。
        作用:用于创建一个新的空白文件
        返回值:boolean
            true:文件不存在,创建成功,返回true
            false:文件存在,不会创建新的,返回false(不会覆盖)
        注意:
            1.此方法只能创建文件,不能创建文件夹
            2.创建文件的名称和路径在构造方法中给出
            3.构造方法中写的路径必须是存在的,否则会抛出异常
            4.有些操作系统,c盘是没有权限创建文件的,会抛出拒绝访问异常
     */
    private static void show01() throws IOException{
        File f1 = new File("E:\\a.txt");
        boolean b1 = f1.createNewFile();
        System.out.println("b1 = " + b1);

        File f2 = new File("day15\\b.txt");
        boolean b2 = f2.createNewFile();
        System.out.println("b2 = " + b2);

       /* File f3 = new File("d:\\aaa\\a.txt");//d盘下没有aaa文件夹,不会创建文件夹
        boolean b3 = f3.createNewFile();
        System.out.println("b3 = " + b3);*/

        File f4 = new File("day15\\新建文件夹");
        boolean b4 = f4.createNewFile();
        System.out.println("b4 = " + b4);
    }
}
