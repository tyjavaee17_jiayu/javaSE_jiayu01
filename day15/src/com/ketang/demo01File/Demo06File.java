package com.ketang.demo01File;

import java.io.File;

/*
    File类的成员方法_目录遍历的方法(重点)
        public String[] list() ：返回一个String数组，表示该File目录中的所有子文件或目录。
        public File[] listFiles() ：返回一个File数组，表示该File目录中的所有的子文件或目录。
    注意:
        1.遍历目录的路径,在构造方法中给出
        2.遍历目录必须是存在的,否则会抛出空指针异常
        3.这两个方法只能遍历目录,不能遍历文件,如果要遍历文件,会抛出空指针异常
 */
public class Demo06File {
    public static void main(String[] args) {
        show01();
        show02();
    }

    private static void show02() {
        File file = new File("E:\\IdeaProject\\JavaEE\\day15");
        File[] files = file.listFiles();
        for (File f : files) {
            System.out.println(f.getName());
        }
    }

    private static void show01() {
        File file = new File("E:\\IdeaProject\\JavaEE\\day15");
        String[] arr = file.list();
        for (String fileName : arr) {
            System.out.println(fileName);
        }
    }
}
