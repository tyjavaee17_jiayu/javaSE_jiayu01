package com.ketang.demo01File;

import java.io.File;

public class Demo01File {
    public static void main(String[] args) {
        String pathSeparator = File.pathSeparator;
        System.out.println(pathSeparator);

        String separator = File.separator;
        System.out.println(separator);

        String path = "d:\\a.txt";
        System.out.println(path);

        path = "d:"+File.separator+"b.txt";
        System.out.println(path);
    }
}
