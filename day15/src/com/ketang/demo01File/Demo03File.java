package com.ketang.demo01File;

import java.io.File;

public class Demo03File {
    public static void main(String[] args) {
        //show01();
        //show02();
        //show03();
        show04();
    }
    /*
        public long length() 获取文件的大小,单位是字节
        注意:
            1.length方法只能获取文件的大小,不能获取文件夹的大小
            2.length方法获取的就是构造方法中传递路径表示的文件,如果文件不存在,那么此方法返回0
     */
    private static void show04() {
        File f1 = new File("E:\\develop\\TV\\License.txt");
        System.out.println(f1.length());

        File f2 = new File("c:\\avdsc\\a.txt");
        System.out.println(f2.length());    //0 字节 文件不存在

        File f3 = new File("E:\\360Downloads");
        System.out.println(f3.length());   //文件夹是不能获取大小的

        File f4 = new File("E:\\develop");
        System.out.println(f4.length());    //4096
    }
    /*
        public String getName() 获取构造方法中路径的末尾部分的文件|文件夹名称
     */
    private static void show03() {
        File f1 = new File("c:\\aaa\\bbb\\ccc\\ddd\\a.txt");
        System.out.println(f1.getName());

        File f2 = new File("c:\\aaa\\bbb\\ccc");
        System.out.println(f2.getName());

        File f3 = new File("day15\\aaa\\b.jpg");
        System.out.println(f3.getName());

    }
    /*
        public String getPath() 获取构造方法中传递的路径,传递的是绝对的,就返回绝对的;传递的是相对的,就返回相对的
        File类重写Object类的toString方法
        toString方法的底层源码
            public String toString() {
                return getPath();
            }
     */
    private static void show02() {
        File f1 = new File("c:\\aaa\\bbb\\ccc\\ddd\\eee\\a.txt");
        System.out.println(f1.getPath());

        File f2 = new File("day15");
        System.out.println(f2);
        System.out.println(f2.toString());
    }
    /*
        public String getAbsolutePath() : 获取构造方法中传递的路径的绝对路径(以盘符开始)
        File getAbsoluteFile() 返回此抽象路径名的绝对路径名形式。
        注意:
            1.无论构造方法中传递的路径是相对的还是绝对的,返回的都是绝对路径
            2.传递相对路径,会在路径的前边自动添加上项目的根目录(D:\Work_idea\EE132)
     */
    private static void show01() {
        File f1 = new File("c:\\aaa\\bbb\\ccc\\ddd\\a.txt");
        String absolutePath = f1.getAbsolutePath();
        System.out.println(absolutePath);

        File f2 = new File("E:\\IdeaProJect\\JavaEE\\day15");
        String absolutePath1 = f2.getAbsolutePath();
        System.out.println(absolutePath1);

        File f3 = new File("day15");
        System.out.println(f3.getAbsolutePath());

        //File a1 = f1.getAbsoluteFile();
        System.out.println(f1.exists());

        //File a2 = f3.getAbsoluteFile();
        System.out.println(f3.exists());
    }
}
