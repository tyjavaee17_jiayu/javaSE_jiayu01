package com.ketang.demo01File;

import java.io.File;

public class Demo02File {
    public static void main(String[] args) {
        show01();
        show02("d:\\","a.txt");
        show03();
    }

    private static void show03() {
        File parent = new File("d:\\");
        String child = "a.txt";
        File file = new File(parent, child);
        System.out.println(file);
    }

    private static void show02(String parent,String child) {
        File file = new File(parent, child);
        System.out.println(file);
    }

    private static void show01() {
        File f1 = new File("c:\\a.txt");
        System.out.println(f1);

        File f2 = new File("d:\\aaa\\bbb\\ccc\\ddd");
        System.out.println(f2);

        File f3 = new File("day15");
        System.out.println(f3);

    }
}
