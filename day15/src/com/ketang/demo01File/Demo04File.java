package com.ketang.demo01File;

import java.io.File;
/*
    File类的成员方法_判断功能的方法(重点)
        public boolean exists() ：此File表示的文件或目录是否实际存在。
        public boolean isDirectory() ：此File表示的是否为目录。
        public boolean isFile() ：此File表示的是否为文件。
 */
public class Demo04File {
    public static void main(String[] args) {
        //show01();
        show02();
    }
    /*
        public boolean isDirectory() 判断构造方法中传递的路径是否是以文件夹结尾的路径
            是文件夹结尾:true
            不是文件夹结尾:false
        public boolean isFile()判断构造方法中传递的路径是否是以文件结尾的路径
            是文件结尾:true
            不是文件结尾:false
        注意:
            1.构造方法中路径必须是存在的,才能判断是文件还是文件夹;否则没有意义,都会返回false
            2.计算机中只有文件和文件夹,所以两个方法互斥
     */
    private static void show02() {
        File f1 = new File("c:\\a.txt");//路径不存在
        if (f1.exists()){
            boolean b1 = f1.isFile();
            System.out.println("b1 = " + b1);
            boolean b2 = f1.isDirectory();
            System.out.println("b2 = " + b2);
        }
        System.out.println("------------------------");
        File f2 = new File("day15");
        if (f2.exists()){
            boolean b3 = f2.isDirectory();
            System.out.println("b3 = "+ b3);
            boolean b4 = f2.isFile();
            System.out.println("b4 = "+ b4);
        }
        System.out.println("-------------------------");
        File f3 = new File("day15\\a.txt");
        if (f3.exists()){
            boolean b5 = f3.isDirectory();
            System.out.println("b5 = " + b5);
            boolean b6 = f3.isFile();
            System.out.println("b6 = " + b6);
        }
    }
    /*
        public boolean exists() 判断构造方法中的路径是否存在
            存在:返回true
            不存在:返回false
     */
    private static void show01() {
        File f1 = new File("c:\\a.txt");
        System.out.println(f1.exists());

        File f2 = new File("c:\\asasa.txt");
        System.out.println(f2.exists());

        File f3 = new File("day15");
        System.out.println(f3.exists());

        File f4 = new File("day16");
        System.out.println(f4.exists());
    }
}
