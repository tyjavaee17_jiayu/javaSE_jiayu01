package com.ketang.demo03OutPutStream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo03OutPutStream {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("day15\\b.txt",true);
        for (int i = 1; i <=10 ; i++) {
            fileOutputStream.write(("你好" + i + "\r\n").getBytes());

        }
        fileOutputStream.close();
    }
}
