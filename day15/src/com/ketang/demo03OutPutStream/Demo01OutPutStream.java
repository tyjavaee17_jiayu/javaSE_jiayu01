package com.ketang.demo03OutPutStream;

import java.io.FileOutputStream;
import java.io.IOException;

public class Demo01OutPutStream {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("day15\\a.txt");
        fileOutputStream.write(97);
        fileOutputStream.close();
    }
}
