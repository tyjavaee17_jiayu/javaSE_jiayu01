package com.ketang.demo03OutPutStream;

import java.io.FileOutputStream;
import java.io.IOException;

public class Demo02OutPutStream {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("day15\\a.txt");

        byte[] bytes = {49,48,48};
        fileOutputStream.write(bytes);

        byte[] bytes1 = {65,66,67,68,69,70};
        fileOutputStream.write(bytes1);

        fileOutputStream.write(bytes1,2,4);

        byte[] bytes2 = "中国".getBytes();
        fileOutputStream.write(bytes2);

        fileOutputStream.close();
    }
}
