package com.itheima.GenericClass;
/*
    定义和使用含有泛型的类:模拟ArrayList集合
        当我们不知道使用什么类型的时候,我们就可以使用泛型,是一个未知的数据类型
        泛型可以赋值任意的数据类型:Integer,String,Student....
    定义格式:
        public class 类名<泛型>{
            类中使用数据类型的地方,都可以使用泛型
        }
    什么时候确定泛型的数据类型
        创建对象的时候确定泛型的数据类型,对象使用什么类型,类的泛型就是什么类型
 */
public class GenericClass<c> {
    private c name;

    public c getName() {
        return name;
    }

    public void setName(c name) {
        this.name = name;
    }
}
