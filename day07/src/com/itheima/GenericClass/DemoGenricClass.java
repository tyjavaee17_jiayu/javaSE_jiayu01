package com.itheima.GenericClass;

public class DemoGenricClass {
    public static void main(String[] args) {
        GenericClass<String> gc = new GenericClass<>();
        gc.setName("小红");
        String name = gc.getName();
        System.out.println(name);
        System.out.println("-------------");
        GenericClass<Integer> gc2 = new GenericClass<>();
        gc2.setName(10);
        Integer in = gc2.getName();
        System.out.println(in);
    }
}
