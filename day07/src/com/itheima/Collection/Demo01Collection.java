package com.itheima.Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Demo01Collection {
    public static void main(String[] args) {
        Collection<String> coll = new ArrayList<>();
        coll.add("张三");
        coll.add("李四");
        coll.add("王五");
        coll.add("赵六");
        coll.add("田七");
        coll.add("张三");
        System.out.println(coll);

        System.out.println("-----------------");
        boolean s = coll.remove("张三");
        System.out.println(s);
        System.out.println(coll);

        System.out.println("-----------------");
        boolean s1 = coll.contains("李四");
        System.out.println(s1);
        boolean s3 = coll.contains("哈哈");
        System.out.println(s3);

        System.out.println("-----------------");
        boolean s4 = coll.isEmpty();
        System.out.println(s4);

        System.out.println("-----------------");
        int i = coll.size();
        System.out.println(i);
        Object[] objects = coll.toArray();
        System.out.println(Arrays.toString(objects));

        System.out.println("-----------------");
        coll.clear();
        System.out.println(coll);
        boolean s5 = coll.isEmpty();
        System.out.println(s5);
    }
}
