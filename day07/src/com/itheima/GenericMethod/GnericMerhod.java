package com.itheima.GenericMethod;
/*
    定义和使用含有泛型的方法(重点)
        泛型需要定义在方法修饰符和返回值类型之间
    定义格式:
        修饰符 <泛型> 返回值类型 方法名(参数-->使用泛型){
            方法体;
        }
    什么时候确定泛型的数据类型:
        调用方法,传递的参数是什么类型,方法的泛型就是什么类型
 */
public class GnericMerhod {
    //定义含有泛型的方法(重点)
    public <C> void show01(C c){
        System.out.println(c);
    }
    public static<D> void show02(D d){
        System.out.println(d);
    }
    public <AAA> AAA show03(AAA aaa){
        //System.out.println(aaa);
        return aaa;
    }
}
