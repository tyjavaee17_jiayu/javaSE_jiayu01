package com.itheima.GenericMethod;

public class DemoGenericMethod {
    public static void main(String[] args) {
        GnericMerhod gm = new GnericMerhod();
        gm.show01(0);
        gm.show01("abd");
        gm.show01(true);
        gm.show01('A');
        gm.show01(10.0);
        System.out.println("------------");
        GnericMerhod.show02(0);
        GnericMerhod.show02("abf");
        GnericMerhod.show02(true);
        GnericMerhod.show02(1.2);
        GnericMerhod.show02('B');
        System.out.println("------------");
        Integer in = gm.show03(100);
        System.out.println(in);
        String s = gm.show03("abdd");
        System.out.println(s);
    }
}
