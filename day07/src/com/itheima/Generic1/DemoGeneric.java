package com.itheima.Generic1;

import java.util.ArrayList;
import java.util.Iterator;
//泛型的通配符只能作为方法的参数的类型使用,不能创建对象作为类型使用
/*
    泛型的通配符
        ?:代表可以接收任意的数据类型
 */
public class DemoGeneric {
    public static void main(String[] args) {
        ArrayList<Integer> list01 = new ArrayList<>();
        list01.add(10);
        list01.add(20);
        ArrayList<String> list02 = new ArrayList<>();
        list02.add("abd");
        list02.add("yui");
        printArrayList(list01);
        printArrayList(list02);
    }

    private static void printArrayList(ArrayList<?> list) {
        Iterator<?> it = list.iterator();
        while(it.hasNext()){
            Object obj = it.next();
            System.out.println(obj);
        }
    }
}
