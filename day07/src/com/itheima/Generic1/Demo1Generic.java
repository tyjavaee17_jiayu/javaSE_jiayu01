package com.itheima.Generic1;

import java.util.ArrayList;
import java.util.Collection;

/*
    泛型通配符高级使用
    泛型的通配符: ?  可以接收任意的数据类型
    泛型的上限限定: ? extends E ==>传递的未知类型?只能是E的子类或者本身
    泛型的下限限定: ? super E   ==>传递的位置类型?只能是E的父类或者本身
 */
public class Demo1Generic {
    public static void main(String[] args) {
        Collection<Integer> list1 = new ArrayList<Integer>();
        Collection<String> list2 = new ArrayList<String>();
        Collection<Number> list3 = new ArrayList<Number>();
        Collection<Object> list4 = new ArrayList<Object>();

        getElement1(list1);
        //getElement1(list2);   编译时就报错   String和Number没有关系
        getElement1(list3);
        //getElement1(list4);   编译时就报错   Object是Number的父类,需要Number的子类

        //getElement2(list1);   Integer是Number的子类,需要Number的父类
        //getElement2(list2);    String和Number没有关系
        getElement2(list3);
        getElement2(list4);
    }
    // 泛型的上限：此时的泛型?，必须是Number类型或者Number类型的子类
    public static void getElement1(Collection<?extends Number> coll ){}
    // 泛型的下限：此时的泛型?，必须是Number类型或者Number类型的父类
    public static void getElement2(Collection<?super Number> coll ){}
}
