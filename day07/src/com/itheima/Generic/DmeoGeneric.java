package com.itheima.Generic;

import java.util.ArrayList;
import java.util.Iterator;

public class DmeoGeneric {
    public static void main(String[] args) {
        show01();
        show02();
    }
    /*
        创建集合对象,使用泛型
        好处:
            1.避免了类型转换的麻烦,集合汇总存储的元素是什么类型,取出的元素就是什么类型
            2.可以把运行期异常(运行程序的时候会抛出的异常),
                提升到编译期(写代码的时候会把.java文件自动编译生成.class文件)
        弊端:
            泛型使用什么类型,就只能存储什么类型的数据了
     */
    private static void show02() {
        ArrayList<String>list = new ArrayList<>();
        list.add("abc");
        Iterator<String> it = list.iterator();
        while (it.hasNext()){
            String s = it.next();
            System.out.println(s+ "   " + s.length());
        }
    }
    //不使用泛型
     /*
        创建集合对象,不使用泛型
        好处:
            集合不使用泛型,创建对象,默认的数据类型就是Object,可以存储任意类型的数据
         弊端:
            1.无法使用元素特有的方法(类型是Object,多态的弊端)
            2.可以向下转型,容易引发类型转换异常

     */
    private static void show01() {
        ArrayList list = new ArrayList();
        list.add("abc");
        list.add(1);
        Iterator it = list.iterator();
        while(it.hasNext()){
            Object o = it.next();
            System.out.println(o);
           /* String s = (String)o;
            System.out.println(s.length());*/
        }
    }
}
