package com.itheima.Iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

public class Dmeo02Iterator {
    public static void main(String[] args) {
       ArrayList<String> list = new ArrayList<>();
        list.add("小明");
        list.add("小红");
        list.add("小兰");
        list.add("小黑");
        list.add("小稚");
        System.out.println(list.size());
        Iterator<String> it = list.iterator();
        while(it.hasNext()) {
            String s = it.next();
            System.out.println(s);
        }
        System.out.println("------------------");
        ListIterator<String> lit = list.listIterator();
        while(lit.hasNext()){
            String s = lit.next();
            System.out.println(s);
            if ("小黑".equals(s)){
                lit.remove();
            }
        }
        System.out.println(list);
    }
}
