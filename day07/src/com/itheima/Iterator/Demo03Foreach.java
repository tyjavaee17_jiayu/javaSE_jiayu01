package com.itheima.Iterator;

import java.util.ArrayList;

 /*
        使用增强for循环遍历集合
                好处:
                可以在遍历的过程中使用元素特有的方法
                快捷键:
                数组名+集合名.for 增强for



         增强for循环的格式:重点
        for(集合|数组中元素的类型 变量名 : 集合|数组){
            sout(变量名);
        }
*/
public class Demo03Foreach {
    public static void main(String[] args) {
        /*
        使用增强for循环遍历集合
        使用集合存储引用数据类型Student
     */
       show01();
       show02();
       show03();
    }

     private static void show03() {
         int[] arr1 = {1,2,3};
         for (int i = 0; i < arr1.length; i++) {
             arr1[i] *= 2;
             System.out.println(arr1[i]);
         }
         System.out.println("arr1[0]:" + arr1[0]);
         System.out.println("----------------");
         int[] arr2 ={1,2,3};
         for (int a: arr2) {
             a*=2;
             System.out.println(a);
         }
         System.out.println("arr2[0]:" + arr2[0]);
     }

     private static void show02() {
         ArrayList<String> list = new ArrayList<>();
         list.add("aaa");
         list.add("bbb");
         list.add("ccc");
         list.add("ddd");
         for (String s : list) {
             System.out.println(s);
         }
     }

     private static void show01() {
         ArrayList<Student> list = new ArrayList<>();
         list.add(new Student("小红",18));
         list.add(new Student("小明",18));
         list.add(new Student("小黑",18));
         list.add(new Student("小兰",18));
         for (Student stu:list) {
             System.out.println(stu);
         }
     }

 }
