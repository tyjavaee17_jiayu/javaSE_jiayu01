package com.itheima.Iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Demo01Iterator {
    public static void main(String[] args) {
        Collection<String> coll = new ArrayList<>();
        coll.add("小明");
        coll.add("小红");
        coll.add("小兰");
        coll.add("小黑");
        coll.add("小稚");
        System.out.println(coll.size());
        Iterator<String> it = coll.iterator();
        while(it.hasNext()) {
            String s = it.next();
            System.out.println(s);
        }
        System.out.println(coll.size());
    }
}
