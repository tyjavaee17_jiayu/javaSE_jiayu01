package com.itheima.GenericIterface;

public interface GenericInterface<I> {
    public abstract void show(I i);
}
