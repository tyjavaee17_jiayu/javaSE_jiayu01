package com.itheima.GenericIterface;
/*
    含有泛型的接口:第二种使用方式
        定义类实现含有泛型的接口,接口使用什么泛型,实现类就使用什么泛型
        实现类跟着接口走,就和定义一个含有泛型的类是一样的
    格式:
         public class GenericInterfaceImpl2<I> implements GenericInterface<I>{
            方法上使用泛型(I i){ }
         }
     注意:
        创建实现类对象的时候,确定泛型的数据类型;创建对象是什么类型,泛型就是什么类型
 */
public class GenericInterfaceImpl2<I> implements  GenericInterface<I> {

    @Override
    public void show(I i) {
        System.out.println(i);
    }
}
