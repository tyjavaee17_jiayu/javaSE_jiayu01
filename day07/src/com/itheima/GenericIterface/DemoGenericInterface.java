package com.itheima.GenericIterface;

public class DemoGenericInterface {
    public static void main(String[] args) {
        GenericInterfaceImpl1 gil1 = new GenericInterfaceImpl1();
        gil1.show(100);

        System.out.println("------------------");

        GenericInterfaceImpl2<String> gil2 = new GenericInterfaceImpl2<>();
        gil2.show("ppp");

        System.out.println("------------------");

        GenericInterfaceImpl2<Boolean> gil3 = new GenericInterfaceImpl2<>();
        gil3.show(true);
    }
}
