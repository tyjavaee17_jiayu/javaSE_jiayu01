package com.zouye.zuoye01;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class DemoCollection {
    public static void main(String[] args) {
        Collection<String> coll = new ArrayList<>();
        coll.add("JavaEE企业级开发指南");
        coll.add("Oracle高级编程");
        coll.add("MySQL从入门到精通");
        coll.add("Java架构师之路");
        Iterator<String> it = coll.iterator();
        while(it.hasNext()){
            String book = it.next();
            System.out.println(book);
        }
        System.out.println("=====================");
        Iterator<String> it1 = coll.iterator();
        while(it1.hasNext()){
            String book = it1.next();
            if (book.length() < 10){
                System.out.println(book);

            }
        }
        System.out.println("=====================");
        Iterator<String> it2 = coll.iterator();
        while(it2.hasNext()){
            String book = it2.next();
            if (book.contains("Java")){
                System.out.println(book);
            }
        }
        System.out.println("=====================");
        Iterator<String> it3 = coll.iterator();
        while(it3.hasNext()){
            String book = it3.next();
            if (book.contains("Oracle")){
                it3.remove();
            }else{
                System.out.println(book);
            }
        }
    }
}
