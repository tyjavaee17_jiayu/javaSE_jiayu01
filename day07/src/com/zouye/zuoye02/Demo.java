package com.zouye.zuoye02;

import java.util.ArrayList;
import java.util.Collection;

public class Demo {
    public static void main(String[] args) {
        Collection<Double> coll = new ArrayList<>();
        coll.add(88.5);
        coll.add(39.2);
        coll.add(77.1);
        coll.add(56.8);
        coll.add(89.0);
        coll.add(99.0);
        coll.add(59.5);
        for (Double score:coll) {
            System.out.println(score);
        }
        System.out.println("--------------------");
        for (Double score:coll) {
            if (score < 60){
                System.out.println(score);
            }
        }
        System.out.println("--------------------");
        int failNum = 0;
        int failSum = 0;
        for (Double score:coll) {
            if (score < 60){
                failNum++;
                failSum += score;
            }
        }
        int failAvg = failSum / failNum;
        System.out.println("不及格平均分是：" + failAvg);
        System.out.println("--------------------");
        double Max = 0;
        for (Double score:coll) {
            if (score > Max){
                Max = score;
            }
        }
        System.out.println("最高的分数是：" + Max);
    }
}
