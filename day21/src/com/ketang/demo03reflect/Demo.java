package com.ketang.demo03reflect;

public class Demo {
    public static void main(String[] args)throws Exception {
        Class clazz = Class.forName("com.ketang.demo03reflect.AandBImpl");
        Class[] interfaces = clazz.getInterfaces();
        for (Class anInterface : interfaces) {
            System.out.println(anInterface);
        }
        System.out.println("--------------------------");

        Class clazz2 = Class.forName("java.util.ArrayList");
        for (Class anInterface : clazz2.getInterfaces()) {
            System.out.println(anInterface);
        }
    }
}
