package com.ketang.demo09Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DemoTest {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Class clazz = Class.forName("com.ketang.demo09Test.DemoMyTest");
        Object obj = clazz.newInstance();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(MyTest.class)){
                method.invoke(obj);
            }
        }
    }
}
