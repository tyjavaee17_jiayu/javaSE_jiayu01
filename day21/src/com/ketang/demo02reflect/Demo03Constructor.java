package com.ketang.demo02reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Demo03Constructor {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Class clazz = Class.forName("com.ketang.demo02reflect.Person");

        Constructor[] constructor = clazz.getConstructors();

        for (Constructor constructor1: constructor) {
            System.out.println(constructor1);
        }
        System.out.println("-----------------------------");
        Constructor[] declaredConstructors = clazz.getDeclaredConstructors();
        for (Constructor declaredConstructor : declaredConstructors) {
            System.out.println(declaredConstructor);
        }
        System.out.println("-----------------------------");

        Constructor constructor1 = clazz.getConstructor();
        System.out.println(constructor1);

        Constructor constructor2 = clazz.getConstructor(String.class, int.class, String.class);
        System.out.println(constructor2);

        Constructor constructor3 = clazz.getDeclaredConstructor(String.class, int.class);
        System.out.println(constructor3);

        System.out.println("-----------------------------");

        Object obj1 = constructor1.newInstance();
        System.out.println(obj1);

        Object obj2 = constructor2.newInstance("张三", 18, "男");
        System.out.println(obj2);

        constructor3.setAccessible(true);
        Object obj3 = constructor3.newInstance("李四", 18);
        System.out.println(obj3);
    }
}
