package com.ketang.demo02reflect;

import java.lang.reflect.Method;

public class Demo05Method {
    public static void main(String[] args) throws Exception{
        Class clazz = Class.forName("com.ketang.demo02reflect.Person");
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            System.out.println(method);
        }
        System.out.println("-------------------------");

        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod);
        }

        System.out.println("-------------------------");

        Method getAge = clazz.getMethod("getAge");
        System.out.println(getAge);

        Method setAge = clazz.getMethod("setAge", int.class);
        System.out.println(setAge);

        Method privatemethod = clazz.getDeclaredMethod("method");
        System.out.println(privatemethod);

        Object obj = clazz.newInstance();
        Object v1 = getAge.invoke(obj);
        System.out.println("v1+" + v1);

        Object v2 = setAge.invoke(obj, 12);
        System.out.println("v2=" + v2);


        /*
            private void method()
            私有方法没有权限运行,会抛出非法访问异常:IllegalAccessException
            使用暴力反射解决:使用Method的父类AccessibleObject中的方法setAccessible取消java语言的访问检查
         */
        privatemethod.setAccessible(true);
        privatemethod.invoke(obj);
    }
}
