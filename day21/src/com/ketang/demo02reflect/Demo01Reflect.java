package com.ketang.demo02reflect;

public class Demo01Reflect {
    public static void main(String[] args) throws ClassNotFoundException {
        //使用Object类中的方法getClass
        Person person = new Person();
        Class class1 = person.getClass();
        System.out.println(class1);

        System.out.println("--------------------");
        //java会为每种数据类型都赋予一个class属性,这个class属性返回的就是class文件对象
        System.out.println(int.class);
        System.out.println(double.class);
        System.out.println(String.class);

        System.out.println("--------------------");

        Class class2 = Person.class;
        System.out.println(class2);

        System.out.println("--------------------");
        // 可以使用Class类中的静态方法forName
        Class class3 = Class.forName("com.ketang.demo02reflect.Person");
        System.out.println(class3);

        System.out.println("--------------------");

        System.out.println(class1 == class2);
        System.out.println(class1 == class3);
        System.out.println(class2 == class3);

    }
}
