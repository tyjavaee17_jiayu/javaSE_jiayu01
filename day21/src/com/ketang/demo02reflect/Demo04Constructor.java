package com.ketang.demo02reflect;

public class Demo04Constructor {
    public static void main(String[] args) throws Exception{
        //1.获取Person类的class文件对象
        Class clazz = Class.forName("com.ketang.demo02reflect.Person");
        //2.使用class文件对象中的方法newInstance实例化对象
        Object obj = clazz.newInstance();

        Person p = (Person)obj;

        System.out.println(p.getName()+ p.getAge() + p.getSex());
    }
}
