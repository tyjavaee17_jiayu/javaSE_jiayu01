package com.ketang.demo02reflect;

public class Demo02ClassMethod {
    public static void main(String[] args) throws ClassNotFoundException {
        //获取class文件对象
        Class clazz = Class.forName("com.ketang.demo02reflect.Person");

        System.out.println("-----------------------------");

        String simpleName = clazz.getSimpleName();
        System.out.println(simpleName);

        System.out.println("-----------------------------");

        String name = clazz.getName();
        System.out.println(name);
    }
}
