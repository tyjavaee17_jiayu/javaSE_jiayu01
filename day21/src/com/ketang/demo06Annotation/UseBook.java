package com.ketang.demo06Annotation;
@Book(value = "java编程思想",authors = "james")
public class UseBook {
    @Book(value = "红楼梦",price = 155.6,authors = {"曹雪芹","曹雪芹的徒弟"})
    private String name;

    @Book(value = "水浒传",price = 144.5,authors = "施耐庵")
    public void method(){};
}
