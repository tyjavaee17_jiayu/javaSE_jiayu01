package com.ketang.demo06Annotation;

public @interface Book {
    public abstract String value();
    public abstract double price() default 1555.5;
    String[] authors();
}
