package com.ketang.demo07Annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
    元注解:java已经定义好的注解,可以使用元注解修饰自定义的注解
    1.@Target
        作用:用来标识自定义注解可以使用的位置,没有标识使用该注解,自定义注解默认可以使用在任意的位置
        属性:
            ElementType[] value  :只有一个属性叫value,使用的使用,可以省略属性名,直接写属性值
            java.lang.annotation.ElementType:是一个枚举,枚举中的变量都是常量,可以通过枚举名.变量名直接使用
                TYPE，类，接口
                FIELD, 成员变量
                METHOD, 成员方法
                PARAMETER, 方法参数
                CONSTRUCTOR, 构造方法
                LOCAL_VARIABLE, 局部变量
    2.@Retention
        作用:用来标识自定义注解生命周期(作用域)
        属性:
            RetentionPolicy value:只有一个属性叫value,使用的使用,可以省略属性名,直接写属性值
            java.lang.annotation.RetentionPolicy:是一个枚举,枚举中的变量都是常量,可以通过枚举名.变量名直接使用
                SOURCE：注解只作用在源码阶段(.java)，生成的字节码文件(.class)中不存在
                CLASS：注解作用在源码阶段，字节码文件阶段，运行阶段(内存中)不存在，默认值
                RUNTIME：注解作用在源码阶段，字节码文件阶段，运行阶段
 */


//声明自定义的Book注解,可以使用在类上(接口上),构造方法上,成员变量上,成员方法上
@Target({ElementType.TYPE,ElementType.CONSTRUCTOR,ElementType.METHOD,ElementType.FIELD})
//声明自定义的Book注解:在.java文件中,.class文件中和内存中有有效
@Retention(RetentionPolicy.RUNTIME)
public @interface Book {
    public abstract String value();
    public abstract double price() default 1555.5;
    String[] authors();
}
