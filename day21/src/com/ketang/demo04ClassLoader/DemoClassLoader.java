package com.ketang.demo04ClassLoader;

public class DemoClassLoader {
    public static void main(String[] args) {
        Class clazz = DemoClassLoader.class;

        ClassLoader classLoader = clazz.getClassLoader();
        System.out.println(classLoader);

        ClassLoader parent = classLoader.getParent();
        System.out.println(parent);

        ClassLoader grandpa = parent.getParent();
        System.out.println(grandpa);
    }
}
