package com.ketang.demo05Annotation;
@MyAnno01
@MyAnno02(a= 15,arr={"aaa","bbb","ccc","ddd"})
public class UseMyAnno {
    @MyAnno01
    @MyAnno02(a= 20,d = 50.33,arr = "fff")
    private String name;


    @MyAnno01
    @MyAnno03(value = "ooo")
    public UseMyAnno() {
    }

    @MyAnno03("jjj")
    public UseMyAnno(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
