package com.ketang.demo05Annotation;

public @interface MyAnno03 {
    public abstract String value();

    public abstract int abc() default 100;
}
