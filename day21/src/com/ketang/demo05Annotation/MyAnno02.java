package com.ketang.demo05Annotation;

public @interface MyAnno02 {
    public abstract int a();

    public abstract double d() default 5.6;

    public abstract String[] arr();
}
