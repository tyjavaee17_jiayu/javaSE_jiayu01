package com.ketang.demo08Annotation;

import com.ketang.demo05Annotation.MyAnno01;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Arrays;

@MyAnno01
@Book(name = "水浒传",price = 100.2,authors = "施耐庵")
public class Demo01ParentAnnotation {
    @Book( name= "西游记",price = 145.6,authors = "吴承恩")
    public void method(){

    }
    @Test
    public void show01(){
        Class clazz = Demo01ParentAnnotation.class;
        //使用class文件对象中的方法isAnnotationPresent判断类上是否有指定的Book注解
        boolean b = clazz.isAnnotationPresent(Book.class);
        System.out.println(b);
        if (b){
            Book book = (Book) clazz.getAnnotation(Book.class);
            String name = book.name();
            System.out.println(name);
            double price = book.price();
            System.out.println(price);
            String[] authors = book.authors();
            System.out.println(Arrays.toString(authors));
        }
    }
    @Test
    public void show02() throws ClassNotFoundException {
        Class clazz = Class.forName("com.ketang.demo08Annotation.Demo01ParentAnnotation");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            boolean b = method.isAnnotationPresent(Book.class);
            if (b){
                Book book = method.getAnnotation(Book.class);
                System.out.println(book.name());
                System.out.println(book.price());
                System.out.println(Arrays.toString(book.authors()));
            }
        }
    }
}
