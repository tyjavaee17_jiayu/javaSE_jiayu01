package com.zuoye.zuoye03.zuoye0301;

import java.lang.reflect.Method;

public class Demo {
    public static void main(String[] args)throws Exception {
        Class clazz = Class.forName("com.zuoye.zuoye03.zuoye0301.Demo");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(MyTest01.class)){
                Object obj = clazz.newInstance();
                Object invoke = method.invoke(obj);
            }
        }
    }
    @MyTest01
    public void show01(){
        System.out.println("show01方法！");
    }
    public void show02(){
        System.out.println("show02方法！");
    }
    @MyTest01
    public void show03(){
        System.out.println("show03方法！");
    }
    public void show04(){
        System.out.println("show04方法！");
    }
    @MyTest01
    public void show05(){
        System.out.println("show05方法！");
    }
}
