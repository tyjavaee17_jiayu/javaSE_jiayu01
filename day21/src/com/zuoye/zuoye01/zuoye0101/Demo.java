package com.zuoye.zuoye01.zuoye0101;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class Demo {
    public static void main(String[] args) throws Exception{
        Class clazz = Class.forName("com.zuoye.zuoye01.zuoye0101.Student");
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println(constructor);
        }

        System.out.println("-----------------------------");

        Constructor constructor = clazz.getConstructor(String.class, int.class);

        Object obj = constructor.newInstance("柳岩", 17);
        System.out.println(obj);

        System.out.println("----------------------------");

        Method method = clazz.getMethod("show");
        Object invoke = method.invoke(obj);
        System.out.println(invoke);
    }
}
