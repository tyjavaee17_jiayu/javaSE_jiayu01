package com.ketang.demo03Stream;

import java.util.stream.Stream;

public class Demo07Limit {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("美羊羊", "喜羊羊", "懒羊羊", "慢羊羊", "沸羊羊", "慢羊羊", "灰太狼", "红太狼", "小灰灰");

        stream.limit(4).forEach(s -> System.out.println(s));
    }
}
