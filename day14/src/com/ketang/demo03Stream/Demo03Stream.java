package com.ketang.demo03Stream;

import java.util.*;
import java.util.stream.Stream;

public class Demo03Stream {
    public static void main(String[] args) {
        Stream<Integer> stream1 = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Stream<String> stream2 = Stream.of("aaa", "bbb", "ccc", "sss", "ddd", "eee");
        String[] arr = {"a","b","c","d","e"};
        Stream<String> stream3 = Stream.of(arr);
        Integer[] arr1 = {1,2,3};
        Stream<Integer> stream4 = Stream.of(arr1);

        show01();
    }

    private static void show01() {
        ArrayList<String> list = new ArrayList<>();
        Stream<String> stream1 = list.stream();

        LinkedHashSet<String> set = new LinkedHashSet<>();
        Stream<String> stream2 = set.stream();

        HashSet<String> hs = new HashSet<>();
        Stream<String> stream3 = hs.stream();

        HashMap<String,Integer> map = new HashMap<>();
        Set<String> keySet = map.keySet();
        Stream<String> stream4 = keySet.stream();
        Collection<Integer> values = map.values();
        Stream<Integer> stream5 = values.stream();

        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
        Stream<Map.Entry<String, Integer>> stream6 = entrySet.stream();
    }

}
