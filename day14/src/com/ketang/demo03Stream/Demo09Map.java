package com.ketang.demo03Stream;

import java.util.stream.Stream;

public class Demo09Map {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("111", "222", "333", "444");
        stream.map(s -> Integer.parseInt(s)).forEach(s-> System.out.println(s));


        Stream<String> stream1 = Stream.of("老王", "老宋", "金莲");
        stream1.map(s ->new Person(s)).forEach(s -> System.out.println(s));
    }
}
