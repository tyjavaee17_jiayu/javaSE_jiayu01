package com.ketang.demo03Stream;

import java.util.ArrayList;

public class Demo01ArrayList {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("张无忌");
        list.add("周芷若");
        list.add("赵敏");
        list.add("张强");
        list.add("张三丰");

        ArrayList<String> zhanglist = new ArrayList<>();
        for (String name : list) {
            if (name.startsWith("张")){
                zhanglist.add(name);
            }
        }
        System.out.println(zhanglist);

        ArrayList<String> sanlist = new ArrayList<>();
        for (String name : zhanglist) {
            if (name.length() == 3){
                sanlist.add(name);
            }
        }
        System.out.println(sanlist);

        for (String name : sanlist) {
            System.out.println(name);
        }
    }
}
