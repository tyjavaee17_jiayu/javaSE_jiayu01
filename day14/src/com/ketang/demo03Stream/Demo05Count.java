package com.ketang.demo03Stream;

import java.util.stream.Stream;

public class Demo05Count {
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9);

        long count = stream.count();
        System.out.println(count);
    }
}
