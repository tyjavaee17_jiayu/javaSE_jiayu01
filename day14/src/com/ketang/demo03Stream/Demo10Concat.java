package com.ketang.demo03Stream;

import java.util.stream.Stream;

public class Demo10Concat {
    public static void main(String[] args) {
        Stream<String> stream1 = Stream.of("美羊羊", "喜羊羊", "懒羊羊", "慢羊羊", "沸羊羊", "慢羊羊", "灰太狼", "红太狼", "小灰灰");
        Stream<String> stream2 = Stream.of("1", "2", "3", "4", "5", "6", "7", "8");

        Stream.concat(stream1,stream2).forEach(s -> System.out.println(s));
    }
}
