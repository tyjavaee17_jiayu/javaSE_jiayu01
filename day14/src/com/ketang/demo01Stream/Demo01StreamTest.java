package com.ketang.demo01Stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/*
    Stream综合案例
        现在有两个 ArrayList 集合存储队伍当中的多个成员姓名，要求使用传统的for循环（或增强for循环）
        依次进行以下若干操作步骤：
        1. 第一个队伍只要名字为3个字的成员姓名,把3个字的成员姓名存储到一个新的集合中
        2. 第一个队伍筛选之后只要前3个人,把前3个人存储到一个新的集合中
        3. 第二个队伍只要姓张的成员姓名,把姓张的成员姓名存储到一个新的集合中
        4. 第二个队伍筛选之后不要前2个人,跳过前2个人,其余的人存储到一个新的集合中
        5. 将两个队伍合并为一个队伍
        6. 根据字符串姓名创建 Person 对象,存储到一个新的集合中
        7. 打印整个队伍的Person对象信息。
 */
public class Demo01StreamTest {
    public static void main(String[] args) {
        List<String> list1= new ArrayList<>();
        list1.add("张无忌");
        list1.add("周芷若");
        list1.add("张三丰");
        list1.add("赵敏");
        list1.add("张强");
        Stream<String> stream1 = list1.stream().filter(s -> s.contains("张")).skip(2);

        List<String> list2= new ArrayList<>();
        list2.add("迪丽热巴");
        list2.add("赵丽颖");
        list2.add("胡歌");
        list2.add("霍建华");
        list2.add("陈乔恩");
        Stream<String> stream2 = list2.stream().filter(s -> s.length() == 3).limit(3);

        Stream<String> allstream = Stream.concat(stream1, stream2);

        allstream.map(s -> new Person(s)).forEach(s-> System.out.println(s));
    }
}
