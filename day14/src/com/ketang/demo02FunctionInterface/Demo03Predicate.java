package com.ketang.demo02FunctionInterface;

import java.util.function.Predicate;

public class Demo03Predicate {
    public static void main(String[] args) {
        method(s -> s.length() == 5,"1111111");
    }
    public static void method(Predicate<String> pre, String s){
        boolean b = pre.test(s);
        System.out.println(b);
    }
}
