package com.ketang.demo02FunctionInterface;

import java.util.Random;
import java.util.function.Supplier;

public class Demo05Supplier {
    public static void main(String[] args) {
        method(()->30);
        method(()->new Random().nextInt(100));
    }

    private static void method(Supplier<Integer> supplier) {
        Integer integer = supplier.get();
        System.out.println(integer);
    }

}
