package com.ketang.demo02FunctionInterface;

import java.util.function.Function;

public class Demo04Fuction {
    public static void main(String[] args) {
     method(s -> Integer.parseInt(s),"100");
    }

    private static void method(Function<String,Integer> function,String s) {
        Integer integer = function.apply(s);
        System.out.println(integer);
    }
}
