package com.ketang.demo02FunctionInterface;

public class Demo01MyFunction {
    public static void main(String[] args) {
    menthod(new MyFunction() {
        @Override
        public void show(int a) {
            System.out.println("使用匿名内部类的方式，重写了show方法"+ a);
        }
    },5);

    menthod((a)->{
        System.out.println("使用Lambda表达式重写了show方法" + a);
    },5);

    menthod(a -> System.out.println(a),10);
    }
    /*
        定义一个方法,方法的参数类型使用函数式接口MyFunction
     */
    public static void menthod(MyFunction my , int a ){
        my.show(a);
    }
}
