package com.ketang.demo02FunctionInterface;
@FunctionalInterface
public interface MyFunction {
    public abstract void show(int a);
}
