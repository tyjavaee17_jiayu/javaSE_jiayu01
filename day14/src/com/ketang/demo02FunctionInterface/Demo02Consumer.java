package com.ketang.demo02FunctionInterface;

import java.util.function.Consumer;

public class Demo02Consumer {
    public static void main(String[] args) {
        method(s -> System.out.println(s),"aaa");
    }
    public static  void method(Consumer<String> consumer, String s){
        consumer.accept(s);
    }
}
