package com.ketang.demo05Stream;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo02Stream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("美羊羊", "喜羊羊", "懒羊羊", "慢羊羊", "暖羊羊", "沸羊羊", "灰太狼", "红太狼", "小灰灰","美羊羊");

        Object[] objects = stream.toArray();
        for (Object object : objects) {
            System.out.println(object);
        }
    }
}
