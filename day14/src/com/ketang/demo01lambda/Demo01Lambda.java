package com.ketang.demo01lambda;

public class Demo01Lambda {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"使用匿名内部类的方式，实现多线程");
            }
        }).start();

        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"使用Lambda表达式的方式，实现多线程");
        }).start();
    }
}
