package com.ketang.demo01lambda;

import java.util.Arrays;
import java.util.Comparator;

/*
    使用Lambda表达式重写有参数有返回值的方法
    需求:
        创建一个数组,数组类型使用Person
        创建3个Person对象,存储到数组中
        使用Arrays数组工具类中的方法sort,根据比较器产生的规则对Person对象进行排序(年龄升序)
 */
public class Demo02Lambda {
    public static void main(String[] args) {
        Person[] arr  = new Person[3];

        arr[0] = new Person("张三",18);
        arr[1] = new Person("李四",19);
        arr[2] = new Person("王五",20);

        System.out.println("排序前的数组中的元素："+Arrays.toString(arr));

        Arrays.sort(arr, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge()-o2.getAge();
            }
        });

        System.out.println("排序后的数组中的元素："+Arrays.toString(arr));

        Arrays.sort(arr,(o1,o2)-> o2.getAge()-o1.getAge());

        System.out.println("排序后，数组中的元素："+ Arrays.toString(arr));
    }
}
