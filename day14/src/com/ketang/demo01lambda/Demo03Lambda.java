package com.ketang.demo01lambda;

import java.util.Arrays;

public class Demo03Lambda {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"使用匿名内部类的方式，实现多线程");

            }
        }).start();

        new Thread(()-> System.out.println(Thread.currentThread().getName()+"使用Lambda表达式的方式，实现多线程")).start();

        Person[] arr= new Person[3];

        arr[0] = new Person("张三",18);
        arr[1] = new Person("李四",19);
        arr[2] = new Person("王五",20);

        System.out.println(Arrays.toString(arr));

        Arrays.sort(arr,(o1,o2)->o2.getAge() - o1.getAge());

        System.out.println(Arrays.toString(arr));
    }
}
