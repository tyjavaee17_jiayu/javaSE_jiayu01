package com.zuoye.zuoye01.zuoye0102;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class DemoLambda {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list,"cab","bac","acb","cba","bca","abc");
        Collections.sort(list,(o1,o2)->o2.compareTo(o1));
        System.out.println(list);
    }
}
