package com.zuoye.zuoye01.zuoye0101;

public class DemoLambda {
    public static void main(String[] args) {
        new Thread(()->{
            for (int i = 1; i <=100 ; i++) {
                System.out.println(i);
            }
        }).start();
    }
}
