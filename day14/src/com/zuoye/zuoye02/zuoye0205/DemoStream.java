package com.zuoye.zuoye02.zuoye0205;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class DemoStream {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list,"王佳乐","张三丰","王思聪","张飞","刘晓敏","张靓颖","王敏");

        Stream<String> stream1 = list.stream().filter(s -> s.contains("张"));
        Stream<String> stream2 = list.stream().filter(s -> s.contains("王"));

        Stream.concat(stream1, stream2).forEach(s -> System.out.println(s));

    }
}
