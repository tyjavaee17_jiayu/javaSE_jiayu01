package com.zuoye.zuoye02.zuoye0204;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoStream {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list,"王佳乐","张三丰","王思聪","张飞","刘晓敏","张靓颖","王敏");
        list.stream().filter(s -> s.contains("张")).limit(2).forEach(s -> System.out.println(s));
        System.out.println("---------------");
        list.stream().filter(s -> s.contains("王")).skip(1).forEach(s -> System.out.println(s));
    }
}
