package com.zuoye.zuoye02.zuoye0201;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoStream {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list,"张三丰","王思聪","张飞","刘晓敏","张靓颖");
        list.stream().forEach(s -> System.out.println(s));
    }
}
