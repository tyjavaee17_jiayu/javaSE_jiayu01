package com.zuoye.zuoye02.zuoye0206;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DemoStream {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list,"王佳乐","张三丰","王思聪","张飞");
        Stream<Person> personStream = list.stream().map(s -> new Person(s));
        List<Person> personList = personStream.collect(Collectors.toList());
        for (Person name : personList) {
            System.out.println(name.getName());
        }
    }
}
