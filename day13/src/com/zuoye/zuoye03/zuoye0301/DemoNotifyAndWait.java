package com.zuoye.zuoye03.zuoye0301;

public class DemoNotifyAndWait {
    private static Object lock = new Object();
    private static int count = 0;
    private static boolean flag = false;
    public static void main(String[] args) throws InterruptedException {

        new Thread(){
            @Override
            public void run() {
                synchronized (lock){
                    try {
                        for (int i = 1; i <= 1000; i++) {
                            if (i % 2 == 0 && i % 3 == 0&& i % 5 == 0&& i % 7 == 0){
                                flag =true;
                                lock.wait();
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        new Thread(){
            @Override
            public void run() {
                while (true){
                synchronized (lock){
                       if (flag == true){
                           count++;
                           flag = false;
                           lock.notify();
                       }
                   }
                }
            }
        }.start();
        Thread.sleep(2000);
        System.out.println(count);
    }
}
