package com.zuoye.zuoye02.zuoye0201;

public class ThreadA extends Thread {

    @Override
    public void run() {
        synchronized (Suo.lockA){
            System.out.println(ThreadA.currentThread().getName()+"线程进入A锁。。。");
            synchronized (Suo.lockB){
                System.out.println(ThreadA.currentThread().getName()+"线程进入B锁。。。");
            }
        }
    }
}
