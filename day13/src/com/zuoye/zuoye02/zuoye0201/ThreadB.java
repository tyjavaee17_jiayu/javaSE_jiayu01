package com.zuoye.zuoye02.zuoye0201;

public class ThreadB implements Runnable{

    @Override
    public void run() {
        synchronized (Suo.lockB){
            System.out.println(Thread.currentThread().getName()+"进入B锁。。。");
            synchronized (Suo.lockA){
                System.out.println(Thread.currentThread().getName()+"进入B锁。。。");
            }
        }
    }
}
