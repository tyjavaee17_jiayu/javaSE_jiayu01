package com.zuoye.zuoye02.zuoye0201;

public class DemoDeadLock {
    public static void main(String[] args) {
        new ThreadA().start();
        new Thread(new ThreadB()).start();
    }
}
