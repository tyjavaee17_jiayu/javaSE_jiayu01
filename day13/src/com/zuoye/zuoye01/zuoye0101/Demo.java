package com.zuoye.zuoye01.zuoye0101;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Demo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService es = Executors.newFixedThreadPool(1);
        CallableImpl callable = new CallableImpl();
        Future<Integer> future = es.submit(callable);
        Integer sum = future.get();
        System.out.println(sum);

    }
}
