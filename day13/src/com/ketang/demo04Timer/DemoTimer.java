package com.ketang.demo04Timer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class DemoTimer {
    public static void main(String[] args) throws Exception {

        //show01();
        //show02();
        //show03();
        show04();
    }

    private static void show04() throws ParseException {
        Timer time = new Timer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse("2020-03-30 17:09:00");
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("嘿嘿");
            }
        },date,1000);
    }

    private static void show03() throws ParseException {
        Timer time = new Timer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse("2020-03-30 17:06:00");
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("呵呵");
                time.cancel();
            }
        },date);
    }

    private static void show02() {
        Timer time = new Timer();
        Date  date = new Date();
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String s = sdf.format(new Date());
                System.out.println(s);
            }
        },2000,1000);
    }

    private static void show01() {
        Timer time = new Timer();
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("哈哈");
            }
        },1000);
    }
}