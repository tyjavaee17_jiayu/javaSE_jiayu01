package com.ketang.demo01ThreadPool;

import java.util.Scanner;
import java.util.concurrent.*;

/*
    线程池的练习
        需求: 使用线程池方式执行任务,返回1-n的和
        分析: 因为需要返回求和结果,所以使用Callable方式的任务
    实现步骤:
         1.使用Scanner获取一个用户输入的整数
         2.使用Executors线程池工厂类中的静态方法newFixedThreadPool创建一个包含执行线程数量的线程池ExecutorService
         3.创建Callable接口的实现类对象,重写call方法,设置线程任务(计算1-n的和返回)
         4.使用线程池ExecutorService中的方法submit,传递线程任务
            submit方法就会在线程池中获取一个线程执行线程任务,执行完先任务,submit方法会把线程在归还给线程池
 */
public class Demo03Test {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //1.使用Scanner获取一个用户输入的整数
        System.out.println("请输入一个整数：");
        int n = new Scanner(System.in).nextInt();
        // 2.使用Executors线程池工厂类中的静态方法newFixedThreadPool创建一个包含执行线程数量的线程池ExecutorService
        ExecutorService es = Executors.newFixedThreadPool(3);
        // 3.创建Callable接口的实现类对象,重写call方法,设置线程任务(计算1-n的和返回)
        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int sum = 0;
                for (int i = 0; i < n; i++) {
                    sum += i;
                }
                return sum;
            }
        };
        // 4.使用线程池ExecutorService中的方法submit,传递线程任务
        //    submit方法就会在线程池中获取一个线程执行线程任务,执行完先任务,submit方法会把线程在归还给线程池
        Future<Integer> future = es.submit(callable);
        Integer integer = future.get();
        System.out.println("1-"+n+"之间的和为：" + integer);

        es.shutdown();
    }
}
