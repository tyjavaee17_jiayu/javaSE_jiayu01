package com.ketang.demo01ThreadPool;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Demo01ThreadPool {
    public static void main(String[] args) {
        RunnableImpl r = new RunnableImpl();

        ExecutorService es = Executors.newFixedThreadPool(5);
        es.submit(r);
        es.submit(r);
        es.submit(r);
        es.submit(r);
        es.submit(r);
        //匿名内部类
        es.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+"线程在执行");
            }
        });
    }
}
