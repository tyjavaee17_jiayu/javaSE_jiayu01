package com.ketang.demo01ThreadPool;

import java.util.Random;
import java.util.concurrent.*;

public class Demo02ThreadPool {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService es = Executors.newFixedThreadPool(3);
        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return new Random().nextInt(10);
            }
        };
        Future<Integer> future = es.submit(callable);
        System.out.println(future);
        Integer v = future.get();
        System.out.println(v);
        Future<Double> f2 = es.submit(new Callable<Double>() {
            @Override
            public Double call() throws Exception {
                //返回一个随机小数[0.0-1.0)
                return Math.random();
            }
        });
        System.out.println(f2.get());
    }
}
