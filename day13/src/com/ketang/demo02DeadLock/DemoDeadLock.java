package com.ketang.demo02DeadLock;

public class DemoDeadLock {
    public static void main(String[] args) {
        RunnableImpl runnable = new RunnableImpl();
        new Thread(runnable).start();
        new Thread(runnable).start();
    }
}
