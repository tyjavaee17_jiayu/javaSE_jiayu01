package com.ketang.demo02DeadLock;
/*
    死锁:线程获取不到锁对象,从而进不去同步中执行
        两个线程你拿着我的锁,我拿着你的锁,导致两个线程都进不去同步中继续执行
    前提:
        1.必须出现同步代码块的嵌套
        2.必须有两个线程
        3.必须有两个锁对象
 */
public class RunnableImpl implements Runnable{
    private String lockA = "A锁";
    private String lockB = "B锁";

    int a = 0;

    @Override
    public void run() {
        while (true){
            if (a % 2 == 0){
                synchronized (lockA){
                    System.out.println(Thread.currentThread().getName()+ "进入if语句的A锁。。。");
                    synchronized (lockB){
                        System.out.println(Thread.currentThread().getName()+"进入if语句的B锁。。。");
                    }
                }
            }else{
                synchronized (lockB){
                    System.out.println(Thread.currentThread().getName()+"进入else语句的B锁。。。");
                    synchronized (lockA){
                        System.out.println(Thread.currentThread().getName()+"进入else语句的A锁。。。");
                    }
                }
            }
            a++;
        }
    }
}
