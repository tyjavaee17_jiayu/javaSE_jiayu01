package com.ketang.demo03waitAndnotify;

public class BaoZiPu extends Thread{
    private  Baozi  baozi = new Baozi();

    public BaoZiPu(Baozi baozi) {
        this.baozi = baozi;
    }

    @Override
    public void run() {
        synchronized (baozi){
            if (baozi.flag == true){
                try {
                    baozi.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
                baozi.pi = "薄皮儿";
                baozi.xian = "猪肉大葱";
                System.out.println("包子铺正在包"+baozi.pi+baozi.xian+"的包子");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("包子铺用3秒做出了"+baozi.pi+baozi.xian+"的包子");
                baozi.flag = true;
            }
            baozi.notify();
        }
    }
}
