package com.ketang.demo03waitAndnotify;

public class ChiHuo implements Runnable {
    private  Baozi baozi = new Baozi();

    public ChiHuo(Baozi baozi) {
        this.baozi = baozi;
    }
    @Override
    public void run() {
        synchronized (baozi){
            if (baozi.flag == true){
                System.out.println("吃货正在"+baozi.pi+baozi.xian+"的包子");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("吃货用1秒吃完了"+baozi.pi+baozi.xian+"的包子");
            }else {
                try {
                    baozi.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                baozi.flag = false;
            }
            baozi.notify();
        }
    }
}
