package day02.com.ketang.demo15test;

public class JiDuQuan extends Quan implements JiDu{

    @Override
    public void JiDu() {
        System.out.println("缉毒犬正在缉毒!");
    }

    @Override
    public void houJiao() {
        System.out.println("缉毒犬嗷嗷的叫!");
    }

    @Override
    public void eat() {
        System.out.println("缉毒犬吃皇家狗粮!");
    }

    @Override
    public void sleep() {
        System.out.println("缉毒犬挂着睡");
    }
}
