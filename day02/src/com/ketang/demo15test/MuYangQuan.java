package day02.com.ketang.demo15test;

public class MuYangQuan extends Quan implements MuYang {
    @Override
    public void muYang() {
        System.out.println("牧羊犬在放羊!");
    }

    @Override
    public void houJiao() {
        System.out.println("牧羊犬在叫唤!");
    }

    @Override
    public void eat() {
        System.out.println("牧羊犬在吃羊!");
    }

    @Override
    public void sleep() {
        System.out.println("牧羊犬在羊圈睡觉!");
    }
}
