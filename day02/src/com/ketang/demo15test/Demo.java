package day02.com.ketang.demo15test;

public class Demo {
    public static void main(String[] args) {
        //星巴类
        XingBa xb = new XingBa();
        xb.eat();
        xb.houJiao();
        xb.sleep();
        System.out.println("-----------");
        //缉毒犬
        JiDuQuan jdq= new JiDuQuan();
        jdq.eat();
        jdq.houJiao();
        jdq.sleep();
        jdq.JiDu();
        System.out.println("------------");
        //牧羊1号
        MuYangQuan myq = new MuYangQuan();
        myq.eat();
        myq.houJiao();
        myq.sleep();
        myq.muYang();
        System.out.println("------------");
        //牧羊2号
        MuYangQuan2 myq2 = new MuYangQuan2();
        myq2.eat();
        myq2.houJiao();
        myq2.sleep();
        myq2.JiDu();
        myq2.muYang();
    }
}
