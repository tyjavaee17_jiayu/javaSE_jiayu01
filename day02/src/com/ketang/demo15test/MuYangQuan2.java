package day02.com.ketang.demo15test;

public class MuYangQuan2 extends Quan implements MuYang,JiDu {
    @Override
    public void JiDu() {
        System.out.println("牧羊犬2号在缉毒!");
    }

    @Override
    public void muYang() {
        System.out.println("牧羊犬2号在放羊!");
    }

    @Override
    public void houJiao() {
        System.out.println("牧羊犬2号在叫唤!");
    }

    @Override
    public void eat() {
        System.out.println("牧羊犬2号在吃羊!");
    }

    @Override
    public void sleep() {
        System.out.println("牧羊犬2号在羊圈睡觉!");
    }
}
