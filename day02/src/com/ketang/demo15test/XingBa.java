package day02.com.ketang.demo15test;

public class XingBa extends Quan {

    @Override
    public void houJiao() {
        System.out.println("星巴旺旺的叫");
    }

    @Override
    public void eat() {
        System.out.println("星巴吃的很少");
    }

    @Override
    public void sleep() {
        System.out.println("星巴趴着睡");
    }
}
