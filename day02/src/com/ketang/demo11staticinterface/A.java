package day02.com.ketang.demo11staticinterface;

public interface A {
    public static void a(){
        System.out.println("A接口中的静态a方法!");
    }

    public static void show(){
        System.out.println("A接口中的静态show方法!");
    }
}
