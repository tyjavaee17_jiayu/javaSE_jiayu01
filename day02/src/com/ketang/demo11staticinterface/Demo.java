package day02.com.ketang.demo11staticinterface;

public class Demo {
    public static void main(String[] args) {
        A.a();
        A.show();
        B.b();
        B.show();

        AandBImpl ab = new AandBImpl();
        //ab.// expected. Identifier expected.
    }
}
