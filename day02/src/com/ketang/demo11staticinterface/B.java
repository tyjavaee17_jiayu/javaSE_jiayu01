package day02.com.ketang.demo11staticinterface;

public interface B {
    public static void b(){
        System.out.println("B接口中的静态b方法!");
    }

    public static void show(){
        System.out.println("B接口中的静态show方法!");
    }
}
