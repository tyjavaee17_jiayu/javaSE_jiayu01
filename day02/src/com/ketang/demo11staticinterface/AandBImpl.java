package day02.com.ketang.demo11staticinterface;
/*
    接口的多实现:实现类实现含有静态方法的多个接口
    格式:
        public class 实现类 implements 接口1,接口2,接口3...{
        }
    注意:
        1.实现类实现含有静态方法的接口,没有意义;静态方法不能被继承,也不能重写,属于接口本身
        2.接口中的静态方法有重复的,不会冲突,静态方法属于每个接口本身
 */
public class AandBImpl implements A,B {

}
