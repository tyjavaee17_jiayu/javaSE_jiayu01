package day02.com.ketang.demo08staticinterface;

public class DemoMyInter {
    public static void main(String[] args) {
        //可以通过接口名.静态方法名(参数)直接使用
        MyInter.show01();
        MyInter.show02("xixi");

        int a = MyInter.show02("haha");
        System.out.println(a);

        MyInterImpl.show01();
    }
}
