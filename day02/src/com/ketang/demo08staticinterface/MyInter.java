package day02.com.ketang.demo08staticinterface;
/*
    定义使用含有静态方法的接口(了解)
    静态方法定义格式
        修饰符 static 返回值类型 方法名(参数){
            方法体;
        }
    注意:
        定义静态方法不能省略static关键字
   含有静态方法的接口使用:
        静态成员属于接口(类)本身,所以可以通过接口名.静态方法名(参数)直接使用
 */
public interface MyInter {
    public static void show01(){
        System.out.println("MyInter接口中的静态show01方法!");
    }
    public static int show02(String a){
        System.out.println("MyInter接口中的静态show02方法!"+a);
        return 100;
    }
}
