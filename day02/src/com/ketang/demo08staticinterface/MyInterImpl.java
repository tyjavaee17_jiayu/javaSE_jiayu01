package day02.com.ketang.demo08staticinterface;
/*
       实现类中能重写静态方法吗?
       不能
       静态方法是不能重写的,静态方法属于类|接口本身,不能被继承,也不能被重写
       在实现类中定义了静态方法,只是名字和接口中的方法一样,属于实现类本身
    */
//@Override  Method does not override method from its superclass
public class MyInterImpl implements MyInter{
    public static void show01(){
        System.out.println("MyInter接口中的静态show01方法!");
    }
}
