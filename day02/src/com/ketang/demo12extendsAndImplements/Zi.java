package day02.com.ketang.demo12extendsAndImplements;
/*
    一个类可以在继承父类的同时,实现多个接口
    格式:
        public class 子类 extends 父类 implements 接口1,接口2,接口3...{
        }
    注意:
        1.父类|接口中有抽象方法,子类需要全部重写
        2.父类中的普通方法和接口中的默认方法重名了,子类优先使用父类的
 */
public class Zi extends QinDie implements GanDie1,GanDie2{

    @Override
    public void kaoBeiDa() {
        System.out.println("Zi类重写GanDie1和GanDie2的kaoBeiDa的抽象方法!");
    }

    @Override
    public void findGF() {
        System.out.println("Zi类重写GanDie2的findGF的抽象方法!");
    }
}
