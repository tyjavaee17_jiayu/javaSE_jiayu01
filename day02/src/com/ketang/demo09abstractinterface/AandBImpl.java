package day02.com.ketang.demo09abstractinterface;
/*
    接口的多实现:类可以同时实现多个接口
    格式:
        public class 实现类 implements 接口1,接口2,接口3,...{
        }
    注意:
        接口中含有抽象方法,实现需要重写所有接口中的抽象方法
        如果接口中有同名的抽象方法,实现类只重写一个就可以了
        不会产生不确定性,抽象方法没有方法体,重写一个就相当于都重写了
 */
public class AandBImpl implements A,B {

    @Override
    public void a() {
        System.out.println("实现类重写A接口中的抽象a方法!");
    }

    @Override
    public void b() {
        System.out.println("实现类重写B接口中的抽象b方法!");
    }

    @Override
    public void show() {
        System.out.println("实现类重写A接口和B接口中的抽象show方法!");
    }
}
