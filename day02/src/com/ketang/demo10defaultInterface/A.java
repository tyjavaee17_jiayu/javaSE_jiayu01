package day02.com.ketang.demo10defaultInterface;

public interface A {
    public default void a(){
        System.out.println("A接口中的默认a方法!");
    }

    public default void show(){
        System.out.println("A接口中的默认show方法!");
    }
}
