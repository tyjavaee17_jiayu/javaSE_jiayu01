package day02.com.ketang.demo10defaultInterface;
/*
    接口的多实现:类实现含有默认方法的多个接口
    格式:
        public class 实现类 implements 接口1,接口2,接口3....{
        }
    注意:
        1.如果多个接口的默认方法不重复,实现类可以选择重写或者不重写默认方法
            重写:使用实现类重写的
            不重写:使用继承自接口的
        2.如果多个接口中的默认方法有重复的,实现类必须重写这个重复的默认方法
            重复了有不确定性,实现类不知道使用继承自哪个接口的默认方法;重写之后使用自己重写的
 */
public class AandBImpl implements A,B {
    @Override
    public void show() {
        System.out.println("实现类重写A接口和B接口中默认的show方法!");
    }
}
