package day02.com.ketang.demo10defaultInterface;

public interface B {
    public default void b(){
        System.out.println("B接口中的默认b方法!");
    }

    public default void show(){
        System.out.println("B接口中的默认show方法!");
    }
}
