package day02.com.ketang.demo03finalVariable;

public class Demo01FinalVariable {
    public static void main(String[] args) {
        final int a = 10;
        final Person p = new Person("张三",20);
        System.out.println(p);//@1540e19d
        System.out.println(p.getName());

        p.setName("李四");
        System.out.println(p);//@1540e19d
        System.out.println(p.getName());
    }
}
