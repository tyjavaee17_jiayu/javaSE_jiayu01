package day02.com.ketang.demo14interface;
/*
    接口中其他成员的特点:
        接口中，无法定义成员变量，但是可以定义常量，其值不可以改变，默认使用public static final修饰(省略不写)。
            常量的命名规则:所有的单词都需要大写,多个单词之间使用_连接
                AAA_BBB_CCC
            在工作中,可以定义一些常用的常量(很少改变)
                公司名称:"江苏省传智播客教育股份有限公司北京分公司"
                公司的网址: www.itcast.cn    www.itheima.com
                公司的地址: 北京市顺义区京顺路99号
        接口中，没有构造方法，不能创建对象。
        接口中，没有静态代码块(讲解静态代码块的时候在讲)
 */
public interface MyInter {
    public static final String NAME = "山西省";
    public static final String WEB = "www.shanxi.com";
    String ADDRESS = "山西省吕梁市";
}
