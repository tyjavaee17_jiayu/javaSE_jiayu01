package day02.com.ketang.demo02finalMethod;

public class Zi extends Fu {
    public void show01(){
        System.out.println("Zi类重写Fu类没有被final修饰的show01方法！");
    }

    //子类不能重写父类final修饰的方法
    //public void show02(){}
    //'show02()' cannot override 'show02()' in 'day02.com.ketang.demo02finalMethod.Fu'; overridden method is final
}
