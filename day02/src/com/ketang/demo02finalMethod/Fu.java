package day02.com.ketang.demo02finalMethod;

public class Fu {
    public void show01(){
        System.out.println("Fu类没有被final修饰得show01方法！");

    }
    public final void show02(){
        System.out.println("Fu类被final修饰得show02方法！");
    }
}
