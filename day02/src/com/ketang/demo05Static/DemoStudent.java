package day02.com.ketang.demo05Static;
/*
    定义每个学生私有属性(非静态)
        只有一种使用方式:创建对象,访问属性,调用方法
    定义每个学生共享(静态)
        有两种使用方式:
            1.创建对象,访问静态属性,调用静态方法(不推荐,占用内存,效率低)
            2.静态的成员属于类,所以我们可以通过类名直接使用
                类名.静态成员变量
                类名.静态的成员方法(参数)
 */
public class DemoStudent {
    public static void main(String[] args) {
        Student s = new Student();
        System.out.println(s.a);
        System.out.println(s.b);
        s.show01();
        s.show02();

        //静态的成员,通过类名可以直接使用
        System.out.println(Student.b);
        Student.show02();

        Student.b = 100;
        System.out.println(Student.b);

        DemoStudent.method();
        //同一个类中,使用静态成员,可以省略类名
        method();
    }
    public static void method(){
        System.out.println("method方法");
    }
}
