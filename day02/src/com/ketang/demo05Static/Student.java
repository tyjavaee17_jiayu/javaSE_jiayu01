package day02.com.ketang.demo05Static;

public class Student {
    int a = 10;
    static int b = 20;

    public void show01(){
        System.out.println("非静态的show01方法");
    }
    public static void show02(){
        System.out.println("静态的show02方法");
    }
}
