package day02.com.ketang.demo04static;
/*
    定义每个学生私有的属性(非静态):姓名,年龄
    定义每个学生共享属性(静态):国家
 */
public class Student {
    String name;
    int age;
    static String country = "中国";
}
