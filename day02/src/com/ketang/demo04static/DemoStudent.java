package day02.com.ketang.demo04static;

public class DemoStudent {
    public static void main(String[] args) {
        Student s1 = new Student();
        s1.name = "张三";
        s1.age = 18;
        System.out.println(s1.name + "\t" + s1.age);
        System.out.println(s1.country);

        Student s2 = new Student();
        s2.name = "李四";
        s2.age = 19;
        System.out.println(s2.name + "\t" + s2.age);
        System.out.println(s2.country);
        s2.country = "中华人民共和国";
        System.out.println(s1.country);
        System.out.println(s2.country);
    }
}
