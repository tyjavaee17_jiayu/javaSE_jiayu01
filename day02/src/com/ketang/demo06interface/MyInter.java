package day02.com.ketang.demo06interface;
/*
    接口:是功能的集合(接口中定义都是方法)
         接口中不能定义变量,可以定义常量(很少使用)
    定义接口使用的也是.java文件;编译生成的也是.class文件
    定义接口使用关键字:interface
    定义格式:
        修饰符 interface 接口名{
            常量;
            抽象方法;
            默认方法;(JDK1.8)
            静态方法;(JDK1.8)
        }
    ----------------------------------------------------
    定义含有抽象方法的接口
    抽象方法:没有方法体,被abstract修饰的方法
    定义格式:
        public abstract 返回值类型 方法名(参数);
        返回值类型 方法名(参数);
    注意:
        接口中的抽象方法修饰符可以省略不写,不写默认也是public abstract
        建议写出,增强阅读性
    -----------------------------------
    接口的使用:
        1.接口不能创建对象使用
        2.定义一个类,实现(继承)接口,重写接口中的抽象方法,创建实现类对象使用
 */
public interface MyInter {
    void show01();

    public abstract int show02();

    public abstract void show03(String s);
}
