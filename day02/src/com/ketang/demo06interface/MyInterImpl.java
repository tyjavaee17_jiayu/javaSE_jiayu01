package day02.com.ketang.demo06interface;
/*
    定义接口的实现类
    作用:使用类实现接口,重写接口中的抽象方法
    格式:
        public class 实现类 implements 接口{
            重写接口中的抽象方法;
        }
    注意:
        实现类似于继承,类实现接口,就可以继承接口中所有非私有的方法
    重写方法:
        快捷键:alt+回车
 */
public class MyInterImpl implements MyInter {

    @Override
    public void show01() {
        System.out.println("实现类重写接口中的抽象show01方法");
    }

    @Override
    public int show02() {
        System.out.println("实现类重写接口中的抽象show02方法");
        return 0;
    }

    @Override
    public void show03(String s) {
        System.out.println("实现类重写接口中的抽象show03方法"+s);
    }
}
