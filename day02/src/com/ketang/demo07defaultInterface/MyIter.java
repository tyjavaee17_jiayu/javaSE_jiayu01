package day02.com.ketang.demo07defaultInterface;
/*
    定义使用含有默认方法的接口
    默认方法定义格式:
        修饰符 default 返回值类型 方法名(参数){
            方法体;
        }
     注意:
        默认方法的修饰符default不能省略
 */
public interface MyIter {
    public default void show01(){
        System.out.println("MyInter接口中的默认show01方法");
    }
    public default void show02(){
        System.out.println("MyInter接口中的默认show02方法");
    }
}
