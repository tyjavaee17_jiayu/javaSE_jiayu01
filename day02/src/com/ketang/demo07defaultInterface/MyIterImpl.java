package day02.com.ketang.demo07defaultInterface;
/*
    含有默认方法的接口使用:定义实现类,实现接口,选择性的重写默认方法,创建实现类对象使用
        重写了默认方法:使用使用重写后的方法
        没有重写默认方法:使用继承自接口的默认方法
    注意:
        实现类重写接口中的默认方法,去掉default关键字
 */
public class MyIterImpl implements MyIter {

    public void show01(){
        System.out.println("实现类重写了MyInter接口中的默认show01方法");
    }
}
