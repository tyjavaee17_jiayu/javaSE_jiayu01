package day02.com.ketang.demo13interface;

public interface A {
    public abstract void a();
    public default void show(){
        System.out.println("A接口中的默认show方法！");
    }
}
