package day02.com.ketang.demo13interface;

public class CImpl implements C{

    @Override
    public void c() {
        System.out.println("实现类重写了A接口中的抽象c方法");
    }

    @Override
    public void a() {
        System.out.println("实现类重写了B接口中的抽象a方法");
    }

    @Override
    public void b() {
        System.out.println("实现类重写了C接口中的抽象b方法");
    }
}
