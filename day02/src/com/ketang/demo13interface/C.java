package day02.com.ketang.demo13interface;

import org.omg.CORBA.PUBLIC_MEMBER;

/*
    类与类之间:继承关系
        public class 子类 extends 父类{}
    类与接口之间:实现关系
        public class 实现类 implements 接口,接口...{}
    接口与接口之间:继承关系
        public interface 子接口 extends 接口,接口...{}
    注意:
        1.子接口继承多个父接口,相当于求接口的并集
        2.子接口包含所有父接口中的方法(抽象的,默认)
        3.父接口中的默认方法有重复的,子接口必须重写,为了区分使用的是哪个默认方法
        */
public interface C extends A,B{
    public abstract void c();
    @Override
    public default void show(){
        System.out.println("C接口中重写A接口和B接口中的默认show方法！");
    }
}
