package day02.com.ketang.demo13interface;

public interface B {
    public abstract void b();
    public default void show(){
        System.out.println("B接口中的默认的show方法！");
    }
}
