package com.ketang.demo11code;

public class Student {
    private String name;
    private int age;
    private static String sex;

    /*
        2.构造代码块:
                写在成员位置(类中方法外)的代码块
        特点:
            优先于构造方法执行,每创建一次对象,都会执行一次
        作用:
            1.可以给成员变量赋初始化值
            2.可以把每个构造方法中共性的内容提取出来,写在构造代码块中
*/

    {
        this.name = "贾宇";
        this.age = 18;
        System.out.println("构造代码块！");
    }

    /*
        3.静态代码块(重点):
                写在成员位置(类中方法外)被static修饰的代码块
        特点:
            static修饰的成员属于类,不属于某一个对象,被所有的对象所共享
            所以我们无论创建多少次对象,静态代码块只执行一次


            静态优选于非静态加载到内存中,优先于构造代码块和构造方法执行


        作用:
            1.可以给静态的成员方法赋初始值
            2.在项目启动的时候,可以做一些初始化的设计(只执行一次 数据库)
     */

    static {
        sex = "男";
        System.out.println("静态代码块！");
    }

    public Student() {
        System.out.println("空参数的构造方法！");
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("带参数的构造代码块！");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static String getSex() {
        return sex;
    }

    public static void setSex(String sex) {
        Student.sex = sex;
    }
}
