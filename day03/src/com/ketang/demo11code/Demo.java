package com.ketang.demo11code;
/*
    代码块:被{ }包裹起来的代码叫代码块
        1.局部代码块:写在方法中的代码块
        2.构造代码块:写在成员位置(类中方法外)的代码块
        3.静态代码块:写在成员位置(类中方法外)被static修饰的代码块
 */
public class Demo {
    public static void main(String[] args) {
            /*
            1.局部代码块:
                    写在方法中的代码块
            作用:
                    修改变量的作用域,提高程序的效率

            变量的作用域:
                    在变量所在的{ }的范围内有效,出了作用域,就会被垃圾回收
         */
        {
         int a = 10;
            System.out.println(a);
        }
        System.out.println("------------------------");
        Student s1 = new Student();
        System.out.println(s1.getName()+"\t"+s1.getAge()+"\t"+s1.getSex());
        Student s2 = new Student("古力娜扎",20);
        System.out.println(s2.getName()+"\t"+s2.getAge()+"\t"+s2.getSex());

    }
}
