package com.ketang.demo09access;

public class Person {
    public int a = 10;
    protected int b = 20;
    int c = 30;
    private int d = 40;

    public void show(){
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
    }
}
