package com.ketang.demo05InnerClass;
/*注意:
        在内部类中可以直接使用外部类的成员变量和成员方法

        内部类只是定义在其他类的内部,其他的使用方式不变,也可以继承其他的类,实现其他的接口
*/
public class Outer {
    int a = 10;
    public void outer(){
        System.out.println("外部类Outer的成员方法outer!");
    }
    public class Inner{
        int b = 20;
        public void inner(){
            System.out.println("内部类Inner的成员方法inner!");
            System.out.println(a);
            outer();
        }
    }
}
