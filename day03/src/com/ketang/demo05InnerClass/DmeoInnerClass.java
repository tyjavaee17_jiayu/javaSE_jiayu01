package com.ketang.demo05InnerClass;

public class DmeoInnerClass {
    public static void main(String[] args) {
        Outer.Inner oi = new Outer().new Inner();
        System.out.println(oi.b);
        oi.inner();
    }
}
