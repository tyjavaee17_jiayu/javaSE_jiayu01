package com.ketang.demo07InnerClass;
/*
    匿名内部类(重点):
        匿名:创建内部类,没有赋值给某一个变量,没有名字
        内部类:是一个局部内部类(写在方法中)
    作用:简化代码
        把子类继承父类,重写父类的方法,创建子类对象合成一步完成
        把实现类实现接口,重写接口中的方法,创建实现类对象合成一步完成
    格式:
        new 父类|接口(){
            重写父类|接口中的方法
        };
*/
public class DemoInnerClass {
    public static void main(String[] args) {
        new Cat().eat();
        new Cat().sleep();
        new Cat().catchMouse();
        System.out.println("------------");
        new Ainmal(){
            @Override
            public void eat() {
                System.out.println("动物在吃饭");
            }
            @Override
            public void sleep() {
                System.out.println("动物在睡觉");
            }
        }.eat();
        System.out.println("---------------");
        Ainmal a = new Ainmal() {
            @Override
            public void eat() {
                System.out.println("动物在吃饭");
            }
            @Override
            public void sleep() {
                System.out.println("动物在睡觉");
            }
        };
        a.eat();
        a.sleep();
    }
}
