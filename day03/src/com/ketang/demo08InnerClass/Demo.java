package com.ketang.demo08InnerClass;

public class Demo {
    public static void main(String[] args) {
       new MaQu().fly();
        System.out.println("--------------");
        new Fly(){
            @Override
            public void fly() {
                System.out.println("动物在飞翔！");
            }
        }.fly();
        System.out.println("--------------");
        new Fu(){
            @Override
            public void work(){
                System.out.println("匿名的儿子在工作！");
            }
            public void eat(){
                System.out.println("父亲在吃饭！");
            }
        }.eat();
        new Fu(){
            @Override
            public void work(){
                System.out.println("匿名的儿子在工作！");
            }
            //定义特有的吃饭的方法
            public void eat(){
                System.out.println("父亲在吃饭！");
            }
        }.work();
        System.out.println("----------------");
        Smoking s = new Smoking() {
            @Override
            public void somke() {
                System.out.println("抽烟!");
            }
        };
        s.somke();
        s.somke();
    }
}
