package com.ketang.demo06InnerClass;
/*使用格式:
            局部内部类的使用范围就是在方法中有效
           使用方式就是在（方法中）定义完局部内部类之后,直接创建局部内部类的对象使用
*/
public class Outer {
    public void outer(){
        final class Inner{
            int a = 10;
            public void inner(){
                System.out.println("1局部内部类的inner成员方法!");
            }
        }
        //定义完局部内部类之后立即创建对象使用
        Inner in = new Inner();
        System.out.println(in.a);
        in.inner();
    }
    public void outter(){
        final class Inner{
            int a = 20;
            public void inner(){
                System.out.println("2局部内部类的inner成员方法!");
            }
        }
        //定义完局部内部类之后立即创建对象使用
        Inner in = new Inner();
        System.out.println(in.a);
        in.inner();
    }
}
