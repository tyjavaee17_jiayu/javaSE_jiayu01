package com.ketang.demo03duotai;

public class Demo {
    public static void main(String[] args) {
        Fu f = new Zi();
        f.work();
        System.out.println("---------------");
        /*
           f.playGame();//Cannot resolve method 'playGame()'
        */
        Zi z = (Zi)f;
        z.playGame();
        System.out.println("------------------");
        Fu f1 = new Fu();
        Zi z1 = (Zi)f1;
        /*
        直接创建父类对象,不能向下转型
        ClassCastException: com.ketang.demo03duotai.Fu cannot be cast to com.ketang.demo03duotai.Zi
        */
    }
}
