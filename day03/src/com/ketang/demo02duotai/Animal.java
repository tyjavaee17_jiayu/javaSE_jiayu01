package com.ketang.demo02duotai;

public abstract class Animal {
    public abstract void eat();
    public abstract void sleep();
}
