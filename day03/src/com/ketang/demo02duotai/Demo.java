package com.ketang.demo02duotai;

public class Demo {
    public static void main(String[] args) {
        Cat c = new Cat();
        show(c);
        Dog d = new Dog();
        show(d);

        System.out.println("---------------");

        Animal a = getAnimal();   // Animal a = getAnimal(); = new Cat(); 多态
        a.eat();
        a.sleep();
    }
    /*
        定义一个方法,返回值类型使用Animal
        Animal是一个抽象类,无法创建对象
        创建Animal的任意子类对象返回
     */
    public static Animal getAnimal(){
        return new Cat();
    }
    public static void show(Animal a){
        a.eat();
        a.sleep();
    }
}
