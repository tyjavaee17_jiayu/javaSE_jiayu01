package com.ketang.demo04instanceof;

import com.ketang.demo02duotai.Animal;
import com.ketang.demo02duotai.Cat;
import com.ketang.demo02duotai.Dog;
/*
    向下转型:有可能发生类型转换异常ClassCastException
        ClassCastException: com.itheima.demo02duotai.Cat cannot be cast to com.itheima.demo02duotai.Dog
    instanceof关键字:判断某个对象是否属于某种数据类型
        boolean b = 对象 instanceof 数据类型;
        对象属于对应的数据类型,返回true
        对象不属于对象的数据类型,返回false
    使用前提:
        对象根据类创建的(对象所属的类必须和判断的数据类型之间((((((有继承或者实现关系)))))
*/
public class DemoInstanceof {
    public static void main(String[] args) {
        Animal a = new Cat();
        a.eat();
        a.sleep();
        if (a instanceof Cat){
            Cat c = (Cat)a;
            c.catchMouse();
        }else if (a instanceof Dog){
            Dog d = (Dog)a;
            d.lookHome();
        }
    }
}
