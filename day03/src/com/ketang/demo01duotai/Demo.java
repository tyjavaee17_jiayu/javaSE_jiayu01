package com.ketang.demo01duotai;

public class Demo {
    public static void main(String[] args) {
        Animal a1 = new Cat();
        a1.eat();
        Animal a2 = new Dog();
        a2.eat();
        System.out.println("----------");
        Fu f = new Zi();
        f.show();
        System.out.println("----------");
        Smoking s = new Teacher();
        s.smoke();
    }
}
