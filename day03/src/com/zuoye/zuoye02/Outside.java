package com.zuoye.zuoye02;

public class Outside {
    private int a = 100;
    class Inside{
        private int a = 200;
        public void show(){
            int a = 300;
            System.out.println(new Outside().a);
            System.out.println(this.a);
            System.out.println(a);
        }
    }
}
