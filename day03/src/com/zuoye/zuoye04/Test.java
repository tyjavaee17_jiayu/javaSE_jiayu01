package com.zuoye.zuoye04;

public class Test {
    public static void main(String[] args) {
        fun(new Cat());
        fun(new Animal() {
            @Override
            public void show() {
                System.out.println("xxxx");
            }
        });
    }
    public static void fun(Animal a){
        a.show();
    }
}
