package com.zuoye.zuoye01;

public class Demo {
    public static void main(String[] args) {
        Animal a = new Cat();
        a.eat();
        /*
            a.catchMouse();//Cannot resolve method 'catchMouse()'
        **/
        Cat c = (Cat)a;
        c.catchMouse();
    }
}
