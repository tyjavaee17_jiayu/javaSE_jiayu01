package com.zuoye.zuoye03;

public class Outside {
    public void show(){
        int a = 10;
        class Inside{
            public void show(){
                System.out.println("a = " + a );
            }
        }
        Inside i = new Inside();
        i.show();
    }
}
