package com.ketang.demo04SetThreadName;

public class Demo01SetThreadName {
    public static void main(String[] args) {
        MyThread mt1 = new MyThread();
        mt1.setName("小强");
        mt1.start();

        MyThread mt2 = new MyThread("旺财");
        mt2.start();
    }
}
