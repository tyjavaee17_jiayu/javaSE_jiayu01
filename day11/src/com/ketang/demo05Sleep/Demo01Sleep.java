package com.ketang.demo05Sleep;

public class Demo01Sleep {
    public static void main(String[] args) {
        System.out.println("线程10秒钟后执行");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("线程过了10秒中睡醒了，继续执行");
    }
}
