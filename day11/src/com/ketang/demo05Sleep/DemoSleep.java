package com.ketang.demo05Sleep;

public class DemoSleep {
    public static void main(String[] args) throws InterruptedException {
        //及时
        for (int i = 0; i < 60; i++) {
            System.out.println(i);
            Thread.sleep(1000);
        }
        //倒计时
        for (int i = 60; i >=1 ; i--) {
            System.out.println(i);
            Thread.sleep(1000);
        }
    }
}
