package com.ketang.demo10atom;

public class MyThread extends Thread {
    public static int money = 0;

    @Override
    public void run() {
        System.out.println("Thread-0线程开始改变money的值");
        for (int i = 0; i < 10000; i++) {
            money++;
        }
        System.out.println("Thread-0线程执行完毕，结束了！");
    }
}
