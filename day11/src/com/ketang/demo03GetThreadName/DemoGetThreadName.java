package com.ketang.demo03GetThreadName;

public class DemoGetThreadName {
    public static void main(String[] args) {
        MyThread mt1 = new MyThread();
        mt1.start();
        System.out.println("============");
        MyThread mt2 = new MyThread();
        mt2.start();
        System.out.println("============");
        System.out.println(Thread.currentThread().getName());
        System.out.println(Thread.currentThread().getPriority());
    }
}
