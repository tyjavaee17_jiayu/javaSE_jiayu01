package com.ketang.demo03GetThreadName;

public class MyThread extends Thread {

    @Override
    public void run() {
        //1.可以使用Thread类中的方法getName
        String name = getName();
        System.out.println(name);
        //2.我们可以获取到当前正在执行的线程Thread
        Thread thread = currentThread();
        String name1 = thread.getName();
        System.out.println(name1);
        //3.直接链式编程
        System.out.println(Thread.currentThread().getName());
    }
}
