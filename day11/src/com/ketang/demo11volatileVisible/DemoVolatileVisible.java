package com.ketang.demo11volatileVisible;

public class DemoVolatileVisible {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();
        System.out.println("main线程，开启完新的线程之后，会继续执行main'方法！");
        while (true){
            if (MyThread.a == 1){
                System.out.println("main线程，判断变量的值为1，结束死循环");
                break;
            }
        }
    }
}
