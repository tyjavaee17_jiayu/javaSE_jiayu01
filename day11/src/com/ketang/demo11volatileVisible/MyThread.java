package com.ketang.demo11volatileVisible;

public class MyThread extends Thread {
    public static  volatile int a = 0;

    @Override
    public void run() {
        System.out.println("Thread-0线程开始执行了，先睡2秒钟！");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread-0线程睡醒了，改变变量的值为1");
        a = 1;
        System.out.println("Thread-0线程执行完线程任务了，结束了！");
    }
}
