package com.ketang.demo02Thread;

public class DemoThread {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        int priority = mt.getPriority();
        System.out.println(priority);
        mt.start();
        System.out.println("-----------");
        for (int i = 0; i < 10; i++) {
            System.out.println("main-->" + i);
        }
    }
}
