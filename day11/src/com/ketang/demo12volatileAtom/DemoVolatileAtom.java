package com.ketang.demo12volatileAtom;

public class DemoVolatileAtom {
    public static void main(String[] args) throws InterruptedException {
        MyThread mt1 = new MyThread();
        mt1.start();

        System.out.println("main线程开启新的线程之后，会继续执行main方法中的代码！");
        System.out.println("main线程开始改变money的值");
        for (int i = 0; i < 10000; i++) {
            MyThread.money++;

        }
        System.out.println("main线程暂停1秒，等待Thread-0线程执行完毕！");
        Thread.sleep(1000);
        System.out.println("最终money值为：" + MyThread.money);
    }
}
