package com.ketang.demo09Visible;

public class MyThread extends Thread {
    public static int a = 0;

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread-0线程睡醒了，改变变量a得值为1");
        a = 1;
        System.out.println("Thread-0线程执行完线程任务了，结束了！");
    }
}
