package com.ketang.demo09Visible;

public class DemoVisible {
    public static void main(String[] args) throws InterruptedException {
        Thread mt = new Thread();
        mt.start();
        System.out.println("main线程，开启完新的线程之后，会继续执行main方法！");
        while (true){
            if (MyThread.a == 1){
                System.out.println("main线程，判断变量的值为1，结束死循环");
                break;
            }
            /*else{
                System.out.println("a的值是0，继续死循环");
            }*/
            Thread.sleep(1000);
        }
    }
}
