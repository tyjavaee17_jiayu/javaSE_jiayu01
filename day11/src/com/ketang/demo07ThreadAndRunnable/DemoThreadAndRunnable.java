package com.ketang.demo07ThreadAndRunnable;

public class DemoThreadAndRunnable {
    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();

        new Thread(new RunnableImpl1()).start();
        new Thread(new RunnableImpl2()).start();
    }
}
