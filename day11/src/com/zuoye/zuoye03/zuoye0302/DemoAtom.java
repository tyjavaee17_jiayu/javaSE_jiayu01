package com.zuoye.zuoye03.zuoye0302;

public class DemoAtom {
    public static void main(String[] args) throws InterruptedException {
        MyThread mt = new MyThread();
        mt.start();
        System.out.println("main线程改变值开始了！");
        for (int i = 0; i < 10000; i++) {
            MyThread.age++;
        }
        Thread.sleep(1000);
        System.out.println("main线程的年龄叠加完毕！");
        System.out.println("最终的年龄为：" + MyThread.age);
    }
}
