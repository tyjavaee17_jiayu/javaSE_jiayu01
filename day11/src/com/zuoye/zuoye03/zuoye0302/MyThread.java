package com.zuoye.zuoye03.zuoye0302;

public class MyThread extends  Thread {
    public static int age = 0;

    @Override
    public void run() {
        System.out.println("我的线程开始加年龄了");
        for (int i = 0; i <10000 ; i++) {
            age++;
        }
        System.out.println("我的线程执行完毕，年龄叠加完毕！");
    }
}
