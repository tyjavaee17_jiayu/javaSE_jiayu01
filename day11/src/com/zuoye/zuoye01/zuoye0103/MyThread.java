package com.zuoye.zuoye01.zuoye0103;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyThread extends Thread {
    @Override
    public void run() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < 10; i++) {
            Date date = new Date();
            String s = sdf.format(date);
            System.out.println(s);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
