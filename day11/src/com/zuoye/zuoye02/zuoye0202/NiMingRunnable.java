package com.zuoye.zuoye02.zuoye0202;

public class NiMingRunnable {
    public static void main(String[] args) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                int sum = 0;
                for (int i = 0; i < 500; i++) {
                    sum += i ;
                }
                System.out.println("1-500的累加和为："+ sum);
            }
        };
        new Thread(r).start();
    }
}
