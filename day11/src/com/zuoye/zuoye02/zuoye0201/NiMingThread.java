package com.zuoye.zuoye02.zuoye0201;

public class NiMingThread {
    public static void main(String[] args) {
        new Thread(){
            @Override
            public void run() {
                int sum = 0;
                for (int i = 0; i < 100; i++) {
                    sum += i ;
                }
                System.out.println("1-100的累加和为：" + sum);
            }
        }.start();
    }
}
