package com.zuoye.zuoye04.zuoye0402;

import java.util.concurrent.atomic.AtomicInteger;

public class MyThread extends  Thread {
    public static AtomicInteger age = new AtomicInteger(0);

    @Override
    public void run() {
        System.out.println("我的线程开始加年龄了");
        for (int i = 0; i <10000 ; i++) {
            age.getAndIncrement();
        }
        System.out.println("我的线程执行完毕，年龄叠加完毕！");
    }
}
